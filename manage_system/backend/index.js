const express = require("express");
const app = express();
const port = 3002;
const cors = require("cors");
const fs = require('fs');
const path = require('path');
const fstream = require('fstream');
const tar = require('tar');
const zlib = require('zlib');

/*database relative*/
const User_db = require("./user_db.js");
const Nlu_db = require("./nlu_db.js");
const Meeting_db = require("./meeting_db.js");
const YMLCreate  = require("./yml_create.js");
var userDatabase = new User_db("./Database/user_info.db");
var nluDatabase = new Nlu_db("./Database/NLU.db");
var meetingDatabase = new Meeting_db("./Database/meeting.db");

/*Authentication relative*/
const bcrypt = require("bcrypt");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const { getUsers } = require("./user_db");
const saltRounds = 10;

app.use(express.json());
app.use(
  cors({
    origin: ["http://localhost:3000", "http://localhost:3001"],
    methods: ["GET", "POST"],
    credentials: true,
  })
);

/*Authentication relative*/
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({
    key: "userID",
    secret: "learnfromonline",
    resave: false,
    saveUninitialized: false,
    cookie: {
      expires: 60 * 60 * 24 * 1000,
    },
  })
);

// ---------------------------------LOGIN------------------------------------------- //

const verifyJwt = (req, res, next) => {
  const token = req.headers["x-access-token"];
  if (!token) {
    res.send("We need a token, pls give it to us next time.");
  } else {
    jwt.verify(token, "learnfromonline", (err, decoded) => {
      if (err) {
        res.json({ auth: false, message: "Failed auth" });
      } else {
        req.userId = decoded.id;
        next();
      }
    });
  }
};

/*
 * Verify user using verifyJwt
 * Method: get
 * Path:
 *       /userVerify
 * Params:
 * Return:
 *       auth: boolean - auth status
 */
app.get("/userVerify", verifyJwt, (require, response) => {
  response.send({ auth: true });
});

/*
 * Return login info
 * Method: get
 * Path:
 *       /login
 * Params:
 *       user: user info
 * Return:
 *       loggedIn: boolean - whether login is successful
 *       user: string - user info
 */
app.get("/login", (require, response) => {
  if (require.session.user) {
    response.send({ loggedIn: true, user: require.session.user });
  } else {
    response.send({ loggedIn: false });
  }
});

/*
 * Return login info
 * Method: post
 * Path:
 *       /login
 * Params:
 *       username: string - username
 *       password: string - password
 * Return:
 *       auth: boolean - whether login is successful
 *       token: token - send if login success
 *       incorrect_message: send if login failed
 *       error: string - send if error
 */
app.post("/login", (require, response) => {
  const username = require.body.username;
  const password = require.body.password;
  const userFound = userDatabase.getUser(username);

  if (userFound === undefined) {
    response.send({ auth: false, incorrect_message: "User does not exist." });
  } else {
    bcrypt.compare(password, userFound.password, (err, res) => {
      if (res) {
        console.log("login success");
        const id = userFound.userID;
        const token = jwt.sign({ id }, "learnfromonline", {
          expiresIn: 60 * 60 * 24 * 1000,
        });
        require.session.user = userFound;
        response.json({ auth: true, token: token, userFound });
      } else {
        response.send({ auth: false, incorrect_message: "Wrong password." });
      }
    });
  }
});

/*
 * Add a user
 * Method: post
 * Path:
 *       /register
 * Params:
 *       username: string - username
 *       password: string - password
 * Return:
 *       message: send if  register success
 *       error: string - send if error
 */
app.post("/register", (require, response) => {
  const username = require.body.username;
  const password = require.body.password;

  bcrypt.hash(password, saltRounds, (err, hash) => {
    if (err) {
      console.log(err);
    }
    const result = userDatabase.addUser(username, hash);
    if (result) {
      response.send({ returnMessage: "Success register." });
    } else {
      response.send({ returnMessage: "Register Failed." });
    }
  });
});

/*
 * change password
 * Method: post
 * Path:
 *       /changePassword
 * Params:
 *       username: string - username
 *       password: string - password
 * Return:
 *       message: send if  change password success
 *       error: string - send if error
 */
app.post("/changePassword", (require, response) => {
  const username = require.body.username;
  const oldPassword = require.body.oldPassword;
  const newPassword = require.body.newPassword;
  console.log(newPassword);

  const user = userDatabase.getUser(username);

  if (!user) {
    response.send({ status: 100, failType: -1, msg: "there is not same user" });

    return;
  }

  bcrypt.compare(oldPassword, user.password, (err, res) => {
    if (res) {
      bcrypt.hash(newPassword, saltRounds, (err, hash) => {
        if (err) {
          console.log(err);
        }
        const result = userDatabase.updateUserPass(username, hash);
        if (result) {
          response.send({ status: 200, msg: "change password success." });
        } else {
          response.send({
            status: 100,
            failType: 1,
            msg: "change password Failed.",
          });
        }
      });
    } else {
      response.send({
        status: 100,
        failType: 0,
      });
    }
  });
});

// ---------------------------------NLU-------------------------------------------------- //
// ---------------------------------UNIT TABLE------------------------------------------- //

/*
 * get info of all units, including their credits, link, prohibition, prerequisites, year, etc
 * Method: get
 * Path:
 *       /dashboard/unit_table/get_unit_table
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       unitList: string - Return a string of all rows in the unit table
 *       reason: string - "success" or "failed"
 */
app.get("/dashboard/unit_table/get_unit_table", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;
  const unitList = nluDatabase.getAllUnits();

  if (unitList === undefined) {
    response.send({ status: 100, reason: "units not found" });
  } else {
    if (dtype === "xml") {
    } else {
      response.send({ status: 200, unitList: unitList, reason: "success" });
    }
  }
});

/*
 * get info of a specific unit, including: credits, link, prohibition, prerequisites, year, etc
 * Method: get
 * Path:
 *       /dashboard/unit_table/get_unit
 * Params:
 *       user_id: String - User ID
 *       unitID: string - Unit code
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       unitList: string - Return a string of all rows in the unit table
 *       reason: string - Return description
 */
app.get("/dashboard/unit_table/get_unit", (require, response) => {
  const user_id = require.query.user_id;
  const unitID = require.query.unitID;
  const dtype = require.query.dtype;
  console.log("require is");
  console.log(require);
  console.log("unitID is" + unitID + " user_id is " + user_id);

  if (!unitID) {
    response.send({ status: 100, reason: "no unit id" });
  } else {
    const unitInfo = nluDatabase.getUnit(unitID);
    if (unitInfo === undefined) {
      response.send({ status: 100, reason: "unit not found" });
    } else {
      if (dtype === "xml") {
        // xml format data
      } else {
        response.send({ status: 200, unitInfo: unitInfo, reason: "success" });
      }
    }
  }
});

/*
 * To get a list of all available unit codes, the back-end will return a list that contains unit codes only
 * Method: get
 * Path:
 *       /dashboard/unit_table/get_unitCodeList
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       unitCodeList: string - Return a string that contains all unit Codes
 *       reason: string - Return description
 */
app.get("/dashboard/unit_table/get_unitCodeList", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;

  const unitCodeList = nluDatabase.getUnitCode();

  if (unitCodeList === undefined) {
    response.send({ status: 100, reason: "units not found" });
  } else {
    if (dtype === "xml") {
      // xml format data
    } else {
      response.send({
        status: 200,
        unitCodeList: unitCodeList,
        reason: "success",
      });
    }
  }
});

/*
 * Edit existed unit info in the db
 * Method: post
 * Path:
 *       /dashboard/unit_table/update_unit
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       unitID: string - Unit code
 *       credits: int - Credit points of a unit
 *       major: String - Major type
 *       core: string - whether the unit is a core unit or not
 *       link: String - The UoS link of a unit
 *       year: string - The year the unit info is offered
 *       prohibition: string - The prohibition of unit enrollment
 *       prerequisites: string - The prerequisites of unit enrollment
 *       coordinator: string - unit coordinator
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/unit_table/update_unit", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;
  const unitID = require.body.unitID;
  const credits = require.body.credits;
  const major = require.body.major;
  const core = require.body.core;
  const link = require.body.link;
  const year = require.body.year;
  const prohibition = require.body.prohibition;
  const prerequisites = require.body.prerequisites;
  const coordinator = require.body.coordinator;

  console.log("year is ", year);
  console.log("link is ", link);
  if (
    !unitID ||
    !credits ||
    !major ||
    !core ||
    !link ||
    !year ||
    !prohibition ||
    !prerequisites ||
    !coordinator
  ) {
    response.send({
      status: 100,
      reason: "please provide all information",
    });
  } else {
    var available = 0;
    const currentYear = new Date().getFullYear();
    if (currentYear == year) {
      available = 1;
    } else {
      available = 0;
    }
    console.log("available in index: " + available);
    const updated = nluDatabase.updateUnit(
      unitID,
      credits,
      major,
      core,
      link,
      year,
      prohibition,
      prerequisites,
      available
    );
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Add a new unit info in the db
 * Method: POST
 * Path:
 *       /dashboard/unit_table/add_unit
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       unitID: string - Unit code
 *       credits: int - Credit points of a unit
 *       major: String - Major type
 *       core: string - whether the unit is a core unit or not
 *       link: String - The UoS link of a unit
 *       year: string - The year the unit info is offered
 *       prohibition: string - The prohibition of unit enrollment
 *       prerequisites: string - The prerequisites of unit enrollment
 *       coordinator: string - unit coordinator
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       unitCodeList: string - Return a string that contains all unit Codes
 *       reason: string - Return description
 */
app.post("/dashboard/unit_table/add_unit", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;
  const unitID = require.body.unitID;
  const credits = require.body.credits;
  const major = require.body.major;
  const core = require.body.core;
  const link = require.body.link;
  const year = require.body.year;
  const prohibition = require.body.prohibition;
  const prerequisites = require.body.prerequisites;
  const coordinator = require.body.coordinator;

  if (
    !unitID ||
    !credits ||
    !major ||
    !core ||
    !link ||
    !year ||
    !prohibition ||
    !prerequisites ||
    !coordinator
  ) {
    response.send({
      status: 100,
      reason: "please provide all information",
    });
  } else {
    var available = 0;
    const currentYear = new Date().getFullYear();
    if (currentYear == year) {
      available = 1;
    } else {
      available = 0;
    }
    console.log("available in index: " + available);
    const updated = nluDatabase.addUnit(
      unitID,
      credits,
      major,
      core,
      link,
      year,
      prohibition,
      prerequisites,
      available
    );
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

// ---------------------------------DEGREE TABLE------------------------------------------- //

/*
 * get info of all rows in the degree table
 * Method: get
 * Path:
 *       /dashboard/degree_table/get_degree_table
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       degreeTableList: string - Return a string of all rows in the degree table
 *       reason: string - Return description
 */
app.get("/dashboard/degree_table/get_degree_table", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;

  const degreeTableList = nluDatabase.getAllDegree();

  if (degreeTableList === undefined) {
    response.send({ status: 100, reason: "degrees not found" });
  } else {
    if (dtype === "xml") {
      // xml format data
    } else {
      response.send({
        status: 200,
        degreeTableList: degreeTableList,
        reason: "success",
      });
    }
  }
});

/*
 * get a question and its related answer
 * Method: get
 * Path:
 *       /dashboard/degree_table/get_question
 * Params:
 *       user_id: String - User ID
 *       questionID: String - Question ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       questionInfo: string - Return a string of a row in the degree table
 *       reason: string - Return description
 */
app.get("/dashboard/degree_table/get_question", (require, response) => {
  const user_id = require.query.user_id;
  const dtype = require.query.dtype;
  const questionID = require.query.questionID;

  if (!questionID) {
    response.send({ status: 100, reason: "id not provided" });
  } else {
    const questionInfo = nluDatabase.getDegree(questionID);
    if (questionInfo === undefined) {
      response.send({ status: 100, reason: "question not found" });
    } else {
      if (dtype === "xml") {
        // xml format data
      } else {
        response.send({
          status: 200,
          questionInfo: questionInfo,
          reason: "success",
        });
      }
    }
  }
});

/*
 * Edit existed question info in the db
 * Method: post
 * Path:
 *       /dashboard/degree_table/update_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       questionID: string - Question ID
 *       question: int - Questions
 *       answer: String - Answers
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/degree_table/update_question", (require, response) => {
  const user_id = require.body.user_id;
  const questionID = require.body.questionID;
  const question = require.body.question;
  const answer = require.body.answer;
  const dtype = require.body.dtype;

  if (!questionID || !question || !answer) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.updateDegree(questionID, question, answer);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Add a new question info in the db
 * Method: post
 * Path:
 *       /dashboard/degree_table/add_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       question: int - Questions
 *       answer: String - Answers
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/degree_table/add_question", (require, response) => {
  const user_id = require.body.user_id;
  const question = require.body.question;
  const answer = require.body.answer;
  const dtype = require.body.dtype;

  if (!question || !answer) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.addDegree(question, answer);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Delete a question in the db
 * Method: post
 * Path:
 *       /dashboard/degree_table/delete_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       questionID: string - Question ID
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/degree_table/delete_question", (require, response) => {
  const user_id = require.body.user_id;
  const questionID = require.body.questionID;
  const dtype = require.body.dtype;

  if (!questionID) {
    response.send({
      status: 100,
      reason: "please provide question id",
    });
  } else {
    const updated = nluDatabase.deleteDegree(questionID);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

// ---------------------------------HONOURS TABLE------------------------------------------- //

/*
 * get info of all rows in the hornous table
 * Method: get
 * Path:
 *       /dashboard/hornous_table/get_hornous_table
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       honoursTableList: string - Return a string of all rows in the hornous table
 *       reason: string - Return description
 */
app.get("/dashboard/hornous_table/get_hornous_table", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;

  const honoursTableList = nluDatabase.getAllHonour();

  if (honoursTableList === undefined) {
    response.send({ status: 100, reason: "honours not found" });
  } else {
    if (dtype === "xml") {
      // xml format data
    } else {
      response.send({
        status: 200,
        honoursTableList: honoursTableList,
        reason: "success",
      });
    }
  }
});

/*
 * get a question and its related answer
 * Method: get
 * Path:
 *       /dashboard/hornous_table/get_question
 * Params:
 *       user_id: String - User ID
 *       questionID: string - Question ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       questionInfo: string - Return a string of a row in the degree table
 *       reason: string - Return description
 */
app.get("/dashboard/hornous_table/get_question", (require, response) => {
  const user_id = require.query.user_id;
  const dtype = require.query.dtype;
  const questionID = require.query.questionID;

  if (!questionID) {
    response.send({ status: 100, reason: "id not provided" });
  } else {
    const questionInfo = nluDatabase.getHonour(questionID);

    if (questionInfo === undefined) {
      response.send({ status: 100, reason: "info not found" });
    } else {
      if (dtype === "xml") {
        // xml format data
      } else {
        response.send({
          status: 200,
          questionInfo: questionInfo,
          reason: "success",
        });
      }
    }
  }
});

/*
 * Edit existed question info in the db
 * Method: post
 * Path:
 *       /dashboard/hornous_table/update_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       questionID: string - Question ID
 *       question: int - Questions
 *       answer: String - Answers
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/hornous_table/update_question", (require, response) => {
  const user_id = require.body.user_id;
  const questionID = require.body.questionID;
  const question = require.body.question;
  const answer = require.body.answer;
  const dtype = require.body.dtype;

  if (!questionID || !question || !answer) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.updateHonour(questionID, question, answer);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Add a new question info in the db
 * Method: post
 * Path:
 *       /dashboard/hornous_table/add_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       question: int - Questions
 *       answer: String - Answers
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/hornous_table/add_question", (require, response) => {
  const user_id = require.body.user_id;
  const question = require.body.question;
  const answer = require.body.answer;
  const dtype = require.body.dtype;

  if (!question || !answer) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.addHonour(question, answer);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Delete a question in the db
 * Method: post
 * Path:
 *       /dashboard/hornous_table/delete_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       questionID: string - Question ID
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/hornous_table/delete_question", (require, response) => {
  const user_id = require.body.user_id;
  const questionID = require.body.questionID;
  const dtype = require.body.dtype;

  if (!questionID) {
    response.send({
      status: 100,
      reason: "please provide question id",
    });
  } else {
    const updated = nluDatabase.deleteHonour(questionID);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

// ---------------------------------FAQ TABLE------------------------------------------- //

/*
 *  get info of all rows in the faq table
 * Method: get
 * Path:
 *       /dashboard/faq_table/get_faq_table
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       faqTableList: string - Return a string of all rows in the faq table
 *       reason: string - Return description
 */
app.get("/dashboard/faq_table/get_faq_table", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;

  const faqTableList = nluDatabase.getAllFAQ();

  if (faqTableList === undefined) {
    response.send({ status: 100, reason: "faqs not found" });
  } else {
    if (dtype === "xml") {
      // xml format data
    } else {
      response.send({
        status: 200,
        faqTableList: faqTableList,
        reason: "success",
      });
    }
  }
});

/*
 * get a question and its related answer
 * Method: get
 * Path:
 *       /dashboard/faq_table/get_question
 * Params:
 *       user_id: String - User ID
 *       questionID: string - Question ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       questionInfo: string - Return a string of a row in the degree table
 *       reason: string - Return description
 */
app.get("/dashboard/faq_table/get_question", (require, response) => {
  const user_id = require.query.user_id;
  const dtype = require.query.dtype;
  const questionID = require.query.questionID;

  if (!questionID) {
    response.send({ status: 100, reason: "id not provided" });
  } else {
    const questionInfo = nluDatabase.getFAQId(questionID);

    if (questionInfo === undefined) {
      response.send({ status: 100, reason: "info not found" });
    } else {
      if (dtype === "xml") {
        // xml format data
      } else {
        response.send({
          status: 200,
          questionInfo: questionInfo,
          reason: "success",
        });
      }
    }
  }
});

/*
 * Edit existed question info in the db
 * Method: post
 * Path:
 *       /dashboard/faq_table/update_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       questionID: string - Question ID
 *       question: int - Questions
 *       answer: String - Answers
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/faq_table/update_question", (require, response) => {
  const user_id = require.body.user_id;
  const questionID = require.body.questionID;
  const question = require.body.question;
  const answer = require.body.answer;
  const dtype = require.body.dtype;

  if (!questionID || !question || !answer) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.updateFAQ(questionID, question, answer);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Add a new question info in the db
 * Method: post
 * Path:
 *       /dashboard/faq_table/add_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       question: int - Questions
 *       answer: String - Answers
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/faq_table/add_question", (require, response) => {
  const user_id = require.body.user_id;
  const question = require.body.question;
  const answer = require.body.answer;
  const dtype = require.body.dtype;

  if (!question || !answer) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.addFAQ(question, answer);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Delete a question in the db
 * Method: post
 * Path:
 *       /dashboard/faq_table/delete_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       questionID: string - Question ID
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/faq_table/delete_question", (require, response) => {
  const user_id = require.body.user_id;
  const questionID = require.body.questionID;
  const dtype = require.body.dtype;

  if (!questionID) {
    response.send({
      status: 100,
      reason: "please provide question id",
    });
  } else {
    const updated = nluDatabase.deleteFAQ(questionID);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

// ---------------------------------STAFF TABLE------------------------------------------- //

/*
 * get info of all rows in the staff table
 * Method: get
 * Path:
 *       /dashboard/staff_table/get_staff_table
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       staffList: string - Return a string of all rows in the staff table
 *       reason: string - Return description
 */
app.get("/dashboard/staff_table/get_staff_table", (require, response) => {
  const user_id = require.body.user_id;
  const dtype = require.body.dtype;

  const staffList = nluDatabase.getAllStaff();

  if (staffList === undefined) {
    response.send({ status: 100, reason: "staffs not found" });
  } else {
    if (dtype === "xml") {
      // xml format data
    } else {
      response.send({ status: 200, staffList: staffList, reason: "success" });
    }
  }
});

/*
 * get a staff and all his roles. A staff can have multiple roles, all must be returned
 * Method: get
 * Path:
 *       /dashboard/staff_table/get_question
 * Params:
 *       user_id: String - User ID
 *       staffID: string - Academic staff ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       roles: string - The role that the staff have
 *       reason: string - Return description
 */
app.get("/dashboard/staff_table/get_question", (require, response) => {
  const user_id = require.query.user_id;
  const dtype = require.query.dtype;
  const staffID = require.query.staffID;

  if (!staffID) {
    response.send({ status: 100, reason: "id not provided" });
  } else {
    const roles = nluDatabase.getStaff(staffID);

    if (roles === undefined) {
      response.send({ status: 100, reason: "info not found" });
    } else {
      if (dtype === "xml") {
        // xml format data
      } else {
        response.send({ status: 200, roles: roles, reason: "success" });
      }
    }
  }
});

/*
 * Edit existing staff info in the db, a staff may have multiple roles,
 * the front-end will send params that contains all roles the staff have via http post.
 * Method: post
 * Path:
 *       /dashboard/staff_table/update_staff
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       staffID: string - Staff ID
 *       staffRole: object - An object list that contains different objects.
 *                           May contains following roles: “coordinator”, “directory”,
 *                           “Undergraduate Administrative officer”, “yearAdvisor”,
 *                           “Honours coordinator”
 *       name: String - name
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/staff_table/update_staff", (require, response) => {
  const user_id = require.body.user_id;
  const staffID = require.body.staffID;
  const staffRole = require.body.staffRole;
  const name = require.body.name;
  const dtype = require.body.dtype;

  if (!staffID || !staffRole || !name) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.updateStaff(staffID, staffRole, name);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Add a new staff info in the db,
 * a staff may have multiple roles, the front-end will send params
 * that contains all roles the staff have via http post.
 *
 * Method: post
 * Path:
 *       /dashboard/staff_table/add_question
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       staffRole: object - An object list that contains different objects.
 *                           May contains following roles: “coordinator”, “directory”,
 *                           “Undergraduate Administrative officer”,
 *                           “yearAdvisor”, “Honours coordinator”
 *       name: String - name
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/staff_table/add_question", (require, response) => {
  const user_id = require.body.user_id;
  const staffRole = require.body.staffRole;
  const name = require.body.name;
  const dtype = require.body.dtype;
  console.log("The parameter are" + user_id + "," + staffRole + "," + name);
  if (!staffRole || !name) {
    response.send({
      status: 100,
      reason: "please provide all required information",
    });
  } else {
    const updated = nluDatabase.addStaff(name, staffRole);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

/*
 * Delete a staff in the db
 * Method: post
 * Path:
 *       /dashboard/staff_table/delete_staff
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       staffID: string - staffID
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post("/dashboard/staff_table/delete_staff", (require, response) => {
  const user_id = require.body.user_id;
  const staffID = require.body.staffID;
  const dtype = require.body.dtype;

  if (!staffID) {
    response.send({
      status: 100,
      reason: "please provide staff id",
    });
  } else {
    const updated = nluDatabase.deleteStaff(staffID);
    if (dtype === "xml") {
    } else {
      if (updated) {
        response.send({
          status: 200,
          reason: "update successful",
        });
      } else {
        response.send({
          status: 100,
          reason: "update failed",
        });
      }
    }
  }
});

// ---------------------------------COORDINATOR TABLE------------------------------------------- //

/*
 * get info of all rows in the unitCoordinator table: including all pairs of the unitCode and the name of the unit coordinator
 * Method: get
 * Path:
 *       /dashboard/unitCoordinator_table/get_unitCoordinator_table
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       unitCoordinatorList: string - Return a string of all rows in the hornous table
 *       reason: string - Return description
 */
app.get(
  "/dashboard/unitCoordinator_table/get_unitCoordinator_table",
  (require, response) => {
    const user_id = require.body.user_id;
    const dtype = require.body.dtype;

    const unitCoordinatorList = nluDatabase.getAllUnitsCoordinator();

    if (unitCoordinatorList === undefined) {
      response.send({ status: 100, reason: "coordinators not found" });
    } else {
      if (dtype === "xml") {
        // xml format data
      } else {
        response.send({
          status: 200,
          unitCoordinatorList: unitCoordinatorList,
          reason: "success",
        });
      }
    }
  }
);

/*
 * get the name of the unit coordinator
 * Method: get
 * Path:
 *       /dashboard/unitCoordinator_table/get_unitCoordinator
 * Params:
 *       user_id: String - User ID
 *       unitID: string - The unit code
 *       dtype: String - The format of return data, xml or json, default as json
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       coordinator: string - Return a string of the coordinator name
 *       reason: string - Return description
 */
app.get(
  "/dashboard/unitCoordinator_table/get_unitCoordinator",
  (require, response) => {
    const user_id = require.query.user_id;
    const dtype = require.query.dtype;
    const unitID = require.query.unitID;

    if (!unitID) {
      response.send({ status: 100, reason: "id not provided" });
    } else {
      const coordinator = nluDatabase.getCoordinator(unitID);

      if (coordinator === undefined) {
        response.send({ status: 100, reason: "info not found" });
      } else {
        if (dtype === "xml") {
          // xml format data
        } else {
          response.send({
            status: 200,
            coordinator: coordinator,
            reason: "success",
          });
        }
      }
    }
  }
);

/*
 * Edit existed coordinator in the unit coordinator table,
 * change the existed name to become the name of new coordinator
 * Method: post
 * Path:
 *       /dashboard/unitCoordinator_table/update_unitCoordinator
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       unitID: string - unitID
 *       name: String - The Coordinator’s name
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       reason: string - Return description
 */
app.post(
  "/dashboard/unitCoordinator_table/update_unitCoordinator",
  (require, response) => {
    const user_id = require.body.user_id;
    const unitID = require.body.unitID;
    const name = require.body.name;
    const dtype = require.body.dtype;

    if (!unitID || !name) {
      response.send({
        status: 100,
        reason: "please provide all required information",
      });
    } else {
      const updated = nluDatabase.updateCoordinator(unitID, name);
      if (dtype === "xml") {
      } else {
        if (updated) {
          response.send({
            status: 200,
            reason: "update successful",
          });
        } else {
          response.send({
            status: 100,
            reason: "update failed",
          });
        }
      }
    }
  }
);

/*
 * Check whether the coordinator existed in the db or not
 *
 * Method: Get
 * Path:
 *       /dashboard/unitCoordinator_table/checkCoordinator
 * Params:
 *       user_id: String - User ID
 *       dtype: String - The format of return data, xml or json, default as json
 *       name: String - The name of the Coordinator
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       isExisted: string - Return true if the name existed in the staff table, otherwise return false
 *       reason: string - Return description
 */
app.post(
  "/dashboard/unitCoordinator_table/checkCoordinator",
  (require, response) => {
    const user_id = require.body.user_id;
    const name = require.body.name;
    const dtype = require.body.dtype;

    if (!name) {
      response.send({
        status: 100,
        reason: "please provide all required information",
      });
    } else {
      const updated = nluDatabase.checkCoordinator(name);
      if (dtype === "xml") {
      } else {
        if (updated) {
          response.send({
            status: 200,
            isExisted: "true",
            reason: "update successful",
          });
        } else {
          response.send({
            status: 100,
            isExisted: "false",
            reason: "update failed",
          });
        }
      }
    }
  }
);



/*end NLU*/

// ---------------------------------Meeting-------------------------------------------------- //
/*
 * get all info about the meetings that not start yet but in schedule, include meeting id, date, student name,
 * teacher name, status, staff type, student email, degree, year, comment, method, place
 *
 * Method: get
 * Path:
 *       /dashboard/meetings/inScheduleMeetings
 * Params:
 *
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       infoRow: string - Return the array of strings of each meeting info
 *       reason: string - Return description
 */
app.get("/dashboard/meetings/inScheduleMeetings", (require, response) => {
  const infoRow = meetingDatabase.getAllFutureMeeting();

  if (infoRow === undefined) {
    response.send({ status: 100, reason: "Meetings not found" });
  } else {
    response.send({ status: 200, infoRow: infoRow, reason: "success" });
  }
});

/*
 * get all info about the meetings that finished, include meeting id, date, student name, teacher name, status,
 * staff type, student email, degree, year, comment, method, place
 *
 * Method: get
 * Path:
 *       /dashboard/meetings/pastMeetings
 * Params:
 *       None
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       infoRow: string - Return the array of strings of each meeting info
 *       reason: string - Return description
 */
app.get("/dashboard/meetings/pastMeetings", (require, response) => {
  const infoRow = meetingDatabase.getAllPastMeeting();

  if (infoRow === undefined) {
    response.send({ status: 100, reason: "Meetings not found" });
  } else {
    response.send({ status: 200, infoRow: infoRow, reason: "success" });
  }
});

/*
 * get all info about all the meetings in the database, include meeting id, date, student name, teacher name, status,
 * staff type, student email, degree, year, comment, method, place
 *
 * Method: get
 * Path:
 *       /dashboard/meetings/allMeetings
 * Params:
 *       None
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       infoRow: string - Return the array of strings of each meeting info
 *       reason: string - Return description
 */
app.get("/dashboard/meetings/allMeetings", (require, response) => {
  const infoRow = meetingDatabase.getAllMeeting();

  if (infoRow === undefined) {
    response.send({ status: 100, reason: "Meetings not found" });
  } else {
    response.send({ status: 200, infoRow: infoRow, reason: "success" });
  }
});

/*
 * Insert a meeting into database
 *
 * Method: post
 * Path:
 *        /meetingmanagement/addMeeting
 * Params:
 *       date: String - Date the student choose
 *       studentName: String
 *       teacherId: string
 *       status: String - Status of the meeting, like (in schedule, finished)
 *       studentEmail: string
 *       degree: string
 *       year: string
 *       comment: string - The comment students leave to teacher
 *       method: string - The meeting method
 *       Place / links: string - The teacher’s office place or the link of the online meeting
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       replyMessage: string - The add success or not
 */
app.post("/dashboard/meetingmanagement/addMeeting", (require, response) => {
  const date = require.body.date;
  // const date = "Tue Feb 15 2022 09:00:00 GMT+1100 (Australian Eastern Daylight Time)";
  const studentName = require.body.studentName;
  const teacherId = require.body.teacherId;
  const status = require.body.status;
  const studentEmail = require.body.studentEmail;
  const year = parseInt(require.body.year);
  const degree = require.body.degree;
  const comment = require.body.comment;
  const method = require.body.method;
  const place = require.body.place;

  if (
    !date ||
    !studentName ||
    !teacherId ||
    !status ||
    !studentEmail ||
    !year ||
    !degree ||
    !method ||
    !place
  ) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    const ausDate = new Date(date);
    ausDate.setHours(ausDate.getHours() + 11);
    const addedDate = ausDate
      .toISOString()
      .replace(/T/, " ")
      .replace(/\..+/, "");
    console.log(addedDate);
    const updated = meetingDatabase.addMeeting(
      addedDate,
      studentName,
      teacherId,
      status,
      studentEmail,
      degree,
      year,
      comment,
      method,
      place
    );
    if (updated) {
      response.send({
        status: 200,
        replyMessage: "update successful",
      });
    } else {
      response.send({
        status: 100,
        replyMessage: "update failed",
      });
    }
  }
});

// ---------------------------------END Meeting-------------------------------------------------- //

// --------------------------------- ALL TEACHER TABLE-------------------------------------------------- //

app.get("/dashboard/meetings/getAllStaff", (require, response) => {
  const infoRow = meetingDatabase.getAllStaff();

  if (infoRow === undefined) {
    response.send({ status: 100, reason: "Teachers not found" });
  } else {
    response.send({ status: 200, infoRow: infoRow, reason: "success" });
  }
});

app.get("/teacher/allTeachersSimple", (require, response) => {
  const infoRow = meetingDatabase.getAllStaff();

  if (infoRow === undefined) {
    response.send({ status: 100, reason: "Teachers not found" });
  } else {
    response.send({ status: 200, infoRow: infoRow, reason: "success" });
  }
});

/*
 * Add a teacher into the system
 *
 * Method: post
 * Path:
 *       /teacher/addTeacher
 * Params:
 *       name: String - name
 *       email: String
 *       available: String - If the teacher is available this time
 *       officePlace: string - The office used for offline meetings
 *       time: string - The available time of the teacher
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       replyMessage: string - The add success or not
 */
app.post("/teacher/addTeacher", (require, response) => {
  const name = require.body.name;
  const email = require.body.email;
  const available = require.body.available;
  const officePlace = require.body.officePlace;

  // const time = require.body.time;

  if (!name || !email || !available || !officePlace) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    let addedAva = 0;
    if (available === "Yes") {
      addedAva = 1;
    }

    const updated = meetingDatabase.addStaff(
      name,
      email,
      addedAva,
      officePlace
    );
    if (updated) {
      response.send({
        status: 200,
        replyMessage: "update successful",
      });
    } else {
      response.send({
        status: 100,
        replyMessage: "update failed",
      });
    }
  }
});

// ---------------------------------UPDATE TEACHER TABLE-------------------------------------------------- //

/*
 * Update the teacher into the system
 *
 * Method: POST
 * Path:
 *       /teacher/updateTeacher
 * Params:
 *       teacherIds: String - The unique teacher id for each teacher
 *       name: String - name
 *       email: String
 *       available: String - If the teacher is available this time
 *       officePlace: string - The office used for offline meetings
 *       time: string - The available time of the teacher
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       replyMessage: string - The add success or not
 */
app.post("/teacher/updateTeacher", (require, response) => {
  const teacherId = require.body.teacherId;
  // const teacherIds = 1;

  const name = require.body.name;
  const email = require.body.email;
  const available = require.body.available;
  const officePlace = require.body.officePlace;

  const time = require.body.time;
  // const time = "2025-07-08T14:00:00,2025-07-12T15:00:00,2025-07-16T16:00:00,2025-07-20T17:00:00,2025-07-15T14:00:00";

  if (!teacherId || !name || !email || !available || !officePlace) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    let addedAva = 0;
    if (available === "Yes") {
      addedAva = 1;
    }
    const updatedInfo = meetingDatabase.updateStaff(
      teacherId,
      name,
      email,
      addedAva,
      officePlace
    );

    let timeUpdated = false;
    const timeCleared = meetingDatabase.deleteTime(teacherId);
    if (!timeCleared) {
      response.send({
        status: 100,
        replyMessage: "info updated: " + updatedInfo + ". Time updated failed.",
      });
    } else {
      if (time) {
        const timeList = time.split(",");
        timeList.forEach((element) => {
          const addedTime = element.replace(/T/, " ");
          timeUpdated = meetingDatabase.addTime(teacherId, addedTime);
        });
      }
      if (updatedInfo) {
        response.send({
          status: 200,
          replyMessage: "update successful",
        });
      } else {
        response.send({
          status: 100,
          replyMessage: "update failed",
        });
      }
    }
  }
});

/*
 * Get all teacher info
 *
 * Method: POST
 * Path:
 *       /teacher/getATeacherAllInfo
 * Params:
 *       teacherIds: String - The unique teacher id for each teacher
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       staff: string - Return the array of strings of each meeting info
 *       time: string - Return the array of strings of each meeting info
 *       replyMessage: string - The add success or not
 */
app.post("/teacher/getATeacherAllInfo", (require, response) => {
  const teacherId = require.body.teacherId;
  // const teacherId = 1;

  if (!teacherId) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    let staff = meetingDatabase.getStaff(teacherId);

    if (staff.available === 0) {
      staff.available = "No";
    } else {
      staff.available = "Yes";
    }

    const currentTime = new Date()
      .toISOString()
      .replace(/T/, " ")
      .replace(/\..+/, "");
    const timeDB = meetingDatabase.getAStaffUnselectedTime(
      teacherId,
      currentTime
    );
    let time = [];
    timeDB.forEach((element) => {
      let timeList = element.time.split(" ");
      const addedTime = timeList[0] + "T" + timeList[1];
      time.push(addedTime);
    });

    if (staff) {
      response.send({
        status: 200,
        staff: staff,
        time: time,
        replyMessage: "update successful",
      });
    } else {
      response.send({
        status: 100,
        replyMessage: "update failed",
      });
    }
  }
});

/*
 * Add time for a teacher
 *
 * Method: POST
 * Path:
 *       /teacher/addTeacherATime
 * Params:
 *       teacherId: String - The unique teacher id for each teacher
 *       time
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       replyMessage: string - The add success or not
 */
app.post("/teacher/addTeacherATime", (require, response) => {
  const teacherId = require.body.teacherId;
  // const teacherId = 1;

  let date = require.body.date;
  // const date = "2022-11-14";

  const time = require.body.time;
  // const time = "15,2,3";

  if (!teacherId) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    let updated = false;

    const timeInfo = time.split(",");
    const dayTime = parseInt(timeInfo[0]);

    const hour = parseInt(timeInfo[1]);
    const loop = parseInt(timeInfo[2]);
    for (let i = 0; i < loop; i++) {
      let addedDate = new Date(date);
      addedDate.setDate(addedDate.getDate() + i * 7 + 1);
      // console.log(addedDate);
      for (let j = 0; j < hour; j++) {
        // addedDate.setUTCHours(0, 0, 0,0);
        // console.log("hour setted: " + addedDate.toISOString());
        addedDate.setUTCHours(dayTime + j, 0, 0);
        // console.log("time:" + addedDate.getHours());
        const dbDate = addedDate
          .toISOString()
          .replace(/T/, " ")
          .replace(/\..+/, "");
        updated = meetingDatabase.addTime(teacherId, dbDate);
        console.log("added: " + dbDate);
      }
    }

    if (updated) {
      response.send({
        status: 200,
        replyMessage: "update successful",
      });
    } else {
      response.send({
        status: 100,
        replyMessage: "update failed",
      });
    }
  }
});

/*
 * delete a teacher from system
 *
 * Method: post
 * Path:
 *       /teacher/deleteATeacher
 * Params:
 *       teacherId: String - The unique id of the teacher
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       replyMessage: string - The add success or not
 */
app.post("/teacher/deleteATeacher", (require, response) => {
  const teacherId = require.body.teacherId;

  if (!teacherId) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    const updated = meetingDatabase.deleteStaff(teacherId);
    if (updated) {
      response.send({
        status: 200,
        replyMessage: "update successful",
      });
    } else {
      response.send({
        status: 100,
        replyMessage: "update failed",
      });
    }
  }
});

/*
 * delete teachers from system
 *
 * Method: post
 * Path:
 *       /teacher/deleteTeachers
 * Params:
 *       teacherIds: String - The array of teacher id need to be deleted
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       replyMessage: string - The add success or not
 */
app.post("/teacher/deleteTeachers", (require, response) => {
  const teacherIds = require.body.teacherIds;

  if (!teacherIds) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    var updated = false;
    teacherIds.forEach((element) => {
      updated = meetingDatabase.deleteStaff(element);
      console.log(element + " updated: " + updated);
    });
    if (updated) {
      response.send({
        status: 200,
        replyMessage: "update successful",
      });
    } else {
      response.send({
        status: 100,
        replyMessage: "update failed",
      });
    }
  }
});

// ---------------------------------CHATBOT TEACHER TABLE-------------------------------------------------- //
/*
 * get info about the teachers' type
 *
 * Method: get
 * Path:
 *       /teacher/getTeacherByType
 * Params:
 *
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       infoRow: string - Return the array of strings of each teacher's type
 *       reason: string - Return description
 */
app.get("/teacher/getTeacherList", (require, response) => {
  const infoRow = meetingDatabase.getStaffList();
  if (infoRow === undefined) {
    response.send({ status: 100, reason: "Teachers not found" });
  } else {
    response.send({ status: 200, infoRow: infoRow, reason: "success" });
  }
});

/*
 * get the time for specific teacher
 *
 * Method: post
 * Path:
 *       /teacher/getTeacherTime
 * Params:
 *       id: string - the id of the selected teacher
 * Return:
 *       Status code: int - Success code: 200, Error code: 100
 *       infoRow: string - Return the array of strings of the available time
 *       reason: string - Return description
 */
app.post("/teacher/getTeacherTime", (require, response) => {
  const id = require.body.id;

  if (!id) {
    response.send({
      status: 100,
      replyMessage: "please provide all required information",
    });
  } else {
    const currentTime = new Date()
      .toISOString()
      .replace(/T/, " ")
      .replace(/\..+/, "");
    const allTime = meetingDatabase.getAStaffUnselectedTime(id, currentTime);
    console.log(allTime);
    let infoRow = [];
    allTime.forEach((time) => {
      let timeList = time.time.split(" ");
      const addedTime = timeList[0] + "T" + timeList[1];
      infoRow.push(addedTime);
    });
    if (infoRow === undefined) {
      response.send({
        status: 100,
        reason: "timetable not found for the staff",
      });
    } else {
      response.send({ status: 200, infoRow: infoRow, reason: "success" });
    }
  }
});


app.get('/file/getZip', function(req, res, next) {
  //YMLCreate.generateNLU(nluDatabase.getAllUnits());
  var file_system = require('fs');
  var archiver = require('archiver');

  var output = file_system.createWriteStream('nlu.zip');
  var archive = archiver('zip');

  output.on('close', function () {
      console.log(archive.pointer() + ' total bytes');
      console.log('archiver has been finalized and the output file descriptor has closed.');
  });

  archive.on('error', function(err){
      throw err;
  });

  archive.pipe(output);

  // append files from a sub-directory, putting its contents at the root of archive
  archive.directory('./res/yml', false);

  // append files from a sub-directory and naming it `new-subdir` within the archive
  //archive.directory('./yml/data', 'data');

  archive.finalize();

  //var fileName = req.params.fileName;
  var filePath = path.join(__dirname, './res/yml/data/nlu.yml');
  var stats = fs.statSync(filePath);

  if(stats.isFile()){
    console.log(' the file existed ' + stats.size + ' sd is ' + filePath);
    res.set({
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': 'attachment; filename=nlu.yml',
      'Content-Length': stats.size
    });

    fs.createReadStream(filePath).pipe(res);
  } else {
    console.log(' the file does not existed');
    res.end(404);
  }

});

// ---------------------------------END CHATBOT TEACHER TABLE-------------------------------------------------- //

// ---------------------------------END TEACHER TABLE-------------------------------------------------- //
//
// app.get('/test', (require, response) => {
//     const date = "Tue Feb 15 2022 09:00:00 GMT+1100 (Australian Eastern Daylight Time)";
//     const ausDate = new Date(date);
//     ausDate.setHours(ausDate.getHours() + 11);
//     const addedDate = ausDate.toISOString().replace(/T/, ' ').replace(/\..+/, '');
//     console.log(addedDate);
// })

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
