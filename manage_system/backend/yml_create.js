
function YMLCreate(){

    /*
        * Create a zip file of the yml folder
        * Params:
        *       filePath: file path to the target compressed directory
    */
    this.generateZip = function () {
        var file_system = require('fs');
        var archiver = require('archiver');

        var output = file_system.createWriteStream('nlu.zip');
        var archive = archiver('zip');

        output.on('close', function () {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');
        });

        archive.on('error', function(err){
            throw err;
        });

        archive.pipe(output);

        // append files from a sub-directory, putting its contents at the root of archive
        archive.directory('./res/yml', false);

        // append files from a sub-directory and naming it `new-subdir` within the archive
        //archive.directory('./yml/data', 'data');

        archive.finalize();
        return;
    };

}

module.exports = YMLCreate;