//cosnt userDatabase =
//var assert = require('chai').assert;
const User_db = require("../user_db.js");
var userDatabase = new User_db("./Test/Database/user_info.db");
/*
var nluDatabase = new Nlu_db('./Database/NLU.db');
var meetingDatabase = new Meeting_db('./Database/meeting.db');*/
var assert = require("chai").assert;
var expect = require("chai").expect;

describe("user_db testing", () => {
  it("Get all users in the list initially ", () => {
    const infoRow = userDatabase.getUsers();
    assert.isArray(infoRow, "is array of users");
    //assert.lengthOf(infoRow, 4, "users length is 3");
  });

  it("Add a new user exist", () => {
    const username = "aurora";
    const password = "aaaaaa";
    const succes = userDatabase.addUser(username, password);
    //expect(succes).to.equal(true);
    assert.equal(succes, undefined, "add fail");
  });

  it("Get a specific user ", () => {
    const username = "aurora";
    var users = userDatabase.getUser(username);

    assert.equal(users.username, username);
    // password: "";
  });

  it("Get a specific user that not exist", () => {
    const username = "oli";
    var users = userDatabase.getUser(username);

    assert.equal(users, null);
    // password: "";
  });

  it("Update a user's password (user exist) ", () => {
    const username = "aurora";
    const password = "aaaa";
    const succes = userDatabase.updateUserPass(username, password);
    assert.equal(succes, true);
  });
  it("Update a user's password (user not exist) ", () => {
    const username = "auro";
    const password = "aaaa";
    const succes = userDatabase.updateUserPass(username, password);
    assert.equal(succes, false);
  });
  it("no db ", () => {
    const db = new User_db("");
    assert.equal(db.getUser(""), undefined);
    assert.equal(db.getUsers(""), undefined);
    assert.equal(db.createTable(), true);
  });
});

/*
  it("Update a user's password (user exist) ", () => {
    const username = "aurora";
    const password = "aaaa";
    const succes = userDatabase.updateUserPass(username, password);
    assert.equal(succes, true);
  });
  it("Update a user's password (user not exist) ", () => {
    const username = "auro";
    const password = "aaaa";
    const succes = userDatabase.updateUserPass(username, password);
    assert.equal(succes, false);
  });*/

/*
  it("Add a new user not exist", () => {
    const username = "aurora";
    const password = "aaaaaa";
    const succes = userDatabase.addUser(username, password);
    //expect(succes).to.equal(true);
    assert.equal(succes, true);
    
    expect(userDatabase.addUser(username)).to.equal({
      username: username,
      password: password,
    });
  });*/
