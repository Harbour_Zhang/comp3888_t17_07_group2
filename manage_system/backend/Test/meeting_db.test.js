//const meetingDatabase

const { assert } = require("chai");
const Meeting_db = require("../meeting_db.js");
const NLUDatabase = require("../nlu_db.js");
var meetingDatabase = new Meeting_db("./Test/Database/meeting.db");
var fakeDb = new Meeting_db("");
describe("meeting_db testing", () => {
  it("get all meeting information ", () => {
    const infoRow = meetingDatabase.getAllMeeting();
    assert.isArray(infoRow);
  });
  it("get all meeting information from empty db", () => {
    const infoRow = fakeDb.getAllMeeting();
    assert.equal(infoRow, undefined);
  });

  it("Get all future meetings ", () => {
    const infoRow = meetingDatabase.getAllFutureMeeting();
    assert.isArray(infoRow);
  });
  it("Get all future meetings from empty db", () => {
    const infoRow = fakeDb.getAllFutureMeeting();
    assert.equal(infoRow, undefined);
  });
  it("Get all past meetings ", () => {
    const infoRow = meetingDatabase.getAllPastMeeting();
    assert.isArray(infoRow);
  });
  it("Get all past meetings from empty db", () => {
    const infoRow = fakeDb.getAllPastMeeting();
    assert.equal(infoRow, undefined);
  });
  it("Add a new meetings successfully ", () => {
    //const date =
    //const studentName = require.body.studentName;
    //const teacherName = require.body.teacherName;
    //const status = require.body.status;
    //const stuffType = require.body.stuffType;
    //const studentEmail = require.body.studentEmail;
    //const year = parseInt(require.body.year);
    //const degree = require.body.degree;
    //const comment = require.body.comment;
    //const method = require.body.method;
    //const place = require.body.place;
    //updated = meetingDatabase.addMeeting(date, studentName, teacherName, status, stuffType, studentEmail, degree, year, comment, method, place);
  });
  it("Update Meeting status sussfully", () => {
    //const id
    //cosnt status
    //meetingDatabase.UpdateStatus
    //expect(true)
  });

  /*******Staff*******/
  it("Get all Staff information ", () => {
    const infoRow = meetingDatabase.getAllStaff();
    assert.isArray(infoRow);
  });
  it("Get all Staff information from empty db", () => {
    const infoRow = fakeDb.getAllStaff();
    assert.equal(infoRow, undefined);
  });

  it("Get a specific staff information ", () => {
    var allRows = meetingDatabase.getAllStaff();
    var min = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id < min.id) {
        min = allRows[i];
      }
    }
    const infoRow = meetingDatabase.getStaff(min.id);
    assert.isNotNull(infoRow.id);
    console.log(infoRow.id);
  });
  it("Get a specific staff information (not exist) ", () => {
    const infoRow = meetingDatabase.getStaff(-1);
    assert.equal(infoRow, undefined);
  });
  it("Get a specific staff information from empty db", () => {
    const infoRow = fakeDb.getStaff();
    assert.equal(infoRow, undefined);
  });

  it("Get all specific staff information  ", () => {
    const infoRow = meetingDatabase.getStaffList();
    assert.isArray(infoRow);
  });
  it("Get all specific staff information from empty db", () => {
    const infoRow = fakeDb.getStaffList();
    assert.equal(infoRow, undefined);
  });

  it("Add a staff", () => {
    const db = require("better-sqlite3")("./Test/Database/meeting.db", {
      fileMustExist: true,
      verbose: console.log,
    });
    const name = "addStaffTest";
    const email = "addStaffTest@gmail.com";
    const available = 0;
    const office = "home";
    const role = "administrator";
    //const sql1 = db.prepare("SELECT * FROM Staffs;");
    // console.log(sql.all());
    /*
    var staffs = meetingDatabase.getAllStaff();
    var target = null;
    for (var i = 0; i < staffs; i++) {
      if (staffs[i].name == name) {
        target = staffs[i];
      }
    }

    const sql2 = db.prepare(
      "INSERT INTO StaffsRole (id, name, role) VALUES (?, ?, ?)"
    );
    const trans = db.transaction((id, name, role) => {
      sql2.run(id, name, role);
    });
    trans(target.id, name, role);*/

    const infoRow = meetingDatabase.addStaff(name, email, available, office);
    assert.equal(infoRow, true);

    var allstaffs = meetingDatabase.getAllStaff();
    var target = null;
    for (var i = 0; i < allstaffs.length; i++) {
      if (allstaffs[i].name == "addStaffTest") {
        target = allstaffs[i];
      }
    }

    //const infoRow1 = meetingDatabase.deleteStaff(target.id);
    //assert.equal(infoRow1, undefined);
    /*
    var allRows = meetingDatabase.getAllStaff();
    var target = null;
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].name == name) {
        target = allRows[i];
      }
    }
    const id = target.id;
    const db = require("better-sqlite3")("./Test/Database/meeting.db", {
      fileMustExist: true,
      verbose: console.log,
    });
    const sql = db.prepare(
      "INSERT INTO StaffsRole (id, name, role) VALUES (?, ?, ?)"
    );
    const trans = db.transaction((id, name, role) => {
      sql.run(id, name, role);
    });*/
    /*
    assert.isNotNull(target);
    const staff = meetingDatabase.getStaff(target.id);
    assert.equal(staff.id, target.id);
    assert.equal(staff.name, name);
    assert.equal(staff.available, available);
    assert.equal(staff.office, office);

    const row = meetingDatabase.deleteStaff(target.id);
    assert.equal(row, true);*/
  });

  it("Add a staff in empty db ", () => {
    const infoRow = fakeDb.addStaff(
      "addStaffTest",
      "addStaffTest@gmail.com",
      0,
      "home"
    );
    assert.equal(infoRow, undefined);
  });
  it("Delete a staffin empty db ", () => {
    const infoRow = fakeDb.deleteStaff("addStaffTest");
    assert.equal(infoRow, undefined);
  });
  it("Delete a staffin empty db  not exist", () => {
    const infoRow = meetingDatabase.deleteStaff(-1);
    assert.equal(infoRow, undefined);
  });
  it("Delete a staff in empty db   exist", () => {
    var allstaffs = meetingDatabase.getAllStaff();
    var target = null;
    for (var i = 0; i < allstaffs.length; i++) {
      if (allstaffs[i].name == "addStaffTest") {
        target = allstaffs[i];
      }
    }

    //const infoRow = meetingDatabase.deleteStaff(target.id);
    //assert.equal(infoRow, undefined);
  });
  it("Add a new meetings ", () => {
    const date = "2021-10-14 23:00:00";
    const student = "oliver";
    const staff_id = "-1";
    const status = "avaialable";
    const email = "email";
    const degree = "Computer Science";
    const year = 3;
    const comment = "";
    const method = "zoom";
    const place = "home";

    const infoRow1 = meetingDatabase.addMeeting(
      date,
      student,
      staff_id,
      status,
      email,
      degree,
      year,
      comment,
      method,
      place
    );
  });

  it("Add a new meetings ", () => {
    const db = require("better-sqlite3")("./Test/Database/meeting.db", {
      fileMustExist: true,
      verbose: console.log,
    });
    var allRows = meetingDatabase.getAllStaff();
    var min = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id < min.id) {
        min = allRows[i];
      }
    }
    const date = "2021-10-14 23:00:00";
    const student = "oliver";
    const staff_id = min.id;
    const status = "avaialable";
    const email = "email";
    const degree = "Computer Science";
    const year = 3;
    const comment = "";
    const method = "zoom";
    const place = "home";

    const infoRow = meetingDatabase.addTime(staff_id, date);
    assert.equal(infoRow, true);

    const infoRow1 = meetingDatabase.addMeeting(
      date,
      student,
      staff_id,
      status,
      email,
      degree,
      year,
      comment,
      method,
      place
    );
    //assert.equal(true, false);
    const sql = db.prepare(
      "DELETE FROM TimeTable WHERE staff_id = ? and status == 'selected'"
    );
    const trans = db.transaction((staff_id) => {
      sql.run(staff_id);
    });
    trans(staff_id);
    //assert.equal(infoRow, true);
  });

  it("Update a meeting in fake db ", () => {
    const infoRow = fakeDb.updateMeetingStatus("", "");
    assert.equal(infoRow, undefined);
  });

  it("Update a meeting  ", () => {
    const allMeetings = meetingDatabase.getAllMeeting();
    var min = allMeetings[0];
    const currentTime = new Date()
      .toISOString()
      .replace(/T/, " ")
      .replace(/\..+/, "");
    console.log(currentTime);
    const infoRow = meetingDatabase.updateMeetingStatus(min.id, currentTime);
    //assert.equal(infoRow, undefined);
  });

  it("Update a staff information ", () => {
    var allRows = meetingDatabase.getAllStaff();
    var min = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id < min.id) {
        min = allRows[i];
      }
    }
    const name = min.name;
    const email = min.email;
    const available = min.available;
    const office = min.office;
    const id = min.id;

    const infoRow = meetingDatabase.updateStaff(
      id,
      name,
      email,
      available,
      office
    );
    //assert.equal(infoRow, true);
  });
  it("Update a staff information not exist", () => {
    var allRows = meetingDatabase.getAllStaff();
    var min = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id < min.id) {
        min = allRows[i];
      }
    }
    const name = -1;
    const email = min.email;
    const available = min.available;
    const office = min.office;
    const id = min.id;

    const infoRow = meetingDatabase.updateStaff(
      id,
      name,
      email,
      available,
      office
    );
    //assert.equal(infoRow, true);
  });

  it("Add a timetable with a given time - status - None and delete it", () => {
    var allRows = meetingDatabase.getAllStaff();
    var max = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id > max.id) {
        max = allRows[i];
      }
    }
    const staff_id = max.id;
    const date = "2021-10-14 15:00:00";
    const infoRow = meetingDatabase.addTime(staff_id, date);
    assert.equal(infoRow, true);

    const infoRow2 = meetingDatabase.deleteTime(staff_id);
    assert.equal(infoRow2, true);
  });
  it("Add a timetable with a given time - status - None (not exist staff)", () => {
    const staff_id = -1;
    const date = "2021-10-14 15:00:00";
    const infoRow = meetingDatabase.addTime(staff_id, date);
    assert.equal(infoRow, undefined);
  });

  it("Add a timetable with a given time - status - None  (have been existed)", () => {
    var allRows = meetingDatabase.getAllStaff();
    var min = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id < min.id) {
        min = allRows[i];
      }
    }
    const staff_id = min.id;
    const date = "2021-10-14 14:00:00";
    const infoRow = meetingDatabase.addTime(staff_id, date);
    assert.equal(infoRow, undefined);
  });

  it("Delete a staff's unselected time table (empty db)", () => {
    const infoRow = fakeDb.deleteTime(1);
    assert.equal(infoRow, undefined);
  });
  it("Add a timetable with a given time - status - None and delete it (update status)", () => {
    const db = require("better-sqlite3")("./Test/Database/meeting.db", {
      fileMustExist: true,
      verbose: console.log,
    });

    var allRows = meetingDatabase.getAllStaff();
    var max = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id > max.id) {
        max = allRows[i];
      }
    }
    const staff_id = max.id;
    const date = "2021-10-15 18:00:00";
    const infoRow = meetingDatabase.addTime(staff_id, date);
    assert.equal(infoRow, true);
    const currentTime = new Date()
      .toISOString()
      .replace(/T/, " ")
      .replace(/\..+/, "");
    console.log(currentTime);

    const sqlAllTime = db.prepare(
      "SELECT * FROM TimeTable WHERE staff_id = ? AND status != 'selected' "
    );
    console.log(sqlAllTime.all(staff_id));
    var allRows2 = sqlAllTime.all(staff_id);
    //meetingDatabase.getALLTime(currentTime);
    var target = null;
    console.log("length: " + allRows2.length);
    for (var i = 0; i < allRows2.length; i++) {
      if (allRows2[i].time == date) {
        target = allRows2[i];
      }
    }
    assert.equal(target.time, date);

    const infoRow2 = meetingDatabase.updateTimeStatus(target.id, date);
    assert.equal(infoRow2, true);

    const infoRow3 = meetingDatabase.deleteTime(staff_id);
    assert.equal(infoRow3, true);

    const infoRow4 = meetingDatabase.addTime(staff_id, date);
    assert.equal(infoRow4, undefined);

    const sql = db.prepare(
      "DELETE FROM TimeTable WHERE staff_id = ? and status == 'selected'"
    );
    const trans = db.transaction((staff_id) => {
      sql.run(staff_id);
    });
    trans(staff_id);
  });
  it("update timeStatus (empty db)", () => {
    const infoRow = fakeDb.updateTimeStatus(1, 1);
    assert.equal(infoRow, undefined);
  });
  it("update timeStus (invalid)", () => {
    const infoRow = meetingDatabase.updateTimeStatus("", "");
    assert.equal(infoRow, undefined);
  });
  it("update time (empty db)", () => {
    //const infoRow = fakeDb.updateTime(1, 1);
    //assert.equal(infoRow, undefined);
  });
  it("update time (invalid)", () => {
    //const infoRow = meetingDatabase.updateTime("-1", [""]);
    //assert.equal(infoRow, undefined);
  });
  it("update time valid", () => {
    /*
    var allRows = meetingDatabase.getAllStaff();
    var max = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id > max.id) {
        max = allRows[i];
      }
    }
    const staff_id = max.id;
    const date = "2021-10-15 18:00:00";
    const infoRow = meetingDatabase.addTime(staff_id, date);
    assert.equal(infoRow, true);

    const infoRow1 = meetingDatabase.updateTime(staff_id, [
      "2021-10-15 19:00:00",
    ]);
    assert.equal(infoRow1, true);
    const infoRow2 = meetingDatabase.deleteTime(staff_id);
    assert.equal(infoRow2, true);*/
  });

  it("Get all time periods with a given date (empty db)", () => {
    const infoRow = fakeDb.getALLTime();
    assert.equal(infoRow, undefined);
  });
  it("Get all time periods with a given date ", () => {
    const infoRow = meetingDatabase.getALLTime();
    assert.isArray(infoRow);
  });
  it("Get one staff's unselected time with a given date (empty db)", () => {
    const infoRow = fakeDb.getAStaffUnselectedTime("", "");
    assert.equal(infoRow, undefined);
  });
  it("Get one staff's unselected time with a given date ", () => {
    const currentTime = new Date()
      .toISOString()
      .replace(/T/, " ")
      .replace(/\..+/, "");
    var allRows = meetingDatabase.getAllStaff();
    var max = allRows[0];
    for (var i = 0; i < allRows.length; i++) {
      if (allRows[i].id > max.id) {
        max = allRows[i];
      }
    }
    const staff_id = max.id;
    const infoRow = meetingDatabase.getAStaffUnselectedTime(
      staff_id,
      currentTime
    );
    assert.isArray(infoRow);
  });
  it("Get unselected time period with a given date (empty db)", () => {
    const currentTime = new Date()
      .toISOString()
      .replace(/T/, " ")
      .replace(/\..+/, "");
    const infoRow = fakeDb.getALLUnselectedTime(currentTime);
    assert.equal(infoRow, undefined);
  });
  it("Get unselected time period with a given date", () => {
    const infoRow = meetingDatabase.getALLUnselectedTime();
    assert.isArray(infoRow);
  });
});
