//cosnt nluDatabase =
//var assert = require('chai').assert;

const Nlu_db = require("../nlu_db.js");

var nluDatabase = new Nlu_db("./Test/Database/NLU.db");
var assert = require("chai").assert;
describe("nlu_db testing", () => {
  it("Get information of all units, including their credits, link prohibition, prerequisites, year, etc ", () => {
    const infoRow = nluDatabase.getAllUnits();
    assert.isArray(infoRow, "is array of units");
  });

  it("Get all unit code", () => {
    const infoRow = nluDatabase.getUnitCode();
    assert.isArray(infoRow, "is array of units");
  });

  it("Get information of a specific unit (thst is not exist)", () => {
    const unit_id = "";
    const unit = nluDatabase.getUnit(unit_id);
    assert.equal(unit, null);
  });

  it("Get information of a specific unit (thst is  exist)", () => {
    const unit_id = "SOFT2412";
    const unit = nluDatabase.getUnit(unit_id);

    assert.equal(unit.unit_id, "SOFT2412");
  });

  it("Add  a unit into the database that is not exist", () => {
    const unit_id = "test1";
    const credits = 6;
    const major = "IS";
    const core = 2;
    const link = "link";
    const year = "2021";
    const prohibition = "COMP9412";
    const prerequisites = "None";
    const available = 1;
    assert.equal(nluDatabase.getUnit(unit_id), undefined);

    const addValid = nluDatabase.addUnit(
      unit_id,
      credits,
      major,
      core,
      link,
      year,
      prohibition,
      prerequisites,
      available
    );

    assert.equal(addValid, true);
    const updateValid = nluDatabase.updateUnit(
      unit_id,
      credits,
      major,
      core,
      link,
      year,
      prohibition,
      prerequisites,
      available
    );

    assert.equal(updateValid, true);

    assert.equal(nluDatabase.addStaff("oli", ["coordinator"]), true);

    var allStaffs = nluDatabase.getAllStaff();
    var targetStaff = null;
    for (var i = 0; i < allStaffs.length; i++) {
      if (allStaffs[i].name === "oli") {
        targetStaff = allStaffs[i];
      }
    }
    assert.equal(targetStaff.name, "oli");

    assert.equal(nluDatabase.addCoordinator(unit_id, targetStaff.name), true);

    assert.isNotNull(nluDatabase.getUnit("test1"));
    assert.equal(nluDatabase.deleteUnit(unit_id), true);
    assert.equal(nluDatabase.deleteStaff(targetStaff.id), true);

    const db = require("better-sqlite3")("./Test/Database/NLU.db", {
      fileMustExist: true,
      verbose: console.log,
    });
    try {
      const sql = db.prepare("DELETE FROM UnitCoordinator WHERE unit_id = ?");
      const trans = db.transaction((unit_id) => {
        sql.run(unit_id);
      });
      trans(unit_id);
    } catch (err) {
      console.log("extra: " + err);
    }
  });

  it("Delete a unit from the database that not exist", () => {
    const unit_id = "test_delete";

    assert.equal(nluDatabase.deleteUnit(unit_id), undefined);
    /*
    const unit_id;
    const credits;
    const major;
    const core;
    constlink;
    const year;
    const prohibition;
    const prerequisites;
    const available;
expect(nluDatabase.getUnit(unit_id)).to.euqal({
        unit_id :unit_id,
        credits :credits,
        major:major,
        core:core,
        link:link,
        year:year,
        prohibition:prohibition,
        prerequisites:prerequisites,
        available:available,
        });
    nluDatabase.deleteUnit(unit_id);
    expect(nluDatabase.getUnit(unit_id)).to.euqal(null);
    */
  });

  it("Add  a unit into the database that is exist", () => {
    const unit_id = "SOFT2412";
    const credits = 6;
    const major = "IS";
    const core = 2;
    const link = "link";
    const year = "2021";
    const prohibition = "COMP9412";
    const prerequisites = "None";
    const available = 1;
    assert.notEqual(nluDatabase.getUnit(unit_id), null);
    //expect().to.euqal(null);

    const addValid = nluDatabase.addUnit(
      unit_id,
      credits,
      major,
      core,
      link,
      year,
      prohibition,
      prerequisites,
      available
    );
    assert.equal(addValid, undefined);
    /*

  const unit = nluDatabase.getUnit(unit_id);
  assert.equal(unit.unit_id, unit_id);
  assert.equal(unit.major, major);
  assert.equal(unit.credits, credits);
  assert.equal(unit.core, core);
  assert.equal(unit.link, link);
  assert.equal(unit.year, year);
  assert.equal(unit.prohibition, prohibition);
  assert.equal(unit.prerequisites, prerequisites);
  assert.equal(unit.available, available);

  assert.equal(nluDatabase.deleteUnit(unit_id), true);*/
  });

  it("Get all units and their major ", () => {
    const infoRow = nluDatabase.getAllUnitsAndMajor();
    assert.isArray(infoRow, "is array of units");
  });

  it("Get a specific unit and its major that not exist", () => {
    const infoRow = nluDatabase.getUnitAndMajor("unit_test");
    assert.equal(infoRow, undefined);
  });

  it("Get a specific unit and its major that exist", () => {
    const infoRow = nluDatabase.getUnitAndMajor("SOFT2412");
    assert.equal(infoRow.unit_id, "SOFT2412");
  });

  it("Add and delete major to a existed unit", () => {
    const infoRow = nluDatabase.addUnitMajor("COMP2017", "test_major");
    assert.equal(infoRow, true);

    var all = nluDatabase.getAllUnitsAndMajor();
    var comp2017 = null;
    for (var i = 0; i < all.length; i++) {
      if (all[i].major === "test_major") {
        comp2017 = all[i];
      }
    }
    assert.isNotNull(comp2017);
    assert.equal(comp2017.unit_id, "COMP2017");
    assert.equal(
      nluDatabase.updateUnitMajor("COMP2017", "test_major"),
      undefined
    );
    assert.equal(
      nluDatabase.updateUnitMajor("COMP2017", "test_major2"),
      undefined
    );
    assert.equal(nluDatabase.deleteUnitMajor("COMP2017"), true);

    nluDatabase.addUnitMajor("COMP2017", "CS");
  });

  it("Add  major to a not existed unit", () => {
    const infoRow = nluDatabase.addUnitMajor("fake_unit", "test_major");
    assert.equal(infoRow, undefined);
  });

  it("Dlete major to a not existed unit", () => {
    const infoRow = nluDatabase.deleteUnitMajor("fake_unit");
    assert.equal(infoRow, true);
  });

  it("Add major to a existed unit that not exist", () => {
    const infoRow = nluDatabase.deleteUnitMajor("unit_test");
    assert.equal(infoRow, true);
  });

  it("Delete a unit and its major", () => {
    const infoRow = nluDatabase.deleteUnitMajor("unit_test");
    assert.equal(infoRow, true);
  });

  it("Delete a unit and its major", () => {
    /*
  const infoRow = nluDatabase.deleteUnitMajor("SOFT2412");
  assert.equal(infoRow, true);*/
  });

  it("Update a unit and its major", () => {
    /*
  const infoRow = nluDatabase.updateUnitMajor("COMP2017", "CS");
  assert.equal(infoRow, true);*/
  });

  /************Degree***************/
  it("Get all degree questions", () => {
    const infoRow = nluDatabase.getAllDegree();
    assert.isArray(infoRow);
  });

  it("Get a degree question exist", () => {
    const allDegree = nluDatabase.getAllDegree();
    var min = allDegree[0];
    for (var i = 0; i < allDegree.length; i++) {
      if (min.id > allDegree[i].id) {
        min = allDegree[i];
      }
    }
    const infoRow = nluDatabase.getDegree(min.id);
    assert.notEqual(infoRow, undefined);
    assert.equal(infoRow.type, "degree");
  });

  it("Get a degree question not exist", () => {
    const infoRow = nluDatabase.getDegree(-1);
    assert.equal(infoRow, undefined);
  });

  it("Add a new degree question", () => {
    const infoRow = nluDatabase.addDegree("Dtest_q", "Dtest_a");
    assert.equal(infoRow, true);
    const allDegree = nluDatabase.getAllDegree();
    var max = allDegree[0];
    for (var i = 0; i < allDegree.length; i++) {
      if (max.id < allDegree[i].id) {
        max = allDegree[i];
      }
    }
    const degree = nluDatabase.getDegree(max.id);
    assert.equal(degree.question, "Dtest_q");
    assert.equal(degree.answer, "Dtest_a");
    assert.equal(degree.type, "degree");
  });

  it("Delete a degree not exist", () => {
    const infoRow2 = nluDatabase.deleteDegree(-1);
    assert.equal(infoRow2, undefined);
  });
  it("Delete a degree  exist", () => {
    const allDegree = nluDatabase.getAllDegree();
    var min = allDegree[0];
    for (var i = 0; i < allDegree.length; i++) {
      if (min.id > allDegree[i].id) {
        min = allDegree[i];
      }
    }
    const infoRow2 = nluDatabase.deleteDegree(min.id);
    assert.equal(infoRow2, true);
  });

  it("Update a degree question not exist", () => {
    const infoRow2 = nluDatabase.updateDegree(-1, "a", "new");
    assert.equal(infoRow2, undefined);
  });

  it("Update a degree question exist", () => {
    const allDegree = nluDatabase.getAllDegree();
    var min = allDegree[0];
    for (var i = 0; i < allDegree.length; i++) {
      if (min.id > allDegree[i].id) {
        min = allDegree[i];
      }
    }
    const infoRow2 = nluDatabase.updateDegree(min.id, "a", "new");

    assert.equal(infoRow2, true);
  });

  /************Honours***************/
  it("Get all honours questions", () => {
    const infoRow = nluDatabase.getAllHonour();
    assert.isArray(infoRow);
  });

  it("Get a honour question exist", () => {
    const all = nluDatabase.getAllHonour();
    var min = all[0];
    for (var i = 0; i < all.length; i++) {
      if (min.id > all[i].id) {
        min = all[i];
      }
    }
    const infoRow = nluDatabase.getHonour(min.id);
    assert.notEqual(infoRow, undefined);
    assert.equal(infoRow.type, "honour");
  });

  it("Get a degree question not exist", () => {
    const infoRow = nluDatabase.getHonour(-1);
    assert.equal(infoRow, undefined);
  });

  it("Add a new honour question", () => {
    const infoRow = nluDatabase.addHonour("Htest_q", "Htest_a");
    assert.equal(infoRow, true);
    const all = nluDatabase.getAllHonour();
    var max = all[0];
    for (var i = 0; i < all.length; i++) {
      if (max.id < all[i].id) {
        max = all[i];
      }
    }
    const row = nluDatabase.getHonour(max.id);
    assert.equal(row.question, "Htest_q");
    assert.equal(row.answer, "Htest_a");
    assert.equal(row.type, "honour");
  });

  it("Delete a honour not exist", () => {
    const infoRow2 = nluDatabase.deleteHonour(-1);
    assert.equal(infoRow2, undefined);
  });

  it("Delete a honour  exist", () => {
    const all = nluDatabase.getAllHonour();
    var min = all[0];
    for (var i = 0; i < all.length; i++) {
      if (min.id > all[i].id) {
        min = all[i];
      }
    }
    const infoRow2 = nluDatabase.deleteHonour(min.id);
    assert.equal(infoRow2, true);
  });

  it("Update a honour question not exist", () => {
    const infoRow2 = nluDatabase.updateHonour(-1, "a", "new");
    assert.equal(infoRow2, undefined);
  });

  it("Update a honour question exist", () => {
    const all = nluDatabase.getAllHonour();
    var min = all[0];
    for (var i = 0; i < all.length; i++) {
      if (min.id > all[i].id) {
        min = all[i];
      }
    }
    const infoRow2 = nluDatabase.updateHonour(min.id, "H", "h");
    assert.equal(infoRow2, true);
  });

  /************FAQ***************/
  it("Get all FAQ questions", () => {
    const infoRow = nluDatabase.getAllFAQ();
    assert.isArray(infoRow);
  });

  it("Get a FAQ question exist", () => {
    const all = nluDatabase.getAllFAQ();
    //assert.equal(all[1], undefined);

    var min = all[0];
    for (var i = 0; i < all.length; i++) {
      if (min.id > all[i].id) {
        min = all[i];
      }
    }
    const infoRow = nluDatabase.getFAQId(min.id);
    assert.isNotNull(infoRow);
    assert.equal(infoRow.type, "faq");
  });

  it("Get a FAQ question not exist", () => {
    const infoRow = nluDatabase.getFAQ(-1);
    assert.equal(infoRow, undefined);
  });

  it("Add a new FAQ question", () => {
    const infoRow = nluDatabase.addFAQ("Ftest_q", "Ftest_a");

    assert.equal(infoRow, true);
    const all = nluDatabase.getAllFAQ();
    var max = all[0];
    for (var i = 0; i < all.length; i++) {
      if (all[0].question == "Ftest_q") {
        max = all[i];
      }
    }
    console.log("aaaa" + max.id);
    assert.equal(nluDatabase.updateFAQ(max.id, "Ftest_q", "Ftest_a"), true);

    const row = nluDatabase.getFAQ(max.question);
    assert.isNotNull(row);
    assert.equal(nluDatabase.deleteFAQ(row.id), true);
  });

  it("Delete a FAQ not exist", () => {
    const infoRow = nluDatabase.deleteFAQ(-1);
    assert.equal(infoRow, undefined);
  });

  it("Update a FAQ question not exist", () => {
    const infoRow2 = nluDatabase.updateFAQ(-1, "a", "new");
    assert.equal(infoRow2, undefined);
  });

  it("Update a FAQ question exist", () => {
    const all = nluDatabase.getAllFAQ();
    var min = all[0];
    for (var i = 0; i < all.length; i++) {
      if (min.id > all[i].id) {
        min = all[i];
      }
    }
    /*
  console.log(min.id);
  assert.equal(nluDatabase.getFAQ(min.id), undefined);
  const infoRow2 = nluDatabase.updateFAQ(min.id, "F", "f");
  assert.equal(infoRow2, true);*/
  });

  /************Staff***************/
  it("Get all Staff information", () => {
    const infoRow = nluDatabase.getAllStaff();
    assert.isArray(infoRow);
  });
  it("Delete role from StaffRole table", () => {
    const row = nluDatabase.addStaff("deleteRole", ["coordinator"]);
    assert.equal(row, true);

    let staffs = nluDatabase.getAllStaff();
    let oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "deleteRole") {
        oliver = staffs[i];
      }
    }

    assert.equal(oliver.name, "deleteRole");
    assert.equal(oliver.role, ["coordinator"]);

    assert.equal(nluDatabase.addRole(oliver.id, oliver.id, ["d"]), undefined);
    assert.equal(
      nluDatabase.addRole(oliver.id, oliver.name, ["coordinator"]),
      true
    );
    /*
  assert.equal(nluDatabase.deleteRole(oliver.id, "coordinator"), undefined);

  assert.equal(
    nluDatabase.addRole(oliver.id, oliver.name, ["coordinator"]),
    true
  );*/
    assert.equal(nluDatabase.deleteStaff(oliver.id), true);
    staffs = nluDatabase.getAllStaff();
    oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "deleteRole") {
        oliver = staffs[i];
      }
    }
    assert.equal(oliver, null);
    assert.equal(nluDatabase.deleteRole(";", ";"));
  });

  it("Update a staff information", () => {
    const infoRow = nluDatabase.updateStaff(
      "33",
      ["coordinator", "directory"],
      "Nguyen Tran"
    );

    assert.equal(infoRow, true);
    const infoRow2 = nluDatabase.updateStaff(
      "33",
      ["coordinator"],
      "Nguyen Tran"
    );

    assert.equal(infoRow2, true);
  });

  it("Get a specific staff information", () => {
    const all = nluDatabase.getAllStaff();
    var min = all[0];
    for (var i = 0; i < all.length; i++) {
      min = all[i];
      const infoRow = nluDatabase.getStaff(min.id);
      assert.equal(infoRow.name, min.name);
      assert.equal(infoRow.phone, min.phone);
      assert.equal(infoRow.email, min.email);
      assert.equal(infoRow.office, min.office);
    }
  });

  it("Add a new Staff and delete that", () => {
    const row = nluDatabase.addStaff("oliver1", ["coordinator"]);
    assert.equal(row, true);

    let staffs = nluDatabase.getAllStaff();
    let oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver1") {
        oliver = staffs[i];
      }
    }

    assert.equal(oliver.name, "oliver1");
    //assert.equal(oliver.role, ["coordinaror"]);

    assert.equal(nluDatabase.deleteStaff(oliver.id), true);
    staffs = nluDatabase.getAllStaff();
    oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver1") {
        oliver = staffs[i];
      }
    }
    assert.equal(oliver, null);
  });

  it("Add a new Staff and delete that", () => {
    const row = nluDatabase.addStaff("oliver", ["coordinator"]);
    assert.equal(row, true);

    let staffs = nluDatabase.getAllStaff();
    let oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver") {
        oliver = staffs[i];
      }
    }

    assert.equal(oliver.name, "oliver");
    assert.equal(oliver.role, ["coordinator"]);

    assert.equal(nluDatabase.addRole(oliver.id, oliver.id, ["d"]), undefined);
    assert.equal(nluDatabase.addRole("a", "c", ["d"]), undefined);

    assert.equal(nluDatabase.deleteStaff(oliver.id), true);
    staffs = nluDatabase.getAllStaff();
    oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver") {
        oliver = staffs[i];
      }
    }
    assert.equal(oliver, null);
  });

  it("delete staff not exits", () => {
    assert.equal(nluDatabase.deleteStaff(-1), undefined);
  });

  it("Update a staff information not exist", () => {
    assert.equal(nluDatabase.updateCoordinator("a", "c"), undefined);
    // const row = nluDatabase.addStaff("oliver", ["coordinator"]);
    //assert.equal(row, true);
    /*
  let staffs = nluDatabase.getAllStaff();
  let oliver = null;
  for (var i = 0; i < staffs.length; i++) {
    console.log("name " + staffs[i].name);

    if (staffs[i].name === "oliver") {
      oliver = staffs[i];
    }
  }

  assert.equal(oliver.name, "oliver");
  assert.equal(oliver.role, ["coordinator"]);

  let min = oliver;

  const name = min.name;
  const phone = min.phone;
  const email = min.email;
  const office = min.office;
  const infoRow2 = nluDatabase.getStaff(min.id);
  assert.equal(infoRow2.name, min.name);
  assert.equal(infoRow2.phone, min.phone);
  assert.equal(infoRow2.email, min.email);
  assert.equal(infoRow2.office, min.office);
  nluDatabase.updateStaff(min.id, ["None"], "oliver");

  staffs = nluDatabase.getAllStaff();
  oliver = null;
  for (var i = 0; i < staffs.length; i++) {
    console.log("name " + staffs[i].name);

    if (staffs[i].name === "oliver") {
      oliver = staffs[i];
    }
  }

  const new_oliver = nluDatabase.getStaff(oliver.id);
  assert.equal(new_oliver.name, "oliver");
  assert.equal(new_oliver.role, ["None"]);

  assert.equal(nluDatabase.deleteStaff(new_oliver.id), true);*/
  });

  it("Update a staff information not exist", () => {
    assert.equal(nluDatabase.updateStaff(-1, "a", "n"), undefined);
  });

  it("Get all Role information", () => {
    const infoRow = nluDatabase.getAllRole();
    assert.isArray(infoRow);
  });

  it("Add a new Staff ,getRole getALLRole and delete that", () => {
    const row = nluDatabase.addStaff("oliver", ["coordinator"]);
    assert.equal(row, true);

    let staffs = nluDatabase.getAllStaff();
    let oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver") {
        oliver = staffs[i];
      }
    }

    assert.equal(oliver.name, "oliver");
    assert.equal(oliver.role, ["coordinator"]);

    //getAllRole
    let roles = nluDatabase.getAllRole();
    let oliverRole = null;
    for (var i = 0; i < roles.length; i++) {
      if (roles[i].name === "oliver") {
        oliverRole = roles[i];
      }
    }
    assert.equal(oliverRole.role, ["coordinator"]);

    //getRole
    let oliverRole1 = nluDatabase.getRole(oliverRole.id);
    assert.equal(oliverRole1.name, "oliver");
    assert.equal(oliverRole1.role, ["coordinator"]);

    assert.equal(nluDatabase.deleteStaff(oliver.id), true);
    staffs = nluDatabase.getAllStaff();
    oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver") {
        oliver = staffs[i];
      }
    }
    assert.equal(oliver, null);
  });

  it("Add a new role to an existed staff(id not exist)", () => {
    assert.equal(nluDatabase.addRole(-1), undefined);
  });

  it("Delete role from StaffRole table (id not exist)", () => {
    assert.equal(nluDatabase.deleteRole(-1), undefined);
  });

  /*************Coordinator**************/

  it("Get all Unit Coordinator Information", () => {
    const infoRow = nluDatabase.getAllUnitsCoordinator();
    assert.isArray(infoRow, "is array of units");
  });
  it("Get all information of a staff who is coordinator (The coordinator is need to be assigned with a unit)", () => {
    const infoRow = nluDatabase.getStaffCoordinator();
    assert.isArray(infoRow, "is array of units");
  });

  it("Get all information of a staff who is coordinator (The coordinator is need to be assigned with a unit)", () => {
    const infoRow = nluDatabase.getAllUnitsCoordinator();
    assert.isArray(infoRow, "is array of units");
  });

  it("Add a new Staff ,getRole getALLRole ,add Coordinator and delete that", () => {
    const row = nluDatabase.addStaff("oliver", ["coordinator"]);
    assert.equal(row, true);

    let staffs = nluDatabase.getAllStaff();
    let oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver") {
        oliver = staffs[i];
      }
    }

    assert.equal(oliver.name, "oliver");
    assert.equal(oliver.role, ["coordinator"]);

    //getAllRole
    let roles = nluDatabase.getAllRole();
    let oliverRole = null;
    for (var i = 0; i < roles.length; i++) {
      if (roles[i].name === "oliver") {
        oliverRole = roles[i];
      }
    }
    assert.equal(oliverRole.role, ["coordinator"]);

    //getRole
    let oliverRole1 = nluDatabase.getRole(oliverRole.id);
    assert.equal(oliverRole1.name, "oliver");
    assert.equal(oliverRole1.role, ["coordinator"]);

    //Basem Fathi Suleiman
    var coordinator = nluDatabase.getUnitCoordinator("SOFT2412");
    assert.equal(coordinator.name, "Basem Fathi Suleiman");

    nluDatabase.addCoordinator("SOFT2412", "oliver");
    //coordinator = nluDatabase.getUnitCoordinator("SOFT2412");
    //assert.equal(coordinator.name, "oliver");
    nluDatabase.addCoordinator("SOFT2412", "Basem Fathi Suleiman");
    coordinator = nluDatabase.getUnitCoordinator("SOFT2412");
    assert.equal(coordinator.name, "Basem Fathi Suleiman");

    assert.equal(nluDatabase.deleteStaff(oliver.id), true);
    staffs = nluDatabase.getAllStaff();
    oliver = null;
    for (var i = 0; i < staffs.length; i++) {
      //console.log("name " + staffs[i].id);

      if (staffs[i].name === "oliver") {
        oliver = staffs[i];
      }
    }
    assert.equal(oliver, null);
  });
  it("  Update the unit coordinator ", () => {
    nluDatabase.updateCoordinator("SOFT2412", "Alexander Fish");
    nluDatabase.updateCoordinator("SOFT2412", "Basem Fathi Suleiman");
  });
  it(" Check Coordinator", () => {
    assert.equal(nluDatabase.checkCoordinator("Basem Fathi Suleiman"), true);
    assert.equal(nluDatabase.checkCoordinator("fake_coordinator"), undefined);
  });

  it(" not db", () => {
    const db = new Nlu_db("");

    assert.equal(db.getAllUnitsCoordinator(), undefined);
    assert.equal(db.getStaffCoordinator(), undefined);
    assert.equal(db.getCoordinator("a"), undefined);
    assert.equal(db.getUnitCoordinator("SOFT2412"), undefined);

    assert.equal(db.addStaff("aa", "aa"), undefined);
    assert.equal(db.deleteStaff("aa"), undefined);
    assert.equal(db.deleteRole("a", "c", "d"), undefined);
    assert.equal(db.checkCoordinator("aa", "a"), undefined);
    assert.equal(db.addRole("aa", "a", "c"), undefined);
    assert.equal(db.getRole("aa"), undefined);
    assert.equal(db.getUnitAndMajor(), undefined);
    assert.equal(db.getAllRole(), undefined);
    assert.equal(db.getAllStaff(), undefined);

    assert.equal(db.addHonour("a", "n"), undefined);
    assert.equal(db.addDegree("a", "n"), undefined);
    assert.equal(db.addFAQ("a", "n"), undefined);

    assert.equal(db.getFAQ("a"), undefined);
    assert.equal(db.getFAQId("a"), undefined);
    assert.equal(db.getHonour("a"), undefined);
    assert.equal(db.getDegree("a"), undefined);
    assert.equal(db.getHonour("a"), undefined);

    assert.equal(db.getAllHonour(), undefined);
    assert.equal(db.getAllFAQ(), undefined);
    assert.equal(db.getAllDegree(), undefined);

    assert.equal(db.getUnit("a"), undefined);
    assert.equal(db.getUnitCode(), undefined);
    assert.equal(db.getAllUnits("a"), undefined);
    assert.equal(db.getAllUnitsAndMajor(), undefined);

    assert.equal(nluDatabase.updateUnit("a"), undefined);
    assert.equal(db.updateStaff("33", undefined, "Nguyen Tran"), undefined);
  });
});
