/*
 * This file mainly works with NLU_db
 */

function NLUDatabase(filepath) {
  // Import Database
  const db = require("better-sqlite3")(filepath, {
    fileMustExist: true
  });

  /*
   * Get information of all units, including their credits, link prohibition, prerequisites, year, etc
   * Return:
   *  [{
          unit_id: 'unit_id',
          core: 0,
          credit: 6,
          prerequisites: 'None',
          prohibition: '',
          link: 'https://www.sydney.edu.au',
          available: 1,
          staff: 'name'
      }]
   */
  this.getAllUnits = function () {
    try {
      const sql = db.prepare(
        "SELECT * FROM (Units NATURAL JOIN UnitCoordinator) NATURAL JOIN Major"
      );
      return sql.all();
    } catch (err) {
      console.log("getAllUnits: " + err);
    }
    return undefined;
  };

  /*
   * Get all unit code
   */
  this.getUnitCode = function () {
    try {
      const sql = db.prepare("SELECT unit_id FROM Units");
      return sql.all();
    } catch (err) {
      console.log("getUnitCode: " + err);
    }
    return undefined;
  };

  /*
   * Get information of a specific unit
   *
   * Parameter:
   *  unit_id - the id of a unit
   *
   * Return:
   *  [{
          unit_id: 'unit_id',
          core: 0,
          credit: 6,
          prerequisites: 'None',
          prohibition: '',
          link: 'https://www.sydney.edu.au',
          available: 1,
          staff: 'name'
      }]
   */
  this.getUnit = function (unit_id) {
    try {
      const sql = db.prepare(
        "SELECT * FROM (Units u NATURAL JOIN UnitCoordinator uc) NATURAL JOIN Major m WHERE u.unit_id = ?"
      );
      return sql.get(unit_id);
    } catch (err) {
      console.log("getUnits: " + err);
    }
    return undefined;
  };

  /*
   * The function updates the value of units
   *
   * The function assumes that all parameters are valid
   * The parameters should be undefined if no update is needed
   *
   * Parameter:
   *  unit_id - must include
   *  credit - int
   *  major - 'CS', 'IS', 'SE', 'CDS', "None"
   *  core - 1 / 0
   *  link - string
   *  year - int
   *  prohibition - string
   *  prerequisites - string
   *  available - 1 / 0
   *
   * Return
   *  true - success
   *  undefined - fail
   */
  this.updateUnit = function (
    unit_id,
    credits,
    major,
    core,
    link,
    year,
    prohibition,
    prerequisites,
    available
  ) {
    try {
      if (this.getUnit(unit_id) === undefined) {
        return undefined;
      }

      // Update Units Table
      let sql, trans;
      sql = db.prepare(
        "UPDATE Units SET credit = @credits, core = @core, link = @link, year = @year, " +
          "prohibition = @prohibition, prerequisites = @prerequisites, available = @available WHERE unit_id = @unit_id"
      );

      trans = db.transaction(
        (
          unit_id,
          credits,
          core,
          link,
          year,
          prohibition,
          prerequisites,
          available
        ) => {
          sql.run({
            credits: credits,
            core: core,
            link: link,
            year: year,
            prohibition: prohibition,
            prerequisites: prerequisites,
            available: available,
            unit_id: unit_id,
          });
        }
      );

      trans(
        unit_id,
        credits,
        core,
        link,
        year,
        prohibition,
        prerequisites,
        available
      );

      // Updates Major table
      this.updateUnitMajor(unit_id, major);

      return true;
    } catch (err) {
      console.log("update_unit error: " + err);
    }
    return undefined;
  };

  /*
   * Add a unit into the database
   */
  this.addUnit = function (
    unit_id,
    credits,
    major,
    core,
    link,
    year,
    prohibition,
    prerequisites,
    available
  ) {
    try {
      if (this.getUnit(unit_id) !== undefined) {
        return undefined;
      }

      // Add to Units table
      let sql = db.prepare(
        "INSERT INTO Units(unit_id, core, credit, prerequisites, prohibition, link, available, year)" +
          "VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
      );

      let trans = db.transaction(
        (
          unit_id,
          core,
          credits,
          prerequisites,
          prohibition,
          link,
          available,
          year
        ) => {
          sql.run(
            unit_id,
            core,
            credits,
            prerequisites,
            prohibition,
            link,
            available,
            year
          );
        }
      );

      trans(
        unit_id,
        core,
        credits,
        prerequisites,
        prohibition,
        link,
        available,
        year
      );

      // Add to Major table
      this.addUnitMajor(unit_id, major);

      // Add a None coordinator
      this.addCoordinator(unit_id, "None");

      return true;
    } catch (err) {
      console.log("addUnit error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a unit from the database
   */
  this.deleteUnit = function (unit_id) {
    try {
      if (this.getUnit(unit_id) == undefined) {
        return undefined;
      }
      const sql = db.prepare("DELETE FROM Units WHERE unit_id = ?");
      const trans = db.transaction((id) => {
        sql.run(id);
      });
      trans(unit_id);

      // Foreign key constraints -> no need to worry about delete unit from Major table
      return true;
    } catch (err) {
      console.log("deleteUnit error: " + err);
    }
    return undefined;
  };

  /*
   * Get all units and their major
   */
  this.getAllUnitsAndMajor = function () {
    try {
      const sql = db.prepare("SELECT * FROM Major");
      return sql.all();
    } catch (err) {
      console.log("getAllUnitsAndMajor: " + err);
    }
    return undefined;
  };

  /*
   * Get a specific unit and its major
   */
  this.getUnitAndMajor = function (unit_id) {
    try {
      const sql = db.prepare("SELECT * FROM Major WHERE unit_id = ?");
      return sql.get(unit_id);
    } catch (err) {
      console.log("getUnitAndMajor: " + err);
    }
    return undefined;
  };

  /*
   * Add major to a existed unit
   */
  this.addUnitMajor = function (unit_id, major) {
    try {
      // Check whether the Major table contains this unit
      if (this.getUnitAndMajor(unit_id) !== undefined) {
        this.deleteUnitMajor(unit_id);
      }
      const sql = db.prepare(
        "INSERT INTO Major (unit_id, major) VALUES (?, ?)"
      );
      const trans = db.transaction((unit_id, major) => {
        sql.run(unit_id, major);
      });
      trans(unit_id, major);
      return true;
    } catch (err) {
      console.log("addUnitMajor error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a unit and its major
   */
  this.deleteUnitMajor = function (unit_id) {
    try {
      // Check whether the Major table contains this unit
      if (this.getUnitAndMajor(unit_id) === undefined) {
        return true;
      }
      const sql = db.prepare("DELETE FROM Major WHERE unit_id = ?");
      const trans = db.transaction((unit_id) => {
        sql.run(unit_id);
      });
      trans(unit_id);
      return true;
    } catch (err) {
      console.log("deleteUnitMajor error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a unit and its major
   */
  this.updateUnitMajor = function (unit_id, major) {
    try {
      // Check whether the Major table contains this unit
      if (this.getUnitAndMajor(unit_id) === undefined) {
        this.addUnitMajor(unit_id, major);
      }
      const sql = db.prepare(
        "UPDATE Units SET major = @major WHERE unit_id = @unit_id"
      );
      const trans = db.transaction((unit_id, major) => {
        sql.run({ unit_id: unit_id, major: major });
      });
      trans(unit_id, major);
      return true;
    } catch (err) {
      console.log("updateUnitMajor error: " + err);
    }
    return undefined;
  };

  /*
   * Add a new degree question
   * No need to pass the id, the id in the database increase automatically
   */
  this.addDegree = function (question, answer) {
    try {
      const sql = db.prepare(
        "INSERT INTO Questions (type, question, answer) VALUES ('degree', ?, ?)"
      );
      const trans = db.transaction((question, answer) => {
        sql.run(question, answer);
      });
      trans(question, answer);
      return true;
    } catch (err) {
      console.log("addDegree error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a degree function
   */
  this.deleteDegree = function (id) {
    try {
      if (this.getDegree(id) == undefined) {
        return undefined;
      }
      const sql = db.prepare("DELETE FROM Questions WHERE id = ?");
      const trans = db.transaction((id) => {
        sql.run(id);
      });
      trans(id);
      return true;
    } catch (err) {
      console.log("deleteDegree error: " + err);
    }
    return undefined;
  };

  // ---------------------------------------------------------------------------- //

  /*
   * Get all degree questions
   */
  this.getAllDegree = function () {
    try {
      const sql = db.prepare("SELECT * FROM Questions WHERE type = 'degree'");
      return sql.all();
    } catch (err) {
      console.log("getAllDegree error: " + err);
    }
    return undefined;
  };

  /*
   * Get a degree question
   */
  this.getDegree = function (id) {
    try {
      const sql = db.prepare("SELECT * FROM Questions WHERE id = ?");
      const q = sql.get(id);
      return q;
    } catch (err) {
      console.log("getDegree error: " + err);
    }
    return undefined;
  };

  /*
   * Update a degree question
   */
  this.updateDegree = function (id, question, answer) {
    try {
      if (this.getDegree(id) == undefined) {
        return undefined;
      }
      const sql = db.prepare(
        "UPDATE Questions SET question = @question, answer = @answer WHERE id = @id"
      );
      const trans = db.transaction((id, question, answer) => {
        sql.run({ question: question, answer: answer, id: id });
      });
      trans(question, answer, id);
      return true;
    } catch (err) {
      console.log("updateDegree error: " + err);
    }
    return undefined;
  };

  /*
   * Add a new degree question
   * No need to pass the id, the id in the database increase automatically
   */
  this.addDegree = function (question, answer) {
    try {
      const sql = db.prepare(
        "INSERT INTO Questions (type, question, answer) VALUES ('degree', ?, ?)"
      );
      const trans = db.transaction((question, answer) => {
        sql.run(question, answer);
      });
      trans(question, answer);
      return true;
    } catch (err) {
      console.log("addDegree error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a degree function
   */
  this.deleteDegree = function (id) {
    try {
      if (this.getDegree(id) == undefined) {
        return undefined;
      }
      const sql = db.prepare("DELETE FROM Questions WHERE id = ?");
      const trans = db.transaction((id) => {
        sql.run(id);
      });
      trans(id);
      return true;
    } catch (err) {
      console.log("deleteDegree error: " + err);
    }
    return undefined;
  };

  // ---------------------------------------------------------------------------- //

  /*
   * Get all honours questions
   */
  this.getAllHonour = function () {
    try {
      const sql = db.prepare("SELECT * FROM Questions WHERE type = 'honour'");
      return sql.all();
    } catch (err) {
      console.log("getAllHonour error: " + err);
    }
    return undefined;
  };

  /*
   * Get a honour question
   */
  this.getHonour = function (id) {
    try {
      const sql = db.prepare("SELECT * FROM Questions WHERE id = ?");
      const q = sql.get(id);
      return q;
    } catch (err) {
      console.log("getHonour error: " + err);
    }
    return undefined;
  };

  /*
   * Update a honour question
   */
  this.updateHonour = function (id, question, answer) {
    try {
      if (this.getHonour(id) == undefined) {
        return undefined;
      }
      const sql = db.prepare(
        "UPDATE Questions SET question = @question, answer = @answer WHERE id = @id"
      );
      const trans = db.transaction((id, question, answer) => {
        sql.run({ question: question, answer: answer, id: id });
      });
      trans(question, answer, id);
      return true;
    } catch (err) {
      console.log("updateHonour error: " + err);
    }
    return undefined;
  };

  /*
   * Add a new honour question
   * No need to pass the id, the id in the database increase automatically
   */
  this.addHonour = function (question, answer) {
    try {
      const sql = db.prepare(
        "INSERT INTO Questions (type, question, answer) VALUES ('honour', ?, ?)"
      );
      const trans = db.transaction((question, answer) => {
        sql.run(question, answer);
      });
      trans(question, answer);
      return true;
    } catch (err) {
      console.log("addHonour error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a honour function
   */
  this.deleteHonour = function (id) {
    try {
      if (this.getHonour(id) == undefined) {
        return undefined;
      }
      const sql = db.prepare("DELETE FROM Questions WHERE id = ?");
      const trans = db.transaction((id) => {
        sql.run(id);
      });
      trans(id);
      return true;
    } catch (err) {
      console.log("deleteHonour error: " + err);
    }
    return undefined;
  };

  // ---------------------------------------------------------------------------- //

  /*
   * Get all FAQ questions
   */
  this.getAllFAQ = function () {
    try {
      const sql = db.prepare("SELECT * FROM Questions WHERE type = 'faq'")
      return sql.all();
    } catch (err) {
      console.log("getAllFAQ error: " + err);
    }
    return undefined;
  };

  /*
   * Get a FAQ question
   */
  this.getFAQ = function (question) {
    try {
      const sql = db.prepare("SELECT * FROM Questions WHERE question = ?");
      const q = sql.get(question);
      return q;
    } catch (err) {
      console.log("getAUser error: " + err);
    }
    return undefined;
  };

  this.getFAQId = function (id) {
    try {
      const sql = db.prepare("SELECT * FROM Questions WHERE id = ?");
      const q = sql.get(id);
      return q;
    } catch (err) {
      console.log("getFAQId error: " + err);
    }
    return undefined;
  };

  /*
   * Update a FAQ question
   */
  this.updateFAQ = function (id, question, answer) {
    try {
      if (this.getFAQId(id) === undefined) {
        return undefined;
      }
      const sql = db.prepare(
        "UPDATE Questions SET question = @question, answer = @answer WHERE id = @id"
      );
      const trans = db.transaction((id, question, answer) => {
        sql.run({ question: question, answer: answer, id: id });
      });
      trans(question, answer, id);
      return true;
    } catch (err) {
      console.log("updateFAQ error: " + err);
    }
    return undefined;
  };

  /*
   * Add a new FAQ question
   * No need to pass the id, the id in the database increase automatically
   */
  this.addFAQ = function (question, answer) {
    try {
      const sql = db.prepare(
        "INSERT INTO Questions (type, question, answer) VALUES ('FAQ', ?, ?)"
      );
      const trans = db.transaction((question, answer) => {
        sql.run(question, answer);
      });
      trans(question, answer);
      return true;
    } catch (err) {
      console.log("addFAQ error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a FAQ function
   */
  this.deleteFAQ = function (id) {
    try {
      if (this.getFAQId(id) == undefined) {
        return undefined;
      }
      const sql = db.prepare("DELETE FROM Questions WHERE id = ?");
      const trans = db.transaction((id) => {
        sql.run(id);
      });
      trans(id);
      return true;
    } catch (err) {
      console.log("deleteFAQ error: " + err);
    }
    return undefined;
  };

  // ---------------------------------------------------------------------------- //

  /*
   * Get all Staff information
   *
   * The role is a string in the format 'role1,role2,role3'
   * Needed to be split to turn into an array
   */
  this.getAllStaff = function () {
    try {
      const sql = db.prepare(
        "SELECT * FROM Staffs NATURAL JOIN (SELECT name, group_concat(role) as role FROM StaffsRole GROUP BY name)"
      );
      return sql.all();
    } catch (err) {
      console.log("getAllStaff error: " + err);
    }
    return undefined;
  };

  /*
   * Get a specific staff information
   *
   * The role is a string in the format 'role1,role2,role3'
   * Needed to be split to turn into an array
   */
  this.getStaff = function (id) {
    try {
      const sql = db.prepare(
        "SELECT * FROM Staffs NATURAL JOIN (SELECT name, group_concat(role) as role FROM StaffsRole GROUP BY name) WHERE id = ?"
      );
      const staff = sql.get(id);
      return staff;
    } catch (err) {
      console.log("getStaff error: " + err);
    }
    return undefined;
  };

  /*
   * Update a staff information
   *
   * Parameter
   *  staffRole - an array containing roles
   */
  this.updateStaff = function (id, staffRole, name) {
    try {
      if (this.getStaff(id) === undefined) {
        return undefined;
      }

      // Get all roles for a staff
      let check_sql = db.prepare("SELECT role FROM StaffsRole WHERE id = ?");
      const roles = check_sql.all(id);

      // Delete roles
      for (let i = 0; i < roles.length; i++) {
        let index = staffRole.indexOf(roles[i].role);
        if (index !== -1) {
          delete staffRole[i];
        } else {
          this.deleteRole(id, roles[i].role);
        }
      }

      // Add new roles
      const sql_add = db.prepare(
        "INSERT INTO StaffsRole (id, name, role) VALUES (?, ?, ?)"
      );
      const trans_add = db.transaction((id, name, role) => {
        sql_add.run(id, name, role);
      });

      for (let i = 0; i < staffRole.length; i++) {
        if (staffRole[i] !== undefined && staffRole[i] !== "None") {
          trans_add(id, name, staffRole[i]);
        }
      }

      this.deleteRole(id, "None");

      return true;
    } catch (err) {
      console.log("update error: " + err);
    }
    return undefined;
  };

  /*
   * Get all Staff information
   */
  this.getAllRole = function () {
    try {
      const sql = db.prepare("SELECT * FROM StaffsRole");
      return sql.all();
    } catch (err) {
      console.log("getAllStaff error: " + err);
    }
    return undefined;
  };

  /*
   * Get a specific staff information
   */
  this.getRole = function (id) {
    try {
      const sql = db.prepare("SELECT * FROM StaffsRole WHERE id = ?");
      const staff = sql.get(id);
      return staff;
    } catch (err) {
      console.log("getStaff error: " + err);
    }
    return undefined;
  };

  /*
   * Add a new role to an existed staff
   */
  this.addRole = function (id, name, staffRole) {
    try {
      // If the staff is not exist -> return undefined
      const sql_check = db.prepare("SELECT * FROM Staffs WHERE id = ?");
      const staff = sql_check.get(id);

      if (staff === undefined) {
        return undefined;
      }

      const sql = db.prepare(
        "INSERT INTO StaffsRole (id, name, role) VALUES (?, ?, ?)"
      );
      const trans = db.transaction((id, name, role) => {
        sql.run(id, name, role);
      });

      let check_sql = db.prepare("SELECT role FROM StaffsRole WHERE id = ?");
      const roles = check_sql.all(id);

      // Get rid of the existed roles
      for (let i = 0; i < roles.length; i++) {
        let index = staffRole.indexOf(roles[i].role);
        if (index !== -1) {
          delete staffRole[i];
        }
      }

      // Add new roles
      for (let i = 0; i < staffRole.length; i++) {
        if (staffRole[i] !== undefined && staffRole[i] !== "None") {
          trans(id, name, staffRole[i]);
        }
      }

      // Get ride of None role;
      // Delete the None role
      // If "None" role not exists, it will raise an error. Don't worry about this
      this.deleteRole(id, "None");

      return true;
    } catch (err) {
      console.log("addRole error: " + err);
    }
    return undefined;
  };

  /*
   * Delete role from StaffRole table
   */
  this.deleteRole = function (id, role) {
    try {
      if (this.getStaff(id) === undefined) {
        return undefined;
      }

      // Delete Role
      const sql = db.prepare(
        "DELETE FROM StaffsRole WHERE id = ? AND role = ?"
      );
      const trans = db.transaction((id, role) => {
        sql.run(id, role);
      });
      trans(id, role);

      // If the staff has no role
      // Add "None" as default value
      if (this.getRole(id) == undefined) {
        const sql_none = db.prepare(
          "INSERT INTO StaffsRole (id, name, role) VALUES (?, ?, ?)"
        );
        const trans_none = db.transaction((id, name, role) => {
          sql_none.run(id, name, role);
        });
        trans_none(id, staff["name"], "None");
      }

      return true;
    } catch (err) {
      console.log("deleteRole error: " + err);
    }
    return undefined;
  };

  /*
   * Add a new Staff
   * No need to pass the id, the id in the database increase automatically
   */
  this.addStaff = function (name, role) {
    try {
      const staff_sql = db.prepare("INSERT INTO Staffs (name) VALUES (?)");
      const staff_trans = db.transaction((name) => {
        staff_sql.run(name);
      });
      staff_trans(name);

      // Get the staff id
      const id = db.prepare("SELECT id FROM Staffs WHERE name = ?").get(name);

      // Add new roles
      this.addRole(id.id, name, role);

      return true;
    } catch (err) {
      console.log("addStaff error: " + err);
    }
    return undefined;
  };

  /*
   * Delete a staff
   */
  this.deleteStaff = function (id) {
    try {
      if (this.getStaff(id) == undefined) {
        return undefined;
      }

      // Delete roles
      const role_sql = db.prepare("DELETE FROM StaffsRole WHERE id = ?");
      const role_trans = db.transaction((id) => {
        role_sql.run(id);
      });
      role_trans(id);

      // Delete staff
      const staff_sql = db.prepare("DELETE FROM Staffs WHERE id = ?");
      const staff_trans = db.transaction((id) => {
        staff_sql.run(id);
      });
      staff_trans(id);

      return true;
    } catch (err) {
      console.log("deleteStaff error: " + err);
    }
    return undefined;
  };

  // ---------------------------------------------------------------------------- //

  /*
   * Get all Unit Coordinator Information
   */
  this.getAllUnitsCoordinator = function () {
    try {
      const sql = db.prepare("SELECT * FROM UnitCoordinator");
      return sql.all();
    } catch (err) {
      console.log("getAllCoordinator error: " + err);
    }
    return undefined;
  };

  /*
   * Get all information of a staff who is coordinator (The coordinator is need to be assigned with a unit)
   */
  this.getStaffCoordinator = function () {
    try {
      const sql = db.prepare(
        "SELECT * FROM StaffsRole WHERE role == 'coordinator'"
      );
      return sql.all();
    } catch (err) {
      console.log("getStaffCoordinator error: " + err);
    }
    return undefined;
  };

  /*
   * Get the unit coordinator for a specific unit
   */
  this.getCoordinator = function () {
    try {
      const sql = db.prepare(
        "SELECT s.id as 'id', uc.staff as 'name' FROM UnitCoordinator uc INNER JOIN Staffs s ON (uc.staff = s.name)"
      );
      const staff = sql.get();
      return staff;
    } catch (err) {
      console.log("getCoordinator error: " + err);
    }
    return undefined;
  };

  /*
   * Get the unit coordinator by unit_id
   */
  this.getUnitCoordinator = function (unit_id) {
    try {
      const sql = db.prepare(
        "SELECT s.id as 'id', uc.staff as 'name' FROM UnitCoordinator uc INNER JOIN Staffs s ON (uc.staff = s.name) WHERE unit_id = ?"
      );
      const staff = sql.get(unit_id);
      return staff;
    } catch (err) {
      console.log("getStaff error: " + err);
    }
    return undefined;
  };

  // Add a coordinator for a unit
  this.addCoordinator = function (unit_id, name) {
    try {
      // Check staff exists or not

      // If the unit does not have a coordinator
      const sql_none = db.prepare(
        "INSERT INTO UnitCoordinator (unit_id, staff) VALUES (?, ?)"
      );
      const trans_none = db.transaction((id, name) => {
        sql_none.run(id, name);
      });

      trans_none(unit_id, name);

      return true;
    } catch (err) {
      console.log("addCoordinator error: " + err);
    }
    return undefined;
  };

  /*
   * Update the unit coordinator
   *
   * The staff already exists -> no need to add them
   */
  this.updateCoordinator = function (unit, staff) {
    try {
      if (this.getCoordinator(staff) == undefined) {
        return undefined;
      }
      // Get old coordinator
      const old_coordinator = this.getUnitCoordinator(unit).id;

      // Update coordinator
      const sql = db.prepare(
        "UPDATE UnitCoordinator SET staff = @staff WHERE unit_id = @unit"
      );
      const trans = db.transaction((unit_id, staff_id) => {
        sql.run({ staff: staff_id, unit: unit_id });
      });
      trans(unit, staff);

      // Check old coordinator -> Are their still a coordinator?
      const list = this.getCoordinator();
      let check = false;
      for (let i = 0; i < list.length; i++) {
        if (list[i].name === old_coordinator) {
          check = true;
        }
      }
      // If the staff is not a coordinator anymore, we should delete this line
      // If the staff is a coordinator, we do nothing
      // We need a default role
      if (!check) {
        if (this.deleteRole(old_coordinator, "coordinator") === undefined) {
          return undefined;
        }
      }

      return true;
    } catch (err) {
      console.log("updateCoordinator: " + err);
    }
    return undefined;
  };

  /*
   * Check Coordinator
   */
  this.checkCoordinator = function (staff) {
    try {
      const sql = db.prepare("SELECT * FROM UnitCoordinator WHERE staff = ?");
      if (sql.get(staff) !== undefined) {
        return true;
      }
    } catch (err) {
      console.log("checkCoordinator error: " + err);
    }
    return undefined;
  };

  // ---------------------------------------------------------------------------- //
}

module.exports = NLUDatabase;
