/*
 * This file mainly works with user_db
 */

function UserDatabase(filepath) {
  // Import Database
  const db = require("better-sqlite3")(filepath, {
    fileMustExist: true
  });

  this.createTable = function () {
    try {
      let sql = db.prepare(
        "create table User\n" +
          "(\n" +
          "\tuserID INTEGER default 0\n" +
          "\t\tconstraint User_pk\n" +
          "\t\t\tprimary key autoincrement,\n" +
          "\tusername VARCHAR(255) default '-' not null,\n" +
          "\tpassword VARCHAR(255) default '-' not null\n" +
          ");"
      );
      sql.run();
      return true;
    } catch (err) {
      console.log("getUsers error: " + err);
    }
    return undefined;
  };

  /*
   * Get all users in the list
   *
   * Return:
   *  [{username: "username", password: "password"}, {username: "u1", password: "pass"}]
   *  If error exists return undefined
   */
  this.getUsers = function () {
    try {
      const sql = db.prepare("SELECT * FROM User");
      return sql.all();
    } catch (err) {
      console.log("getUsers error: " + err);
    }
    return undefined;
  };

  /*
   * Add a new user
   *
   * Parameter:
   *  username - user's name
   *  password - user's password
   * Return:
   *  true - if successes
   *  undefined - fails
   */
  this.addUser = function (username, password) {
    try {
      const sql = db.prepare(
        "INSERT INTO User (username, password) VALUES (?, ?)"
      );
      const trans = db.transaction((name, pass) => {
        sql.run(username, password);
      });
      trans(username, password);
      return true;
    } catch (err) {
      console.log("addUser error: " + err);
    }
    return undefined;
  };

  /*
   * Get a specific user
   *
   * Parameter:
   *  username - user's name
   * Return:
   *  {username: "username", password: "password"} - success
   *  undefined - fail
   */
  this.getUser = function (username) {
    try {
      const sql = db.prepare("SELECT * FROM User WHERE username = ?");
      const user = sql.get(username);
      return user;
    } catch (err) {
      console.log("getAUser error: " + err);
    }
    return undefined;
  };

  /*
   * Update a user's password
   *
   * Parameter:
   *  username - user's name
   *  password - user's password
   *
   * Return:
   *  true - success
   *  undefined - fail
   */
  this.updateUserPass = function (username, password) {
    try {
      if (this.getUser(username) == undefined) {
        return false;
      }
      const sql = db.prepare(
        "UPDATE User SET password = @password WHERE username = @username"
      );
      const trans = db.transaction((name, pass) => {
        sql.run({ username: username, password: password });
      });
      trans(username, password);
      return true;
    } catch (err) {
      console.log("updateUserPass error: " + err);
    }
    return undefined;
  };
}

// Allow UserDatabase to be called in another file.
module.exports = UserDatabase;
