/*
 * This file mainly works with user_db
 */


function MeetingDatabase(filepath) {
    // Import Database
    const db = require('better-sqlite3')(filepath, {fileMustExist: true});

// ----------------------------------------------- Meeting part ----------------------------------------------- //

    /*
     * Get all the meeting information
     */
    this.getAllMeeting = function() {
        try {
            const sql = db.prepare("SELECT * FROM (Meetings M INNER JOIN Staffs S ON (M.staff_id = S.id))");
            return sql.all();
        } catch (err) {
            console.log("getAllMeeting: " + err);
        }
        return undefined;
    }


    /*
     * Get all future meetings
     */
    this.getAllFutureMeeting = function() {
        try {
            const sql = db.prepare("SELECT * FROM (Meetings M INNER JOIN Staffs S ON (M.staff_id = S.id)) WHERE status = 'future'");
            return sql.all();
        } catch (err) {
            console.log("getAllFutureMeeting: " + err);
        }
        return undefined;
    }

    /*
     * Get all past meetings
     */
    this.getAllPastMeeting = function() {
        try {
            const sql = db.prepare("SELECT * FROM (Meetings M INNER JOIN Staffs S ON (M.staff_id = S.id)) WHERE status = 'past'");
            return sql.all();
        } catch (err) {
            console.log("getAllPastMeeting: " + err);
        }
        return undefined;
    }


    /*
     * Add a new meetings
     *
     * Parameter:
     *  date - YYYY-MM-DD HH:MM:SS
     *  student - string
     *  staff - string
     *  status - "in meeting", "available", "unavailable", "past", "future"
     *  email - string
     *  degree - "CS", "CDS", "IS", "SE"
     *  year - int
     *  comment - string
     *  method - 'online', 'local'
     *  place - string (link / office)
     */
    this.addMeeting = function(date, student, staff, status, email, degree, year, comment, method, place) {
        /*
         * The function assumes that the staff exist in the db
         */
        try {
            // Check the time exists in the timetable and it is available
            const sql_check = db.prepare("SELECT id FROM TimeTable WHERE staff_id = ? AND time = ? AND status != 'selected'");
            const timetable_id = sql_check.get(staff, date).id;

            if (timetable_id === undefined) {
                return undefined;
            } else {
                // Change the selected timetable period to selected
                this.updateTimeStatus(timetable_id, date);
            }

            const sql = db.prepare("INSERT INTO Meetings (date, student, staff_id, status, email, degree, year, comment, method, place, timetable_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            const trans = db.transaction((date, student, staff, status, email, degree, year, comment, method, place, timetable_id) => {
                sql.run(date, student, staff, status, email, degree, year, comment, method, place, timetable_id);
            })
            trans(date, student, staff, status, email, degree, year, comment, method, place, timetable_id);

            return true;
        } catch (err) {
            console.log("addMeeting error: " + err);
        }
        return undefined;
    }


    /*
     * Update all meeting's status based on the given time
     * Before the time - 'passed'
     * After the time - 'future'
    */
    this.updateMeetingStatus = function(id, current) {
        try {
            const sql_check = db.prepare("SELECT (DATE(date) > Date(?)) AS 'result' FROM Meetings WHERE id = ?");
            let check = sql_check.get(current, id);
            if (check === undefined) {
                return undefined;
            }

            let status;
            if (check.result === 0) {
                status = 'past';
            } else {
                status = 'future';
            }

            const sql = db.prepare("UPDATE Meetings SET status = @status WHERE id = @id");
            const trans = db.transaction((id, status) => {
                sql.run({status: status, id: id});
            })
            trans(id, status);

            return true;
        } catch (err) {
            console.log("updateMeetingStatus: " + err);
        }
        return undefined;
    }


    /*
     * meeting table
     *  updates status (passed / future) - given current time -> updates status
     *  delete function?
     *  present history?
     *  present a person history?
     */


// ----------------------------------------------- Staff part ----------------------------------------------- //


    /*
     * Get all Staff information
     *
     * Needed to be split to turn into an array
     */
    this.getAllStaff = function() {
        try {
            const sql = db.prepare("SELECT * FROM Staffs");
            // console.log(sql.all());
            return sql.all();
        } catch (err) {
            console.log("getAllStaff error: " + err);
        }
        return undefined;
    }

    /*
     * Get a specific staff information
     *
     * Needed to be split to turn into an array
     */
    this.getStaff = function(id) {
        try {
            const sql = db.prepare("SELECT * FROM Staffs WHERE id = ?");
            const staff = sql.get(id);
            return staff;
        } catch (err) {
            console.log("getStaff error: " + err);
        }
        return undefined;
    }

    /*
     * Get a specific staff information
     *
     * Needed to be split to turn into an array
     */
    this.getStaffList = function() {
        try {
            const sql = db.prepare("SELECT name, id FROM Staffs");
            const staff = sql.all();
            return staff;
        } catch (err) {
            console.log("getStaff error: " + err);
        }
        return undefined;
    }

    /*
     * Add a staff
     */
    this.addStaff = function(name, email, available, office) {
        try {
            // Insert information
            const staff_sql = db.prepare("INSERT INTO Staffs (name, email, available, office) VALUES (?, ?, ?, ?)");
            const staff_trans = db.transaction((name, email, available, office) => {
                staff_sql.run(name, email, available, office);
            })
            staff_trans(name, email, available, office);

            return true;
        } catch (err) {
            console.log("addStaff error: " + err);
        }
        return undefined;
    }

    /*
     * Delete a staff
     */
    this.deleteStaff = function(id) {
        try {
            if (this.getStaff(id) == undefined) {
                return undefined;
            }

            // Delete staff
            const staff_sql = db.prepare("DELETE FROM Staffs WHERE id = ?");
            const staff_trans = db.transaction((id) => {
                staff_sql.run(id);
            })
            staff_trans(id);

            return true;
        } catch (err) {
            console.log("deleteStaff error: " + err);
        }
        return undefined;
    }

    /*
     * Update a staff information
     *
     * Parameter
     */
    this.updateStaff = function (id, name, email, available, office) {
        try {
            if (this.getStaff(id) === undefined) {
                return undefined;
            }

            // Update Staff's info
            const sql = db.prepare("UPDATE Staffs SET name = @name, email = @email, available = @available, office = @office WHERE id = @id");
            const trans = db.transaction((id, name, email, available, office) => {
                sql.run({name: name, email: email, available: available, office: office, id: id});
            })
            trans(id, name, email, available, office);

            return true;
        } catch (err) {
            console.log("updateStaff error: " + err);
        }
        return undefined;
    }


// ----------------------------------------------- Timetable part ----------------------------------------------- //

    /*
     * Get unselected time period with a given date
     * The timetable before the date will not be shown
     * Parameter
     *  - current: the current local time
     *              Must follow the format: 'yyyy-mm-dd hh:mm:ss'
     */
    this.getALLUnselectedTime = function(current) {
        try {
            const sql = db.prepare("SELECT * FROM TimeTable WHERE status != 'selected' AND DATE(time) > ?");
            return sql.all(current);
        } catch (err) {
            console.log("getALLUnselectedTime error: " + err);
        }
        return undefined;
    }

    /*
     * Get one staff's unselected time with a given date
     * The timetable before the date will not be shown
     * Parameter
     *  - staff: staff id
     *  - current: the current local time
     *              Must follow the format: 'yyyy-mm-dd hh:mm:ss'
     */
    this.getAStaffUnselectedTime = function(staff, current) {
        try {
            const sql = db.prepare("SELECT * FROM TimeTable WHERE staff_id = ? AND status != 'selected' AND DATE(time) > ?");
            return sql.all(staff, current);
        } catch (err) {
            console.log("getAStaffUnselectedTime error: " + err);
        }
        return undefined;
    }


    /*
     * Get all time periods with a given date
     * The timetable before the date will not be shown
     * - current: the current local time
     *              Must follow the format: 'yyyy-mm-dd hh:mm:ss'
     */
    this.getALLTime = function(current) {
        try {
            const sql = db.prepare("SELECT * FROM TimeTable WHERE DATE(time) > ?");
            return sql.all(current);
        } catch (err) {
            console.log("getAllTime error: " + err);
        }
        return undefined;
    }

    /*
     * Add a timetable with a given time - status - None
     * Parameter:
     *  staff - staff id
     *  time - an array of date in the format: yyyy-mm-dd hh:mm:ss
     *
     * All the status of the time period will be set to 'available'
     */
    this.addTime = function(staff, time) {
        try {
            // Check whether the staff existed in the Staff table
            if (this.getStaff(staff) === undefined) {
                return undefined;
            }
            // Check whether the time already existed, if existed we do nothing
            const check_sql = db.prepare("SELECT * FROM TimeTable WHERE staff_id = ? AND time = ?");
            if (check_sql.get(staff, time) !== undefined) {
                return undefined;
            }
            // Insert timetable
            const sql = db.prepare("INSERT INTO TimeTable (staff_id, time, status) VALUES (?, ?, 'available')");
            const trans = db.transaction((staff, time) => {
                sql.run(staff, time);
            })
            trans(staff, time);
            return true;
        } catch (err) {
            console.log("addTime error: " + err);
        }
        return undefined;
    }

    /*
     * Delete a staff's unselected time table
     */
    this.deleteTime = function(staff) {
        try {
            const sql = db.prepare("DELETE FROM TimeTable WHERE staff_id = ? and status != 'selected'");
            const trans = db.transaction((staff) => {
                sql.run(staff);
            })
            trans(staff);
            return true;
        } catch (err) {
            console.log("deleteTime error: " + err);
        }
        return undefined;
    }


    /*
     * Select a time period by changing the status to 'selected'
     */
    this.updateTimeStatus = function(id, time) {
        try {
            const sql_check = db.prepare("SELECT * FROM TimeTable WHERE id = ? and time = ?");
            if (sql_check.get(id, time) === undefined) {
                return undefined;
            }

            // Update timetable status
            const sql = db.prepare("UPDATE Timetable SET status = @status WHERE id = @id AND time = @time");
            const trans = db.transaction((status, id, time) => {
                sql.run({status: status, id: id, time: time});
            })
            trans('selected', id, time);
            return true;
        } catch (err) {
            console.log("updateTimeStatus error: " + err);
        }
        return undefined;
    }


}

// Allow UserDatabase to be called in another file.
module.exports = MeetingDatabase;