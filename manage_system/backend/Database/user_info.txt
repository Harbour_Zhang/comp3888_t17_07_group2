create table User
(
	userID INTEGER default 0
		constraint User_pk
			primary key autoincrement,
	username VARCHAR(255) default '-' not null,
	password VARCHAR(255) default '-' not null
);