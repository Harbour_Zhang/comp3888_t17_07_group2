import { Typography, CardActions, CardContent, Button, Card, Grid } from '@mui/material';
import { NavLink as RouterLink } from 'react-router-dom';

const TopicBox = ({id, name, description, path}) => {
    console.log(name)
    return(
      <Grid item key={id} xs={12} sm={6} md={4}>
          <Card
          sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
          >
              <CardContent sx={{ flexGrow: 1 }}>
              <Typography gutterBottom variant="h6" style={{ fontWeight: 600 }}>
              {name}
              </Typography>
              <Typography >
              {description}
              </Typography>
              </CardContent>
              <CardActions>
                  <Button size="small" component={RouterLink} to={path}>Edit</Button>
              </CardActions>
          </Card> 
      </Grid>
    )
  }

export default TopicBox