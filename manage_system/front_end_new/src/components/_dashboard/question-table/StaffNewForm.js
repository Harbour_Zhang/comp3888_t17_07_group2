import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { Form, FormikProvider, useFormik } from 'formik';
// material
import { styled } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';
import {
  Card,
  Grid,
  Stack,
  Radio,
  RadioGroup,
  TextField,
  Typography,
  FormControlLabel
} from '@mui/material';
import Axios from 'axios';
// utils
import fakeRequest from '../../../utils/fakeRequest';

const LabelStyle = styled(Typography)(({ theme }) => ({
  ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
}));

const ANSWER_OPTION = ['Yes', 'No'];
// ----------------------------------------------------------------------

StaffNewForm.propTypes = {
  isEdit: PropTypes.bool,
};

export default function StaffNewForm({ isEdit, currentPath, list }) {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const NewProductSchema = Yup.object().shape({
    name: Yup.string().required('Name is required'),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: list[0]?.name || '',
      role1: list[0]?.role.includes('coordinator') ? 'Yes': 'No',
      role2: list[0]?.role.includes('director') ? 'Yes': 'No',
      role3: list[0]?.role.includes('undergraduate administrative officer') ? 'Yes': 'No',
      role4: list[0]?.role.includes('year advisor') ? 'Yes': 'No',
      role5: list[0]?.role.includes('honours coordinator') ? 'Yes': 'No',
    },
    validationSchema: NewProductSchema,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      try {
        await fakeRequest(500);
        resetForm();
        setSubmitting(false);
        updateStaff()
      } catch (error) {
        console.error(error);
        setSubmitting(false);
        setErrors(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;
  
  const updateStaff = () => {
      const roleList = [];
      if(formik.values.role1 === 'Yes'){
        roleList.push('coordinator');
      }
      if(formik.values.role2 === 'Yes'){
        roleList.push('director');
      }
      if(formik.values.role3 === 'Yes'){
        roleList.push('undergraduate administrative officer');
      }
      if(formik.values.role4 === 'Yes'){
        roleList.push('year advisor');
      }
      if(formik.values.role5 === 'Yes'){
        roleList.push('honours coordinator');
      }
      if(roleList.length === 0){
        roleList.push('None');
      }

      Axios.get('http://localhost:3002/dashboard/staff_table/get_staff_table', {
          user_id:1,
          dtype: 'json',
      }).then((response) => {
          if(response.data.status === 200){
            if(isEdit){
              Axios.post('http://localhost:3002/dashboard/staff_table/update_staff', {
                user_id:1,
                dtype: 'json',
                staffID: list[0].id.toString(),
                staffRole: roleList,
                name: formik.values.name,
              }
            ).then(() => {
              navigate('/dashboard/academic_table');
            }).catch((error) => {
              console.log(error);
            })
          }
          else{
            let isExisted = false;
            for (let i = 0; i < response.data.staffList.length; i++) {
              if (response.data.staffList[i].name === formik.values.name) {
                isExisted = true;
                break;
              }
            }
            if(isExisted){
              enqueueSnackbar('Name Existed', { variant: 'error' });
            }
            else{
                Axios.post('http://localhost:3002/dashboard/staff_table/add_question', {
                    user_id:1,
                    dtype: 'json',
                    staffRole: roleList,
                    name: formik.values.name,
                  }
                ).then((response) => {
                  navigate('/dashboard/academic_table');
                }).catch((error) => {
                  console.log(error);
                })
              
              enqueueSnackbar(!isEdit ? 'Create success' : 'Update success', { variant: 'success' });
            }    
          }
        }
      }).catch((error) => {
        console.log(error);
      })
  }


  return (
    <FormikProvider value={formik}>
      <Form noValidate autoComplete="off" onSubmit={handleSubmit}>

        <Grid container spacing={3}>
          <Grid item xs={12} md={8}>
            <Card sx={{ p: 3 }}>
              <Stack spacing={3}>
                <div>
                <LabelStyle>Staff Name</LabelStyle>
                <TextField
                  fullWidth
                  rows={2}
                  {...getFieldProps('name')}
                  focused
                  error={Boolean(touched.name && errors.name)}
                  helperText={'name' && errors.name}
                />
                </div>

                <div>
                <LabelStyle>Coordinator</LabelStyle>
                <RadioGroup {...getFieldProps('role1')} row>
                  <Stack spacing={1} direction="row">
                    {ANSWER_OPTION.map((answer) => (
                      <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                    ))}
                  </Stack>
                </RadioGroup>
                </div>

                <div>
                <LabelStyle>Directory</LabelStyle>
                <RadioGroup {...getFieldProps('role2')} row>
                  <Stack spacing={1} direction="row">
                    {ANSWER_OPTION.map((answer) => (
                      <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                    ))}
                  </Stack>
                </RadioGroup>
                </div>

                <div>
                <LabelStyle>Undergraduate Administrative Officer</LabelStyle>
                <RadioGroup {...getFieldProps('role3')} row>
                  <Stack spacing={1} direction="row">
                    {ANSWER_OPTION.map((answer) => (
                      <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                    ))}
                  </Stack>
                </RadioGroup>
                </div>

                <div>
                <LabelStyle>Year Advisor</LabelStyle>
                <RadioGroup {...getFieldProps('role4')} row>
                  <Stack spacing={1} direction="row">
                    {ANSWER_OPTION.map((answer) => (
                      <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                    ))}
                  </Stack>
                </RadioGroup>
                </div>

                <div>
                <LabelStyle>Honours Coordinator</LabelStyle>
                <RadioGroup {...getFieldProps('role5')} row>
                  <Stack spacing={1} direction="row">
                    {ANSWER_OPTION.map((answer) => (
                      <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                    ))}
                  </Stack>
                </RadioGroup>
                </div>

          <Grid item xs={12} md={4}>
          <Card sx={{ p: 2 }} />

            
              <LoadingButton type="submit" variant="contained" size="large" loading={isSubmitting}>
                {!isEdit ? 'Create Staff Member' : 'Save Changes'}
              </LoadingButton>
              
          </Grid>

            </Stack>

            </Card>
          </Grid>

          
          
        </Grid>
      </Form>
    </FormikProvider>
  );
}
