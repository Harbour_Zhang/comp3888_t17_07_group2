import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { Form, FormikProvider, useFormik } from 'formik';
import Axios from 'axios';
// material
import { styled } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';
import {
  Card,
  Grid,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
// utils
import fakeRequest from '../../../utils/fakeRequest';

const LabelStyle = styled(Typography)(({ theme }) => ({
  ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
}));

UnitNewForm.propTypes = {
  isEdit: PropTypes.bool,
};

export default function UnitNewForm({ isEdit, unit, coordinator, staffList}) {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const NewProductSchema = Yup.object().shape({
    name: Yup.string().required('Coordinator name is required')
  });


  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      unit : unit || '',
      name: coordinator ||''
    },
    validationSchema: NewProductSchema,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      try {
        await fakeRequest(500);
        resetForm();
        setSubmitting(false);
        enqueueSnackbar('Update success', { variant: 'success' });
        updateUnit();
      } catch (error) {
        console.error(error);
        setSubmitting(false);
        setErrors(error);
      }
    }
  });

  const { handleSubmit, isSubmitting, getFieldProps } = formik;

  const updateUnit = () => {
    const url = "http://localhost:3002/dashboard/unitCoordinator_table/update_unitCoordinator"

    Axios.post( url, {
          user_id:1,
          dtype: 'json',
          unitID: formik.values.unit,
          name: formik.values.name
        }
      ).then(() => {
        navigate('/dashboard/unit_coordinator_table');
      }).catch((error) => {
        console.log(error);
      })
  }

  return (
    <FormikProvider value={formik}>
      <Form noValidate autoComplete="off" onSubmit={handleSubmit}>

        <Grid container spacing={3}>
          <Grid item xs={12} md={8}>
            <Card sx={{ p: 3 }}>
              <Stack spacing={3}>
                <div>
                <LabelStyle>Unit</LabelStyle>
                <TextField
                  fullWidth
                  disabled
                  rows={2}
                  {...getFieldProps('unit')}
                  focused
                />
                </div>

                <div>
                  <LabelStyle>Coordinator name</LabelStyle>
                  <TextField
                    select
                    fullWidth
                    {...getFieldProps('name')}
                    SelectProps={{ native: true }}
                  >
                    <option value="" />
                    {staffList.map((option) => (
                      <option key={option.id} value={option.name}>
                        {option.name}
                      </option>
                    ))}
                  </TextField>
                </div>

                

                <Grid item xs={12} md={4}>
          <Card sx={{ p: 2 }} />

            
              <LoadingButton type="submit" variant="contained" size="large" loading={isSubmitting}>
                {'Save Changes'}
              </LoadingButton>
              
          </Grid>

            </Stack>

            </Card>
          </Grid>

          
          
        </Grid>
      </Form>
    </FormikProvider>
  );
}
