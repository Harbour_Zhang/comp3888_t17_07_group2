import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { Form, FormikProvider, useFormik } from 'formik';
// material
import { styled } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';
import {
  Card,
  Grid,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import Axios from 'axios';
// utils
import fakeRequest from '../../../utils/fakeRequest';
// routes


// ----------------------------------------------------------------------

const LabelStyle = styled(Typography)(({ theme }) => ({
  ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
}));

// ----------------------------------------------------------------------

QuestionNewForm.propTypes = {
  isEdit: PropTypes.bool,
};

export default function QuestionNewForm({ isEdit, currentPath, list }) {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const NewProductSchema = Yup.object().shape({
    question: Yup.string().required('Question is required'),
    answer: Yup.string().required('Answer is required'),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      question: list[0]?.question || '',
      answer: list[0]?.answer || ''
    },
    validationSchema: NewProductSchema,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      try {
        await fakeRequest(500);
        resetForm();
        setSubmitting(false);
        if(currentPath === "/dashboard/academic_table"){
          updateStaff();
        }
        else if(currentPath === '/dashboard/degree_table'){
          updateDegreeQuestion();
        }
        else if(currentPath === '/dashboard/hornous_table'){
          updateHonourQuestion();
        }
        else if(currentPath === '/dashboard/faq_table'){
          updateFaqQuestion();
        }
        
        enqueueSnackbar(!isEdit ? 'Create success' : 'Update success', { variant: 'success' });
      } catch (error) {
        console.error(error);
        setSubmitting(false);
        setErrors(error);
      }
    }
  });

  const { errors, touched, handleSubmit, isSubmitting,getFieldProps } = formik;

  const updateStaff = () => {
    const roleList = formik.values.answer.split(",");
    if(isEdit){
        Axios.post('http://localhost:3002/dashboard/staff_table/update_staff', {
          user_id:1,
          dtype: 'json',
          staffID: list[0].id.toString(),
          staffRole: roleList,
          name: formik.values.question,
        }
      ).then(() => {
        navigate('/dashboard/academic_table');
      }).catch((error) => {
        console.log(error);
      })
    }
    else{
      Axios.post('http://localhost:3002/dashboard/staff_table/add_question', {
          user_id:1,
          dtype: 'json',
          staffRole: roleList,
          name: formik.values.question,
        }
      ).then(() => {
        navigate('/dashboard/academic_table');
      }).catch((error) => {
        console.log(error);
      })
    }
  }

  const updateDegreeQuestion = () => {
    if(isEdit){
        Axios.post('http://localhost:3002/dashboard/degree_table/update_question', {
          user_id:1,
          dtype: 'json',
          questionID: list[0].id.toString(),
          question: formik.values.question,
          answer: formik.values.answer,
        }
      ).then(() => {
        navigate('/dashboard/degree_table');
      }).catch((error) => {
        console.log(error);
      })
    }
    else{
      Axios.post('http://localhost:3002/dashboard/degree_table/add_question', {
          user_id:1,
          dtype: 'json',
          question: formik.values.question,
          answer: formik.values.answer,
        }
      ).then(() => {
        navigate('/dashboard/degree_table');
      }).catch((error) => {
        console.log(error);
      })
    }
  }

  const updateHonourQuestion = () => {
    if(isEdit){
      Axios.post('http://localhost:3002/dashboard/hornous_table/update_question', {
        user_id:1,
        dtype: 'json',
        questionID: list[0].id.toString(),
        question: formik.values.question,
        answer: formik.values.answer,
      }
    ).then(() => {
      navigate('/dashboard/hornous_table');
    }).catch((error) => {
      console.log(error);
    })
    }
    else{
      Axios.post('http://localhost:3002/dashboard/hornous_table/add_question', {
          user_id:1,
          dtype: 'json',
          name: formik.values.question,
          question: formik.values.question,
          answer: formik.values.answer,
        }
      ).then(() => {
        navigate('/dashboard/hornous_table');
      }).catch((error) => {
        console.log(error);
      })
    }
  }

  const updateFaqQuestion = () => {
    if(isEdit){
      Axios.post('http://localhost:3002/dashboard/faq_table/update_question', {
        user_id:1,
        dtype: 'json',
        questionID: list[0].id.toString(),
        question: formik.values.question,
        answer: formik.values.answer,
      }
    ).then(() => {
      navigate('/dashboard/faq_table');
    }).catch((error) => {
      console.log(error);
    })
    }
    else{
      Axios.post('http://localhost:3002/dashboard/faq_table/add_question', {
          user_id:1,
          dtype: 'json',
          name: formik.values.question,
          questionID: '0',
          question: formik.values.question,
          answer: formik.values.answer,
        }
      ).then(() => {
        navigate('/dashboard/faq_table');
      }).catch((error) => {
        console.log(error);
      })
    }
  }

  return (
    <FormikProvider value={formik}>
      <Form noValidate autoComplete="off" onSubmit={handleSubmit}>

        <Grid container spacing={3}>
          <Grid item xs={12} md={8}>
            <Card sx={{ p: 3 }}>
              <Stack spacing={3}>
                <div>
                <LabelStyle>Question</LabelStyle>
                <TextField
                  fullWidth
                  multiline
                  rows={3}
                  {...getFieldProps('question')}
                  focused
                  error={Boolean(touched.question && errors.question)}
                  helperText={'q' && errors.question}
                />
                </div>

                <div>
                  <LabelStyle>Answer</LabelStyle>
                  <TextField
                    id="prohibition"
                    multiline
                    rows={5}
                    fullWidth
                    {...getFieldProps('answer')}
                    focused
                    error={Boolean(touched.answer && errors.answer)}
                  helperText={'a' && errors.answer}
                  />
                </div>

                <Grid item xs={12} md={4}>
          <Card sx={{ p: 2 }} />

            
              <LoadingButton type="submit" variant="contained" size="large" loading={isSubmitting}>
                {!isEdit ? 'Create Product' : 'Save Changes'}
              </LoadingButton>
              
          </Grid>

            </Stack>

            </Card>
          </Grid>

          
          
        </Grid>
      </Form>
    </FormikProvider>
  );
}
