import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { Form, FormikProvider, useFormik } from 'formik';
import Axios from 'axios';
// material
import { styled } from '@mui/material/styles';
import { LoadingButton } from '@mui/lab';
import {
  Card,
  Grid,
  Stack,
  Radio,
  Select,
  TextField,
  InputLabel,
  Typography,
  RadioGroup,
  FormControl,
  FormControlLabel
} from '@mui/material';
// utils
import fakeRequest from '../../../utils/fakeRequest';

const ANSWER_OPTION = ['Yes', 'No'];
const MAJOR_OPTION = ['Computer Science', 'Information System', 'Software Engineering', 'Computational Data Science']

const LabelStyle = styled(Typography)(({ theme }) => ({
  ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
}));

// ----------------------------------------------------------------------

ProductNewForm.propTypes = {
  isEdit: PropTypes.bool,
};

export default function ProductNewForm({ isEdit, unitInfo}) {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const NewProductSchema = Yup.object().shape({
    unitID: Yup.string().required('unit ID is required'),
    year: Yup.number().required('Year is required'),
    credit:  Yup.number().required('Credit is required'),
    prohibition: Yup.string().required('prohibition detail is required'),
    prerequisites: Yup.string().required('prerequisite detail is required'),
    link: Yup.mixed().required('unit link is required'),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      unitID: unitInfo[0]?.unit_id || '',
      year: unitInfo[0]?.year || 0,
      credit:  unitInfo[0]?.credit || '',
      core: Boolean(unitInfo[0]?.core === 1) ? 'Yes': 'No',
      prohibition: unitInfo[0]?.prohibition || '',
      prerequisites: unitInfo[0]?.prerequisites || '',
      link: unitInfo[0]?.link || '',
      major: MAJOR_OPTION[0]
    },
    validationSchema: NewProductSchema,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      try {
        await fakeRequest(500);
        resetForm();
        setSubmitting(false);
        enqueueSnackbar(!isEdit ? 'Create success' : 'Update success', { variant: 'success' });
        updateUnit();
      } catch (error) {
        console.error(error);
        setSubmitting(false);
        setErrors(error);
      }
    }
  });
  const { errors, values, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  const updateUnit = () => {
    if(isEdit){
        Axios.post('http://localhost:3002/dashboard/unit_table/update_unit', {
          user_id:1,
          dtype: 'json',
          unitID: formik.values.unitID,
          credits: formik.values.credit.toString(),
          major: 'Computer Science',
          core: formik.values.core ? '1': '0',
          elective:'0',
          link: formik.values.link,
          year: formik.values.year.toString(),
          prohibition: formik.values.prohibition,
          prerequisites: formik.values.prerequisites,
          coordinator: '1',
        }
      ).then((response) => {
        navigate('/dashboard/unit_table');
      }).catch((error) => {
        console.log(error);
      })
    }
    else{
      Axios.post('http://localhost:3002/dashboard/unit_table/add_unit', {
          user_id:1,
          dtype: 'json',
          unitID: formik.values.unitID,
          credits: formik.values.credit.toString(),
          major: 'Computer Science',
          core: formik.values.core ? '1': '0',
          elective:'0',
          link: formik.values.link,
          year: formik.values.year.toString(),
          prohibition: formik.values.prohibition,
          prerequisites: formik.values.prerequisites,
          coordinator: '1',
        }
      ).then((response) => {
        navigate('/dashboard/unit_table');
      }).catch((error) => {
        console.log(error);
      })
    }
    
  }

  return (
    <FormikProvider value={formik}>
      <Form noValidate autoComplete="off" onSubmit={handleSubmit}>

        <Grid container spacing={3}>
          <Grid item xs={12} md={8}>
            <Card sx={{ p: 3 }}>
              <Stack spacing={3}>
                <LabelStyle>Unit Code</LabelStyle>
                <TextField
                  fullWidth
                  {...getFieldProps('unitID')}
                  error={Boolean(touched.unitID && errors.unitID)}
                  helperText={'id' && errors.unitID}
                />

                <div>
                  <LabelStyle>Unit Prohibition</LabelStyle>
                  <TextField
                    id="prohibition"
                    multiline
                    rows={4}
                    fullWidth
                    defaultValue=""
                    {...getFieldProps('prohibition')}
                    error={Boolean(touched.prohibition && errors.prohibition)}
                    helperText={'pb' && errors.prohibition}
                  />
                </div>

                <div>
                  <LabelStyle>Unit Prerequests</LabelStyle>
                  <TextField
                    id="prerequests"
                    multiline
                    rows={4}
                    fullWidth
                    defaultValue=""
                    {...getFieldProps('prerequisites')}
                    error={Boolean(touched.prerequisites && errors.prerequisites)}
                    helperText={'pr' && errors.prerequisites}
                  />      
                </div>

                <LabelStyle>Unit Link</LabelStyle>
                <TextField
                  fullWidth
                  {...getFieldProps('link')}
                  error={Boolean(touched.link && errors.link)}
                  helperText={'pr' && errors.link}
                />
              </Stack>
            </Card>
          </Grid>

          <Grid item xs={12} md={4}>
            <Stack spacing={3}>
              <Card sx={{ p: 3 }}>

                <Stack spacing={3}>
                  <TextField 
                    fullWidth 
                    label="Unit Credit" 
                    {...getFieldProps('credit')} 
                    error={Boolean(touched.credit && errors.credit)}
                    helperText={'pr' && errors.credit}
                  />
                  <TextField 
                    fullWidth 
                    label="Year" 
                    {...getFieldProps('year')} 
                    error={Boolean(touched.year && errors.year)}
                    helperText={'pr' && errors.year}
                  />

                  <div>
                    <LabelStyle>Core Unit</LabelStyle>
                    <RadioGroup {...getFieldProps('core')} row>
                      <Stack spacing={1} direction="row">
                        {ANSWER_OPTION.map((answer) => (
                          <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                        ))}
                      </Stack>
                    </RadioGroup>
                  </div>

                  
                  <FormControl fullWidth>
                    <InputLabel>Major</InputLabel>
                    <Select label="major" native {...getFieldProps('major')} value={values.major}>
                      {MAJOR_OPTION.map((major) => (
                            <option key={major} value={major}>
                              {major}
                            </option>
                          ))}
                    </Select>
                  </FormControl>
                  
                </Stack>
              </Card>

              <LoadingButton type="submit" fullWidth variant="contained" size="large" loading={isSubmitting}>
                {!isEdit ? 'Create Unit' : 'Save Changes'}
              </LoadingButton>
            </Stack>
          </Grid>
        </Grid>
      </Form>
    </FormikProvider>
  );
}
