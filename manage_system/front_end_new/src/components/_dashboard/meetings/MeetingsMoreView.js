import { useState } from 'react';
import { useTheme } from '@mui/material/styles';

import Label from '../../Label';
import MeetingsDetailsTable from './MeetingsDetailsTable';

// ----------------------------------------------------------------------

export default function MeetingsMoreView({ details, editModalType }) {
  const theme = useTheme();

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const DetailsTable = (details) => {
    let table = null;
    table = (
      <MeetingsDetailsTable
        handleClose={handleClose}
        open={open}
        details={details}
      />
    );
    return table;
  };

  return (
    <>
      <Label
        variant={theme.palette.mode === 'filled'}
        color='success'
        onClick={() => handleOpen()}
      >
        More
      </Label>

      {DetailsTable(details)}

    </>
  );
}
