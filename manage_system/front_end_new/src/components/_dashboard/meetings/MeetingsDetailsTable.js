import {
  Box,
  Modal,
  Button,
  Typography,
  TextField,
} from '@mui/material';

export default function MeetingsDetailsTable({
  open,
  handleClose,
  details
}) {

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    height: '60%',

    bgcolor: '#ffff',
    border: 'none',
    boxShadow: '0 0 2px 0 rgb(145 158 171 / 24%), 0 16px 32px -4px rgb(145 158 171 / 24%)',

    borderRadius: '16px',

    p: 4
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style} style={{'overflow-y':'scroll'}}>
        <Button sx={{ marginBottom: 3 }} onClick={handleClose}>
          Go Back
        </Button>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          Details
        </Typography>
        <Box sx={{ display: 'grid', gridTemplateRows: 'repeat(3, 1fr)' }}>
          <TextField
            value={details.role}
            label="Staff Type"
            name="staffType"
            variant="outlined"
            sx={{ marginTop: 5 }}
            contentEditable={'false'}
          />
          <TextField
            value={details.email}
            label="Student Email"
            name="studentEmail"
            variant="outlined"
            sx={{ marginTop: 5 }}
            contentEditable={'false'}
          />
          <TextField
            value={details.degree}
            label="Student Degree"
            name="studentDegree"
            variant="outlined"
            sx={{ marginTop: 5 }}
            contentEditable={'false'}
          />
          <TextField
            value={details.year}
            label="Student Year"
            name="studentYear"
            variant="outlined"
            sx={{ marginTop: 5 }}
            contentEditable={'false'}
          />
          <TextField
            value={details.comment}
            label="Student Comment"
            name="studentComment"
            variant="outlined"
            sx={{ marginTop: 5 }}
            contentEditable={'false'}
          />
          <TextField
            value={details.method}
            label="Meeting Method"
            name="method"
            variant="outlined"
            sx={{ marginTop: 5 }}
            contentEditable={'false'}
          />
          <TextField
            value={details.place}
            label="Meeting Place"
            name="place"
            variant="outlined"
            sx={{ marginTop: 5 }}
            contentEditable={'false'}
          />
        </Box>

      </Box>
    </Modal>
  );
}
