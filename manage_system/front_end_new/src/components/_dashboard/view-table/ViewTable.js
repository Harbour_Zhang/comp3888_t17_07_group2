import { useState } from 'react';
import {
  Box,
  Modal,
  Button,
  Typography,
  TextField,
} from '@mui/material';

export default function ViewTable({
  open,
  handleClose,
  details
}) {
  const [questionData, setQuestionData] = useState({
    prohibition: details.prohibition === null ? '' : details.prohibition,
    prerequests: details.prerequests === null ? '' : details.prerequisites,
  });
  const { prohibition, prerequests } = questionData;
  const onChange = (e) => {
    setQuestionData({ ...questionData, [e.target.name]: e.target.value });
  };

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    height: '60%',

    bgcolor: '#ffff',
    border: 'none',
    boxShadow: '0 0 2px 0 rgb(145 158 171 / 24%), 0 16px 32px -4px rgb(145 158 171 / 24%)',

    borderRadius: '16px',

    p: 4
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
      <Button sx={{ marginBottom: 3 }} onClick={handleClose}>
          Go Back
        </Button>
        <Typography id="modal-modal-title" variant="h6" component="h2">
          More Info
        </Typography>
        <Box sx={{ display: 'grid', gridTemplateRows: 'repeat(3, 1fr)' }}>
          <TextField
            value={prohibition}
            label="Unit Prohibition"
            name="prohibition"
            variant="outlined"
            onChange={onChange}
            sx={{ marginTop: 5 }}
          />
          <TextField
            value={prerequests}
            label="Unit Prerequests"
            name="prerequisites"
            variant="outlined"
            sx={{ marginTop: 5 }}
            onChange={onChange}
          />
        </Box>
        
      </Box>
    </Modal>
  );
}
