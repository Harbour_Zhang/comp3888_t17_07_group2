import { useState } from 'react';
import { useTheme } from '@mui/material/styles';
// component
import { ViewTable } from '.';
import Label from '../../Label';

// ----------------------------------------------------------------------

export default function TableMoreMenu({ details, editModalType }) {
  const theme = useTheme();

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const editModal = (details) => {
    let editModal = null;
    editModal = (
      <ViewTable
        handleClose={handleClose}
        open={open}
        details={details}
      />
    );
    return editModal;
  };
  return (
    <>
      <Label
        variant={theme.palette.mode === 'filled'}
        color='success'
        onClick={() => handleOpen()}
      >
        More
      </Label>

      {editModal(details)}

    </>
  );
}
