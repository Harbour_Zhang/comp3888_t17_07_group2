import { Icon } from '@iconify/react';
import downloadFill from '@iconify/icons-eva/download-fill';
// material
import { Stack, Button } from '@mui/material';
// ----------------------------------------------------------------------

export default function YMLToolbar({url}) {

  return (
    <>
      <Stack mb={5} direction="row" justifyContent="flex-end" spacing={1.5}>
        <Button
          color="success"
          size="small"
          variant="contained"
          href={'https://docs.google.com/forms/d/1bAhMxD4AeuiwJdaLtr-YRI15DjRlc3kfK7dzuelP9p0/edit#responses'}
          sx={{ mx: 1 }}
        >
          Response
        </Button>
        <Button
          color="info"
          size="small"
          variant="contained"
          href={url}
          endIcon={<Icon icon={downloadFill} />}
          sx={{ mx: 1 }}
        >
          Download
        </Button>
      </Stack>

    </>
  );
}
