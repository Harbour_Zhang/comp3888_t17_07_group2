import React from 'react';
import PropTypes from 'prop-types';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { Form, FormikProvider, useFormik } from 'formik';
// material
import { styled } from '@mui/material/styles';
import { DesktopDatePicker, LoadingButton, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import {
  Card,
  Grid,
  Switch,
  Stack,
  Radio,
  Select,
  TextField,
  InputLabel,
  Typography,
  RadioGroup,
  FormControl,
  FormControlLabel,
  MenuItem
} from '@mui/material';
// utils
import fakeRequest from '../../../utils/fakeRequest';
import Axios from 'axios';

const ANSWER_OPTION = ['Yes', 'No'];

const LabelStyle = styled(Typography)(({ theme }) => ({
  ...theme.typography.subtitle2,
  color: theme.palette.text.secondary,
  marginBottom: theme.spacing(1)
}));

TeacherNewForm.propTypes = {
  isEdit: PropTypes.bool,
};

export default function TeacherNewForm({ isEdit, teacherInfo, timeInfo }) {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const [date, setDate] = React.useState(new Date('2021-7-14'));
  const [time, setTime] = React.useState('8');
  const [hour, setHour] = React.useState('1');
  const [loop, setLoop] = React.useState('1');

  function generateNewTime(){
    return time + "," + hour + ',' + loop;
  }

  function addTime(){
    console.log(date);
    Axios.post('http://localhost:3002/teacher/addTeacherATime', {
      teacherId:teacherInfo.id,
      date: date,
      time: generateNewTime(),
    }).then((res) => {
      console.log(res);
      if(res.data.status === 200){
        enqueueSnackbar(!isEdit ? 'Add success' : 'Add success', { variant: 'success' });
        Axios.post('http://localhost:3002/teacher/getATeacherAllInfo', {
          teacherId:teacherInfo.id,
        }).then((res) => {
          if(res.data.status === 200){
            formik.setFieldValue('available_time', res.data.time,false);
          }
        })
      }
    })
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: teacherInfo?.name ||'',
      email: teacherInfo?.email || '',
      office_place: teacherInfo?.office || '',
      availability: teacherInfo?.available || 'Yes',
      available_time: timeInfo || '',
      status: false
    },
    // validationSchema: NewProductSchema,
    onSubmit: async (values, { setSubmitting, resetForm, setErrors }) => {
      try {
        await fakeRequest(500);
        resetForm();
        setSubmitting(false);
        if(isEdit){
          // console.log(formik.values.available_time.toString());
          // console.log(formik.values.name)
          Axios.post('http://localhost:3002/teacher/updateTeacher', {
            teacherId:teacherInfo.id,
            name: formik.values.name,
            email: formik.values.email,
            officePlace: formik.values.office_place,
            available: formik.values.availability,
            time: formik.values.available_time.toString()
          }).then((res) => {
            if(res.data.status === 200){
              enqueueSnackbar(!isEdit ? 'Create success' : 'Update success', { variant: 'success' });
              navigate('/dashboard/teachermanage');
            }else{
              enqueueSnackbar('Update Failed', { variant: 'error' });
            }
          })
        }else{
          Axios.post('http://localhost:3002/teacher/addTeacher', {
            name: formik.values.name,
            email: formik.values.email,
            officePlace: formik.values.office_place,
            available: 'Yes',
          }).then((res) => {
            if(res.data.status === 200){
              enqueueSnackbar(!isEdit ? 'Create success' : 'Update success', { variant: 'success' });
              navigate('/dashboard/teachermanage');
            }else{
              enqueueSnackbar('Add Failed', { variant: 'error' });
            }
          })
        }
      } catch (error) {
        console.error(error);
        setSubmitting(false);
        setErrors(error);
      }
    }
  });

  const { values, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form noValidate autoComplete="off" onSubmit={handleSubmit}>

        <Grid container spacing={3}>
          <Grid item xs={12} md={8}>
            <Card sx={{ p: 3 }}>
              <Stack spacing={3}>
                <LabelStyle>Name</LabelStyle>
                <TextField
                  id={'name'}
                  fullWidth
                  {...getFieldProps('name')}
                  focused
                />
                <LabelStyle>Email</LabelStyle>
                <TextField
                  id={'email'}
                  fullWidth
                  {...getFieldProps('email')}
                  focused
                />
                <div>
                  <LabelStyle>Office Place</LabelStyle>
                  <TextField
                    id="office_place"
                    multiline
                    rows={4}
                    fullWidth
                    defaultValue=""
                    {...getFieldProps('office_place')}
                    focused
                  />
                </div>
                {isEdit ? (<div>
                  <LabelStyle>Available Time</LabelStyle>
                  <TextField
                    fullWidth
                    multiline
                    maxRows={5}
                    {...getFieldProps('available_time')}
                    focused
                  />
                  <LabelStyle>If you want to change the left meetings, please follow the format that:</LabelStyle>
                  <LabelStyle>2021-07-03T21:00:00</LabelStyle>
                  <LabelStyle>Note:
                    <p>
                      if single num month please add 0 before: 07, 08, etc.
                    </p>
                    <p>
                      please do not delete the comma between each time.
                    </p>
                    <p>
                      please use add meeting button to add meetings.
                    </p>
                  </LabelStyle>
                </div>):(<div></div>)}
              </Stack>
            </Card>
          </Grid>


          <Grid item xs={12} md={4}>
            <Stack spacing={5}>
              {isEdit ? (
                <div>
                  <Card sx={{p:3}}>
                    <LabelStyle>Add A Time</LabelStyle>
                    <br/>
                    <Grid xs={12}>
                      <FormControl fullWidth>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                          <DesktopDatePicker
                            label="Date"
                            value={date}
                            minDate={new Date('2020-01-01')}
                            onChange={(newValue) => {
                              // console.log(newValue);
                              setDate(newValue);
                            }}
                            renderInput={(params) => <TextField {...params} />}
                          />
                        </LocalizationProvider>
                      </FormControl>
                    </Grid>
                    <br/>
                    <Grid xs={12}>
                      <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Time</InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={time}
                          label="Time"
                          onChange={(e)=>{
                            setTime(e.target.value);
                          }}
                        >
                          <MenuItem value={'8'}>8:00:00</MenuItem>
                          <MenuItem value={'9'}>9:00:00</MenuItem>
                          <MenuItem value={'10'}>10:00:00</MenuItem>
                          <MenuItem value={'11'}>11:00:00</MenuItem>
                          <MenuItem value={'12'}>12:00:00</MenuItem>
                          <MenuItem value={'13'}>13:00:00</MenuItem>
                          <MenuItem value={'14'}>14:00:00</MenuItem>
                          <MenuItem value={'15'}>15:00:00</MenuItem>
                          <MenuItem value={'16'}>16:00:00</MenuItem>
                          <MenuItem value={'17'}>17:00:00</MenuItem>
                          <MenuItem value={'18'}>18:00:00</MenuItem>
                          <MenuItem value={'19'}>19:00:00</MenuItem>
                          <MenuItem value={'20'}>20:00:00</MenuItem>
                        </Select>
                      </FormControl>

                    </Grid>
                    <br/>
                    <Grid xs={12}>
                      <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Hour</InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={hour}
                          label="Hour"
                          onChange={(e)=>{
                            setHour(e.target.value);
                          }}
                        >
                          <MenuItem value={1}>1</MenuItem>
                          <MenuItem value={2}>2</MenuItem>
                          <MenuItem value={3}>3</MenuItem>
                          <MenuItem value={4}>4</MenuItem>
                          <MenuItem value={5}>5</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <br />
                    <Grid xs={12}>
                      <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Loop</InputLabel>
                        <Select
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={loop}
                          label="Loop"
                          onChange={(e)=>{
                            setLoop(e.target.value);
                          }}
                        >
                          <MenuItem value={1}>1</MenuItem>
                          <MenuItem value={2}>2</MenuItem>
                          <MenuItem value={3}>3</MenuItem>
                          <MenuItem value={4}>4</MenuItem>
                          <MenuItem value={5}>5</MenuItem>
                          <MenuItem value={6}>6</MenuItem>
                          <MenuItem value={7}>7</MenuItem>
                          <MenuItem value={8}>8</MenuItem>
                          <MenuItem value={9}>9</MenuItem>
                          <MenuItem value={10}>10</MenuItem>
                          <MenuItem value={11}>11</MenuItem>
                          <MenuItem value={12}>12</MenuItem>
                          <MenuItem value={13}>13</MenuItem>
                          <MenuItem value={14}>14</MenuItem>
                          <MenuItem value={15}>15</MenuItem>
                        </Select>
                      </FormControl>
                    </Grid>
                    <br />
                    <Grid xs={12}>
                      <LoadingButton fullWidth variant="contained" size="large" onClick={addTime}>
                        Add Time
                      </LoadingButton>
                    </Grid>
                  </Card>
                  <br />
                  <Card sx={{ p: 3 }}>
                    <Stack spacing={3}>
                      <div>
                        <LabelStyle>Availability</LabelStyle>
                        <RadioGroup {...getFieldProps('availability')} row>
                          <Stack spacing={1} direction="row">
                            {ANSWER_OPTION.map((answer) => (
                              <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                            ))}
                          </Stack>
                        </RadioGroup>
                      </div>

                    </Stack>
                  </Card>
                </div>
              ): (
                <Card sx={{ p: 3 }}>
                  <FormControlLabel
                    control={<Switch {...getFieldProps('status')} checked={values.status} />}
                    label="In meeting"
                    sx={{ mb: 2 }}
                  />
                  <Stack spacing={3}>
                    <div>
                      <LabelStyle>Availability</LabelStyle>
                      <RadioGroup {...getFieldProps('availability')} row>
                        <Stack spacing={1} direction="row">
                          {ANSWER_OPTION.map((answer) => (
                            <FormControlLabel key={answer} value={answer} control={<Radio />} label={answer} />
                          ))}
                        </Stack>
                      </RadioGroup>
                    </div>

                  </Stack>
                </Card>
              )}

              <LoadingButton type="submit" fullWidth variant="contained" size="large" loading={isSubmitting}>
                {!isEdit ? 'Create Teacher' : 'Save Changes'}
              </LoadingButton>
            </Stack>
          </Grid>
        </Grid>
      </Form>
    </FormikProvider>
  );
}
