import { Box, Card, Stack, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import UserCheck from '../../../../security/UserCheck';

// ----------------------------------------------------------------------

export default function AccountGeneral() {

  let navigate = useNavigate();
  useEffect(() => {
      UserCheck().then((res) => {
        if(!res){
          navigate('/login');
        }
      });
    }
    , [])

  function logOut() {
    if (window.confirm("Confirm Logout?")) {
      localStorage.clear();
      sessionStorage.clear();
      document.cookie = "userID= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
      navigate('/login');
    }else{
      ;
    }
  }

  return (
    <Card sx={{ p: 3 }}>
      <Stack spacing={{ xs: 2, md: 3 }}>
        <TextField fullWidth disabled value={"Admin"} />
      </Stack>

      <Box sx={{ mt: 3, display: "flex", justifyContent: "flex-end" }}>
        <LoadingButton
          type="submit"
          variant="contained"
          onClick={logOut}
        >
          Log Out
        </LoadingButton>
      </Box>
    </Card>
  );
}
