import * as Yup from "yup";
import { useSnackbar } from "notistack";
import { useFormik, Form, FormikProvider } from "formik";
// material
import { Stack, Card, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
// utils
import { useNavigate } from 'react-router-dom';
import fakeRequest from "../../../../utils/fakeRequest";
import React, { useEffect } from 'react';
import Axios from "axios";
import UserCheck from '../../../../security/UserCheck';

// ----------------------------------------------------------------------

export default function AccountChangePassword() {
  let navigate = useNavigate();
  useEffect(() => {
      UserCheck().then((res) => {
        if(!res){
          navigate('/login');
        }
      });
    }
    , [])

  const { enqueueSnackbar } = useSnackbar();

  const ChangePassWordSchema = Yup.object().shape({
    oldPassword: Yup.string().required("Old Password is required"),
    newPassword: Yup.string()
      .min(6, "Password must be at least 6 characters")
      .required("New Password is required"),
    confirmNewPassword: Yup.string().oneOf(
      [Yup.ref("newPassword"), null],
      "Passwords must match"
    ),
  });

  const formik = useFormik({
    initialValues: {
      oldPassword: "",
      newPassword: "",
      confirmNewPassword: "",
    },
    validationSchema: ChangePassWordSchema,
    onSubmit: async (values, { setSubmitting }) => {
      await fakeRequest(500);
      setSubmitting(false);
      alert(JSON.stringify(values, null, 2));

      const unikey = "1";
      const oldPassword = values.oldPassword;
      const newPassword = values.newPassword;

      Axios.post("http://localhost:3002/changePassword", {
        username: unikey,
        oldPassword: oldPassword,
        newPassword: newPassword,
      }).then((response) => {
        console.log(response);
        if (response.data.status === 200) {
          enqueueSnackbar("Change success", { variant: "success" });
        } else {
          enqueueSnackbar("Change Failed", { variant: "error" });
          if (response.data.failType === 0) {
            formik.setErrors({ oldPassword: "old password not match" });
          }
        }
        //setMessage(response.data.returnMessage);
      });
    },
  });

  const { errors, touched, isSubmitting, handleSubmit, getFieldProps } = formik;

  return (
    <Card sx={{ p: 3 }}>
      <FormikProvider value={formik}>
        <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
          <Stack spacing={3} alignItems="flex-end">
            <TextField
              {...getFieldProps("oldPassword")}
              fullWidth
              autoComplete="on"
              type="password"
              label="Old Password"
              error={Boolean(touched.oldPassword && errors.oldPassword)}
              helperText={touched.oldPassword && errors.oldPassword}
            />

            <TextField
              {...getFieldProps("newPassword")}
              fullWidth
              autoComplete="on"
              type="password"
              label="New Password"
              error={Boolean(touched.newPassword && errors.newPassword)}
              helperText={
                (touched.newPassword && errors.newPassword) ||
                "Password must be minimum 6+"
              }
            />

            <TextField
              {...getFieldProps("confirmNewPassword")}
              fullWidth
              autoComplete="on"
              type="password"
              label="Confirm New Password"
              error={Boolean(
                touched.confirmNewPassword && errors.confirmNewPassword
              )}
              helperText={
                touched.confirmNewPassword && errors.confirmNewPassword
              }
            />

            <LoadingButton
              type="submit"
              variant="contained"
              loading={isSubmitting}
            >
              Save Changes
            </LoadingButton>
          </Stack>
        </Form>
      </FormikProvider>
    </Card>
  );
}
