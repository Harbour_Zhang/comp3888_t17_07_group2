import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { useEffect, useState } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
// material
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination
} from '@mui/material';
import { UserListToolbar } from '../_dashboard/user';
import NLUTableHead from 'src/components/Table/NLUTableHead';
import Scrollbar from '../Scrollbar';
import SearchNotFound from '../SearchNotFound';
import Page from '../Page';
// ----------------------------------------------------------------------
import TeacherMoreMenu from '../../components/TeacherMoreMenu';
import Axios from 'axios';
import { useSnackbar } from 'notistack';
import UserCheck from '../../security/UserCheck';
// ----------------------------------------------------------------------
const TABLE_HEAD = [
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'email', label: 'Email', alignRight: false },
  { id: 'available', label: 'Available', alignRight: false },
  { id: 'office', label: 'Office Place', alignRight: false },
  { id: '' }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (teacher) => teacher.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function TeacherInfoDashboard() {
  const { enqueueSnackbar } = useSnackbar();
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [teacherList, setTeacherList] = useState([]);

  let navigate = useNavigate();
  useEffect(() => {
    UserCheck().then((res) => {
      if(!res){
        navigate('/login');
      }
    });
    Axios.get('http://localhost:3002/dashboard/meetings/getAllStaff').then((res) => {
      if(res.data.status === 200){
        const filterList = applySortFilter(res.data.infoRow, getComparator(order, orderBy), filterName);
        setTeacherList(filterList);
      }
    })
  },[filterName,orderBy,selected,order])


  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const handleDeleteTeacher = (teacherId) => {
    Axios.post('http://localhost:3002/teacher/deleteATeacher', {
      teacherId: teacherId
    }).then((res) => {
      if(res.data.status === 200){
        enqueueSnackbar('Delete Success', { variant: 'success' });
        Axios.get('http://localhost:3002/dashboard/meetings/getAllStaff').then((res) => {
          if(res.data.status === 200){
            const filterList = applySortFilter(res.data.infoRow, getComparator(order, orderBy), filterName);
            setTeacherList(filterList);
          }
        })
      }else{
        enqueueSnackbar('Delete Failed', { variant: 'error' });
      }
    })
    console.log("deleted")
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - teacherList.length) : 0;

  // const filteredUsers = applySortFilter(teacherList, getComparator(order, orderBy), filterName);

  const isUserNotFound = teacherList.length === 0;

  return (
    <Page title="Teachers">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom style={{color:'#e64727f2'}}>
            Teachers
          </Typography>
          <Button
            variant="contained"
            component={RouterLink}
            to="/dashboard/teachermanage/new"
            startIcon={<Icon icon={plusFill} />}
          >
            New Teacher
          </Button>
        </Stack>

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <NLUTableHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={teacherList.length}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {teacherList
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      const { id, name, email, available, office} = row;
                      const isItemSelected = selected.indexOf(id) !== -1;

                      return (
                        <TableRow
                          hover
                          key={id}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                        >
                          <TableCell padding="checkbox">
                          </TableCell>
                          <TableCell component="th" scope="row" padding="none">
                            <Stack direction="row" alignItems="center" spacing={2}>
                              <Avatar alt={name} src={'https://www.google.com/logos/doodles/2016/andrejs-jurjanss-160th-birthday-5206479465349120-hp.jpg'} />
                              <Typography variant="subtitle2" noWrap>
                                {name}
                              </Typography>
                            </Stack>
                          </TableCell>
                          <TableCell align="left">{email}</TableCell>
                          <TableCell align="left">{available ? 'Yes' : 'No'}</TableCell>
                          <TableCell align="left">{office}</TableCell>
                          <TableCell align="right">
                            <TeacherMoreMenu onDelete={() => handleDeleteTeacher(id)} teacherId={id}/>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={teacherList.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </Page>
  );
}
