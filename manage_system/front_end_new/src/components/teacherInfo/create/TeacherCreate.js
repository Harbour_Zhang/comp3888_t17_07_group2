import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
// material
import { Container } from "@mui/material";
// redux
// hooks
// import useSettings from '../../hooks/useSettings';
// components
import Page from "../../Page";
import TeacherNewForm from "../../_dashboard/teacher-table/TeacherNewForm";
import HeaderBreadcrumbs from "../../HeaderBreadcrumbs";
import Axios from 'axios';
import UserCheck from '../../../security/UserCheck';
// ----------------------------------------------------------------------

export default function TeacherCreate() {
  const { pathname } = useLocation();
  const isEdit = pathname.includes("edit");
  const [teacherInfo, setTeacherInfo] = useState('');
  const [timeInfo, setTimeInfo] = useState('');
  let navigate = useNavigate();

  useEffect(()=> {
    UserCheck().then((res) => {
      if(!res){
        navigate('/login');
      }
    });
    if(isEdit){
      var currentURL = window.location.href;
      var currentTeacherId = currentURL.split('/');
      // console.log(currentTeacherId[5]);
      Axios.post('http://localhost:3002/teacher/getATeacherAllInfo', {
        teacherId:currentTeacherId[5],
      }).then((res) => {
        if(res.data.status === 200){
          setTeacherInfo(res.data.staff);
          setTimeInfo(res.data.time);
        }
      })
    }
  },[])

  return (
    <Page title="Teachers">
      <Container maxWidth="lg">
        <HeaderBreadcrumbs
          heading={!isEdit ? "Create a new teacher " : "Edit teacher info"}
          links={[
            { name: "Dashboard", href: "/dashboard" },
            { name: "Teacher Table", href: "/dashboard/teachermanage" },
            { name: "Teacher Details" },
          ]}
        />
        <TeacherNewForm isEdit={isEdit} teacherInfo={teacherInfo} timeInfo={timeInfo}/>
      </Container>
    </Page>
  );
}
