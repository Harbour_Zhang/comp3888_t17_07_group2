const commonQuestions = [
  {
    id: 1,
    type: 'type a',
    question: 'what is ur name',
    answer: 'Oliver'
  },
  {
    id: 2,
    type: 'type b',
    question: 'what is ur height',
    answer: '200cm'
  },
  {
    id: 3,
    type: 'type c',
    question: 'what is ur weight',
    answer: '100kg'
  }
];
export default commonQuestions;
