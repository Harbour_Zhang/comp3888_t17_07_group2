// ----------------------------------------------------------------------

const account = {
  displayName: 'Admin',
  email: 'demo@minimals.cc',
  photoURL: '/static/mock-images/avatars/avatar_default.jpg',
  role: 'Lecturer'
};

export default account;
