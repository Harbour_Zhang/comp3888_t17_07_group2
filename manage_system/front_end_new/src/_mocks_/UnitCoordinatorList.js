const COORDINATORS = [
    {
        id: 1,
        unit: 'MATH1021',
        coordinator: 'Daniel Hauer',
    },
    {
        id: 2,
        unit: 'MATH1064',
        coordinator: 'Jonathan Spreer'
    },
    {
        id: 3,
        unit: 'INFO1110',
        coordinator: 'Vera Yuk Ying Chung',
    },
    {
        id: 4,
        unit: 'INFO1910',
        coordinator: 'Vera Yuk Ying Chung',
    },
    {
        id: 5,
        unit: 'INFO1111',
        coordinator: 'David Brian Lowe',
    },
    {
        id: 6,
        unit: 'INFO1112',
        coordinator: 'Robert Kummerfeld',
    },
    {
        id: 7,
        unit: 'INFO1113',
        coordinator: 'Mohammad Masbaul Alam Polash',
    },
    {
        id: 8,
        unit: 'DATA1001',
        coordinator: 'Di Warren',
    }
]
export default COORDINATORS;