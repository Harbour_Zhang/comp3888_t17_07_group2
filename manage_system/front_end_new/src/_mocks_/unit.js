
const UNITLIST = [
    {
        id:1,
        unitID: 'COMP2017',
        credits: 6,
        major: 'CS',
        IsCore: false,
        IsElective: true,
        link: 'https://www.sydney.edu.au/units/COMP2017',
        year: 2021,
        prohibition: 'COMP2129 OR COMP9017 OR COMP9129',
        prerequests: 'INFO1113 OR INFO1105 OR INFO1905 OR INFO1103'
    },
    {
        id:2,
        unitID: 'MATH1021',
        credits: 3,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/MATH1021',
        year: 2021,
        prohibition: 'MATH1011 or MATH1901 or MATH1906 or ENVX1001 or MATH1001 or MATH1921 or MATH1931',
        prerequests: 'None'
    },
    {
        id:3,
        unitID: 'MATH1921',
        credits: 3,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/MATH1921',
        year: 2021,
        prohibition: 'MATH1001 or MATH1011 or MATH1906 or ENVX1001 or MATH1901 or MATH1021 or MATH1931',
        prerequests: 'None'
    },
    {
        id:4,
        unitID: 'MATH1064',
        credits: 6,
        major: 'None',
        IsCore: false,
        IsElective: true,
        link: 'https://www.sydney.edu.au/units/MATH1064',
        year: 2021,
        prohibition: 'MATH1001 or MATH1011 or MATH1906 or ENVX1001 or MATH1901 or MATH1021 or MATH1931',
        prerequests: 'None'
    },
    {
        id:5,
        unitID: 'DATA1001',
        credits: 6,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/DATA1001',
        year: 2021,
        prohibition: 'DATA1901 or MATH1005 or MATH1905 or MATH1015 or MATH1115 or ENVX1001 or ENVX1002 or ECMT1010 or BUSS1020 or STAT1021',
        prerequests: 'None'
    },
    {
        id:6,
        unitID: 'INFO1110',
        credits: 6,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/INFO1110',
        year: 2021,
        prohibition: 'INFO1910 or INFO1103 or INFO1903 or INFO1105 or INFO1905 or ENGG1810',
        prerequests: 'None'
    },
    {
        id:7,
        unitID: 'INFO1111',
        credits: 6,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/INFO1111',
        year: 2021,
        prohibition: 'ENGG1805 OR ENGG1111 OR ENGD1000',
        prerequests: 'None'
    },
    {
        id:8,
        unitID: 'INFO1112',
        credits: 6,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/INFO1112',
        year: 2021,
        prohibition: 'None',
        prerequests: 'None'
    },
    {
        id:9,
        unitID: 'INFO1113',
        credits: 6,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/INFO1113',
        year: 2021,
        prohibition: 'INFO1103 OR INFO1105 OR INFO1905',
        prerequests: 'INFO1110 OR INFO1910 OR ENGG1810'
    },
    {
        id:10,
        unitID: 'INFO1910',
        credits: 6,
        major: 'None',
        IsCore: true,
        IsElective: false,
        link: 'https://www.sydney.edu.au/units/INFO1910',
        year: 2021,
        prohibition: 'INFO1110 OR INFO1103 OR INFO1903 OR INFO1105 OR INFO1905',
        prerequests: 'None'
    }
]

export default UNITLIST

/*
MATH1021
    - MATH1921
    - MATH1002
    - MATH1064
    - DATA1001
    - INFO1110
    - INFO1111
    - INFO1112
    - INFO1113
    - INFO1910
    - COMP2123
    - COMP2823
    - INFO2222
    - ISYS2120
    - SOFT2412
    - INFO3333
    - INFO4001
    - INFO4002
    - INFO4444
    - COMP2017
    - COMP2022
    - COMP2922
    - COMP3027
    - COMP3927
    - COMP3221
    - COMP3308
    - COMP3608
    - COMP3419
    - COMP3520
    - COMP3888
    - COMP3988
    - ISYS2110
    - ISYS3401
    - ISYS2160
    - ISYS3888
    - SOFT3410
    - SOFT3202
    - SOFT2201
    - SOFT3888
    - DATA3404
    - DATA3406
    - DATA3001
    - DATA2002
    - DATA2001
    - DATA3888
    - COMP3109
    - INFO2150
    - INFO3315
    - INFO3616
*/