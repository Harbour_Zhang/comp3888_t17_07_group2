const academicQuestions = [
  {
    id: 1,
    coordinatorType: 'Academic Staff',
    name: 'Oliver',
    contactMethod: 'hhh@gmail.com'
  },
  {
    id: 2,
    coordinatorType: 'Unit Staff',
    name: 'Oliva',
    contactMethod: 'ccc@gmail.com'
  },
  {
    id: 3,
    coordinatorType: 'School Staff',
    name: 'Olva',
    contactMethod: 'c@gmail.com'
  }
];
export default academicQuestions;
