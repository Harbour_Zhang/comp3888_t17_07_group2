// routes
import Router from './routes';
// theme
import ThemeConfig from './theme';
import GlobalStyles from './theme/globalStyles';
// components
import NotistackProvider from './components/NotistackProvider';
import ScrollToTop from './components/ScrollToTop';
import { BaseOptionChartStyle } from './components/charts/BaseOptionChart';


// ----------------------------------------------------------------------

export default function App() {
  return (
    <ThemeConfig>
      <NotistackProvider>
      <ScrollToTop />
      <GlobalStyles />
      <BaseOptionChartStyle />
      <Router />
      </NotistackProvider>
    </ThemeConfig>
  );
}
