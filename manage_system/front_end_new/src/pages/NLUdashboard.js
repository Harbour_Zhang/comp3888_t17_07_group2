// material
import { Container,  Grid } from '@mui/material';
// components
import Page from '../components/Page';
import TopicBox from '../components/TopicBox'
import {YMLToolbar} from '../components/_dashboard/yml';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import UserCheck from '../security/UserCheck';
// ----------------------------------------------------------------------

const boxList = [
  {
    id: 1,
    name: 'Academic coordinators',
    description: 'Year advisors and Course advisors',
    path: '/dashboard/academic_table'
  },
  {
    id: 2,
    name: 'Honours related Questions',
    description: 'Honours requirements and honours description',
    path: '/dashboard/hornous_table'
  },
  {
    id: 3,
    name: 'Unit Information',
    description: 'Core units, major units and information about each unit',
    path: '/dashboard/unit_table'
  },
  {
    id: 4,
    name: 'Degree Question',
    description: 'Degree related questions - Bachelor of advanced computing',
    path: '/dashboard/degree_table'
  },
  {
    id: 5,
    name: 'Common Question',
    description: 'Frequent FAQs and Canvas portal questions',
    path: '/dashboard/faq_table'
  },
  {
    id: 6,
    name: 'Unit Coordinator',
    description: 'The table of unit and its coordinator',
    path: '/dashboard/unit_coordinator_table'
  }

]

export default function NLUdashboard() {
  let navigate = useNavigate();
  useEffect(() => {
      UserCheck().then((res) => {
        if(!res){
          navigate('/login');
        }
      });
    }
    , [])
  return (
    <Page title="Dashboard: NLU management dashboard">
      <Container sx={{ py: 8 }} maxWidth="md">
          <YMLToolbar url={"http://localhost:3002/file/getZip"}/>

          <Grid container spacing={4}>
              {boxList.map((item) => (
                <TopicBox key={item.id} id={item.id} name={item.name} description={item.description} path={item.path}/>
              ))}
          </Grid>

        </Container>
    </Page>
  );
}
