import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import { filter } from 'lodash';
import { useEffect, useState } from 'react';
import Page from '../../components/Page';
import {Card, Container, Stack, TablePagination } from '@mui/material';
import NLUTableHead from 'src/components/Table/NLUTableHead';
import {UserListToolbar } from '../../components/_dashboard/user';
import Scrollbar from '../../components/Scrollbar';
import SearchNotFound from '../../components/SearchNotFound';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import MeetingsMoreView from '../../components/_dashboard/meetings/MeetingsMoreView';
import Axios from 'axios';
import UserCheck from '../../security/UserCheck';
import { useNavigate } from 'react-router-dom';

const TABLE_HEAD = [
    { id: 'meetingid', label: 'Meeting ID', alignRight: false },
    { id: 'date', label: 'Date', alignRight: false },
    { id: 'studentName', label: 'Student Name', alignRight: false },
    { id: 'teacherName', label: 'Teacher Name', alignRight: false },
    { id: 'status', label: 'Status', alignRight: false },
    { id: 'more', label: 'More', alignRight: false }
];

function descendingComparator(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator(order, orderBy) {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    if (query) {
        return filter(array, (meeting) => meeting.student.toLowerCase().indexOf(query.toLowerCase()) !== -1);
    }
    return stabilizedThis.map((el) => el[0]);
}

export default function InScheduleMeetings() {
    const [page, setPage] = useState(0);
    const [order, setOrder] = useState('asc');
    const [selected, setSelected] = useState([]);
    const [orderBy, setOrderBy] = useState('name');
    const [filterName, setFilterName] = useState('');
    const [rowsPerPage, setRowsPerPage] = useState(5);

    const [meetingsList,setMeetingsList] = useState([]);
    var found = 0;
    let navigate = useNavigate();
    useEffect(() => {
        UserCheck().then((res) => {
            if(!res){
                navigate('/login');
            }
        });
       Axios.get('http://localhost:3002/dashboard/meetings/inScheduleMeetings').then((response) => {
           if(response.data.status === 100){
               found = 0;
           }else{
               setMeetingsList(response.data.infoRow);
           }
       }).catch((error)=>{
           console.log(error);
       })
    },[])

    const handleRequestSort = (event, property) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const handleFilterByName = (event) => {
        setFilterName(event.target.value);
    };

    const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - meetingsList.length) : 0;

    const filteredUsers = applySortFilter(meetingsList, getComparator(order, orderBy), filterName);

    const isUserNotFound = filteredUsers.length === 0;

    return (
      <Page title="In Schedule Meetings">
          <Container>
              <HeaderBreadcrumbs
                heading="In Schedule Meetings"
                links={[
                    { name: 'Dashboard', href: "/dashboard/meetings" },
                    { name: 'In Schedule Meetings', href: "/dashboard/InScheduleMeetings" }
                ]}
              />

              <Card>
                  <UserListToolbar
                    numSelected={selected.length}
                    filterName={filterName}
                    onFilterName={handleFilterByName}
                  />

                  <Scrollbar>
                      <TableContainer sx={{ minWidth: 800 }}>
                          <Table>
                            <NLUTableHead
                                order={order}
                                orderBy={orderBy}
                                headLabel={TABLE_HEAD}
                                rowCount={filteredUsers.length}
                                onRequestSort={handleRequestSort}
                            />
                              <TableBody>
                                  {filteredUsers
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((row) => {
                                        const { id, date, student, staff, status  } = row;
                                        const isItemSelected = selected.indexOf(id) !== -1;

                                        return (
                                          <TableRow
                                            hover
                                            key={id}
                                            tabIndex={-1}
                                            role="checkbox"
                                            selected={isItemSelected}
                                            aria-checked={isItemSelected}
                                          >
                                            <TableCell component="th" scope="row" align="left" />

                                              <TableCell align={'left'}>
                                                  <Stack direction="row" alignItems="center" spacing={2}>
                                                      <Typography variant="subtitle2" noWrap>
                                                          {id}
                                                      </Typography>
                                                  </Stack>
                                              </TableCell>
                                              <TableCell align="left">{date}</TableCell>
                                              <TableCell align="left">{student}</TableCell>
                                              <TableCell align="left">{staff}</TableCell>
                                              <TableCell align={'left'}>{status}</TableCell>
                                              <TableCell align={'left'}>
                                                  <MeetingsMoreView details={row} />
                                              </TableCell>
                                          </TableRow>
                                        );
                                    })}
                                  {emptyRows > 0 && (
                                    <TableRow style={{ height: 53 * emptyRows }}>
                                        <TableCell colSpan={6} />
                                    </TableRow>
                                  )}
                              </TableBody>
                              {isUserNotFound && (
                                <TableBody>
                                    <TableRow>
                                        <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                                            <SearchNotFound searchQuery={filterName} />
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                              )}
                          </Table>
                      </TableContainer>
                  </Scrollbar>

                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={meetingsList.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                  />
              </Card>
          </Container>
      </Page>
    );
}
