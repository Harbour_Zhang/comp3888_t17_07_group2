// material
import { Container,  Grid } from '@mui/material';
import Page from '../../components/Page';
import MeetingTypeBox from './MeetingTypeBox';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import UserCheck from '../../security/UserCheck';
// components
// ----------------------------------------------------------------------

const boxList = [
  {
    id: 1,
    name: 'In Schedule Meetings',
    description: 'Meetings That Not Start Yet',
    path: '/dashboard/InScheduleMeetings'
  },
  {
    id: 2,
    name: 'Past Meetings',
    description: 'Meetings That Finished',
    path: '/dashboard/PastMeetings'
  },
  {
    id: 3,
    name: 'All Meeting',
    description: 'All The Meetings Record',
    path: '/dashboard/AllMeetings'
  }

]

export default function MeetingsDashBoard() {
  let navigate = useNavigate();
  useEffect(() => {
      UserCheck().then((res) => {
        if(!res){
          navigate('/login');
        }
      });
  }
  , [])
  return (
    <Page title="Dashboard: Meetings">
      <Container sx={{ py: 8 }} maxWidth="md">
        <Grid container spacing={4}>
          {boxList.map((item) => (
            <MeetingTypeBox key={item.id} id={item.id} name={item.name} description={item.description} path={item.path}/>
          ))}
        </Grid>
      </Container>
    </Page>
  );
}
