// material
import { Box, Container, Typography } from "@mui/material";
// components
import React, { useEffect } from 'react';
import Page from "../components/Page";
import { useNavigate } from 'react-router-dom';
import UserCheck from '../security/UserCheck';

// ----------------------------------------------------------------------

export default function DashboardApp() {
  let navigate = useNavigate();
  useEffect(() => {
      UserCheck().then((res) => {
        if(!res){
          navigate('/login');
        }
      });
    }
    , [])
  return (
    <Page title="Dashboard">
      <Container maxWidth="xl">
        <Box sx={{ pb: 5 }}>
          <Typography variant="h4">Hi, Welcome back</Typography>
        </Box>
        <div>
        <img src={'https://i.ytimg.com/vi/A_6pZU3-QvI/maxresdefault.jpg'}  alt={"main"} />
        </div>
      </Container>
    </Page>
  );
}
