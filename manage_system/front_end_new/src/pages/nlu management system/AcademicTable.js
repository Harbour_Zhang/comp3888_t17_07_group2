import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import * as React from 'react';
import { useSnackbar } from 'notistack';
import { sentenceCase } from 'change-case';
import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
// material
import {
  Card,
  Table,
  Stack,
  Button,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination
} from '@mui/material';
import Axios from 'axios';
// components
import Page from '../../components/Page';
import Scrollbar from '../../components/Scrollbar';
import SearchNotFound from '../../components/SearchNotFound';
import NLUTableHead from 'src/components/Table/NLUTableHead';
import {UserListToolbar } from '../../components/_dashboard/user';
import ProductMoreMenu from '../../components/ProductMoreMenu';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
//
import Label from 'src/components/Label';
import UserCheck from '../../security/UserCheck';
// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'coordinator', label: 'Coordinator', alignRight: false },
  { id: 'director', label: 'Director', alignRight: false },
  { id: 'uao', label: 'Undergraduate administrative officer', alignRight: false },
  { id: 'year advisor', label: 'Year Advisor', alignRight: false },
  { id: 'honours coordinator', label: 'Honours Coordinator', alignRight: false },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }

  return stabilizedThis.map((el) => el[0]);
}

export default function AcademicTable() {
  const [page, setPage] = React.useState(0);
  const [order, setOrder] = React.useState('asc');
  const [selected, setSelected] = React.useState([]);
  const [orderBy, setOrderBy] = React.useState('name');
  const [filterName, setFilterName] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [unitList, setUnitList] = React.useState([]);
  const [count, setCount] = React.useState(0);
  const { enqueueSnackbar } = useSnackbar();
  let navigate = useNavigate();

  React.useEffect(() => {
    UserCheck().then((res) => {
      if(!res){
        navigate('/login');
      }
    });
    Axios.get('http://localhost:3002/dashboard/staff_table/get_staff_table', {
        user_id:1,
        dtype: 'json',
    }).then((response) => {
        if(response.data.status == 200){
          setUnitList(response.data.staffList);
        }
    }).catch((error) => {
      console.log(error);
    })
  },[count])

  const filteredUsers = applySortFilter(unitList, getComparator(order, orderBy), filterName);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const handleDeleteProduct = (id) => {
    Axios.post('http://localhost:3002/dashboard/staff_table/delete_staff', {
        user_id:1,
        dtype: 'json',
        staffID: id,
    }).then((response) => {
        if(response.data.status == 200){
          enqueueSnackbar('Delete Successful', { variant: 'success' });
          window.location.reload();
        }
    }).catch((error) => {
      enqueueSnackbar('Delete Error', { variant: 'error' });
    })
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - filteredUsers.length) : 0;

  const isNotFound = filteredUsers.length === 0;
  return (
    <Page title="Academic Coordinator Table">
      <Container>
        <HeaderBreadcrumbs
          heading="Academic Coordinator Table"
          links={[
            { name: 'Dashboard', href: "/dashboard/nlu_management" },
            { name: 'Academic Coordinator Table', href: "/dashboard/academic_table" }
          ]}
          action={
            <Button
              variant="contained"
              component={RouterLink}
              to="/dashboard/academic_table/new"
              startIcon={<Icon icon={plusFill} />}
            >
              New Item
            </Button>
          }
        />

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <NLUTableHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={filteredUsers.length}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {filteredUsers
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const isItemSelected = selected.indexOf(row.name) !== -1;
                      const isCoordinator = row.role.includes('coordinator');
                      const isDirectory = row.role.includes('directory');
                      const isUAO = row.role.includes('undergraduate administrative officer');
                      const isYA = row.role.includes('year advisor');
                      const isHC = row.role.includes('honours coordinator');

                      return (
                        <TableRow
                          hover
                          key={index}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                        >
                          <TableCell component="th" scope="row" align="left">
                            <Stack direction="row" alignItems="center" spacing={2}>
                              <Typography variant="subtitle2" noWrap>
                                {index + 1}
                              </Typography>
                            </Stack>
                          </TableCell>
                          <TableCell align="left">{row.name}</TableCell>

                          <TableCell align="left">
                            <Label
                              color={isCoordinator ? 'success' : 'error'}
                            >
                              {isCoordinator ? sentenceCase('Yes') : sentenceCase('No')}
                            </Label>
                          </TableCell>

                          <TableCell align="left">
                            <Label
                              color={isDirectory ? 'success' : 'error'}
                            >
                              {isDirectory ? sentenceCase('Yes') : sentenceCase('No')}
                            </Label>
                          </TableCell>

                          <TableCell align="left">
                            <Label
                              color={isUAO ? 'success' : 'error'}
                            >
                              {isUAO ? sentenceCase('Yes') : sentenceCase('No')}
                            </Label>
                          </TableCell>

                          <TableCell align="left">
                            <Label
                              color={isYA ? 'success' : 'error'}
                            >
                              {isYA ? sentenceCase('Yes') : sentenceCase('No')}
                            </Label>
                          </TableCell>

                          <TableCell align="left">
                            <Label
                              color={isHC ? 'success' : 'error'}
                            >
                              {isHC  ? sentenceCase('Yes') : sentenceCase('No')}
                            </Label>
                          </TableCell>


                          <TableCell align="right">
                            <ProductMoreMenu onDelete={() => handleDeleteProduct(row.id)} tableName='academic_table' productName={`question${row.id}`} />
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={filteredUsers.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </Page>
  );
}
