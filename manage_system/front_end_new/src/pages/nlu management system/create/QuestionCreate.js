import { useLocation } from "react-router-dom";
// material
import { Container } from "@mui/material";
import Axios from "axios";
import * as React from "react";
// redux
// hooks
// import useSettings from '../../hooks/useSettings';
// components
import Page from "../../../components/Page";
import QuestionNewForm from "../../../components/_dashboard/question-table/QuestionNewForm";
import HeaderBreadcrumbs from "../../../components/HeaderBreadcrumbs";
// ----------------------------------------------------------------------

function getTableName(
  isDegreeTable,
  isFaqTable,
  isAcademicTable,
  isHornousTable
) {
  if (isDegreeTable) {
    return { title: "Degree Table", pathname: "/dashboard/degree_table" };
  } else if (isFaqTable) {
    return { title: "Faq Table", pathname: "/dashboard/faq_table" };
  } else if (isAcademicTable) {
    return {
      title: "Academic Coordinator Table",
      pathname: "/dashboard/academic_table",
    };
  } else if (isHornousTable) {
    return { title: "Hornous Table", pathname: "/dashboard/hornous_table" };
  }
  return { title: "", pathname: "" };
}

export default function UnitCreate() {
  // const { themeStretch } = useSettings();
  // const dispatch = useDispatch();
  const { pathname } = useLocation();
  // const { products } = useSelector((state) => state.product);
  const isEdit = pathname.includes("edit");
  const isDegreeTable = pathname.includes("degree_table");
  const isFaqTable = pathname.includes("faq_table");
  const isAcademicTable = pathname.includes("academic_table");
  const isHornousTable = pathname.includes("hornous_table");
  // const currentProduct = products.find((product) => paramCase(product.name) === name);
  const currentPath = getTableName(
    isDegreeTable,
    isFaqTable,
    isAcademicTable,
    isHornousTable
  );
  const [list, setList] = React.useState([]);
  const [count, setCount] = React.useState(0);

  React.useEffect(() => {
    console.log(isDegreeTable, isFaqTable, isHornousTable);
    if (isEdit) {
      const questionID = pathname.split("/")[3].split("question")[1];
      if(isDegreeTable){
        Axios.get("http://localhost:3002/dashboard/degree_table/get_question", {
          params: {
            user_id: 1,
            questionID: questionID,
            dtype: "json",
          },
        })
          .then((response) => {
            if (response.data.status == 200) {
              setList([response.data.questionInfo]);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
      else if(isFaqTable){
        Axios.get("http://localhost:3002/dashboard/faq_table/get_question", {
          params: {
            user_id: 1,
            questionID: questionID,
            dtype: "json",
          },
        })
          .then((response) => {
            console.log("!!!!! question is ", response);
            if (response.data.status == 200) {
              setList([response.data.questionInfo]);
              console.log("response.data.questionInfo",response.data.questionInfo)
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
      else if(isHornousTable){
        Axios.get("http://localhost:3002/dashboard/hornous_table/get_question", {
          params: {
            user_id: 1,
            questionID: questionID,
            dtype: "json",
          },
        })
          .then((response) => {
            if (response.data.status == 200) {
              setList([response.data.questionInfo]);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
    } else {
      setList([{}]);
    }
  }, [count]);

  return (
    <Page title={`NLU management - ${currentPath.title}`}>
      <Container maxWidth="xl">
        <HeaderBreadcrumbs
          heading={!isEdit ? "Create a new question" : "Edit question"}
          links={[
            { name: "Dashboard", href: "/dashboard/nlu_management" },
            { name: currentPath.title, href: `${currentPath.pathname}` },
            { name: "Question" },
          ]}
        />
        <QuestionNewForm
          isEdit={isEdit}
          currentPath={currentPath.pathname}
          list={list}
        />
      </Container>
    </Page>
  );
}