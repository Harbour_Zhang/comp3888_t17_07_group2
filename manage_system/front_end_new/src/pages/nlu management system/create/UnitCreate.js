
import { useLocation } from 'react-router-dom';
// material
import { Container } from '@mui/material';
import Axios from 'axios';
import * as React from 'react';
// redux
// hooks
// import useSettings from '../../hooks/useSettings';
// components
import Page from '../../../components/Page';
import ProductNewForm from '../../../components/_dashboard/question-table/ProductNewForm';
import HeaderBreadcrumbs from '../../../components/HeaderBreadcrumbs';
// ----------------------------------------------------------------------

export default function UnitCreate() {
  // const { themeStretch } = useSettings();
  // const dispatch = useDispatch();
  const { pathname } = useLocation();
  // const { products } = useSelector((state) => state.product);
  const isEdit = pathname.includes('edit');
  // const currentProduct = products.find((product) => paramCase(product.name) === name);
  const [unitInfo, setUnitInfo] = React.useState([]);
  const [count, setCount] = React.useState(0);

  React.useEffect(() => {
    if(isEdit){
      const unitID = pathname.split("/")[3];
      Axios.get('http://localhost:3002/dashboard/unit_table/get_unit', {
        params: {
          user_id:1,
          unitID: unitID.toUpperCase(),
          dtype: 'json',
        }
      }).then((response) => {
          if(response.data.status === 200){
            console.log("successfully get")
            setUnitInfo([response.data.unitInfo]);
            console.log(unitInfo)
          }

      }).catch((error) => {
        console.log(error);
      })
    }
    else{
      setUnitInfo([{}]);
    }

  }, [count]);



  return (
    <Page title="NLU management - Unit Table">
      <Container maxWidth='lg'>
        <HeaderBreadcrumbs
          heading={!isEdit ? 'Create a new unit' : 'Edit unit'}
          links={[
            { name: 'Dashboard', href: "/dashboard/nlu_management" },
            { name: 'Unit Table', href: "/dashboard/unit_table" },
            { name: 'Unit Details' }
          ]}
        />
        <ProductNewForm isEdit={isEdit} unitInfo={unitInfo}/>
      </Container>
    </Page>
  );
}
