
import { useLocation } from 'react-router-dom';
// material
import { Container } from '@mui/material';
// redux
// hooks
// import useSettings from '../../hooks/useSettings';
// components
import Axios from 'axios';
import * as React from 'react';
import Page from '../../../components/Page';
import UnitNewForm from '../../../components/_dashboard/question-table/UnitNewForm';
import HeaderBreadcrumbs from '../../../components/HeaderBreadcrumbs';
// ----------------------------------------------------------------------

export default function UnitCoordinatorCreate() {
  // const { themeStretch } = useSettings();
  // const dispatch = useDispatch();
  const { pathname } = useLocation();
  // const { products } = useSelector((state) => state.product);
  const isEdit = pathname.includes('edit');
  const unit = pathname.split("/")[2];
  // const currentProduct = products.find((product) => paramCase(product.name) === name);
  const [coordinator, setCoordinator] = React.useState("");
  const [count, setCount] = React.useState(0);
  const [staffList, setStaffList] = React.useState([]);
  const unitID = pathname.split("/")[3].toUpperCase();

  React.useEffect(() => {
      
      console.log("unitId is " + unitID);
      Axios.get('http://localhost:3002/dashboard/unitCoordinator_table/get_unitCoordinator', {
        params: {
          user_id:1,
          unitID: unitID,
          dtype: 'json',
        }
      }).then((response) => {
          if(response.data.status == 200){
            console.log("successfully get")
            
            setCoordinator(response.data.coordinator.name);
            console.log(coordinator)
          } 

      }).catch((error) => {
        console.log(error);
      })

      Axios.get('http://localhost:3002/dashboard/staff_table/get_staff_table', {
        user_id:1,
        dtype: 'json',
      }).then((response) => {
          if(response.data.status == 200){
            console.log("Successfully get the staff table from the server");
            setStaffList(response.data.staffList);
          } 
      }).catch((error) => {
        console.log(error);
      })
    
    
  }, [count]);
  /*
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);
  */
  return (
    <Page title="NLU management - Unit Coordinator Table">
      <Container maxWidth='lg'>
        <HeaderBreadcrumbs
          heading={'Edit unit'}
          links={[
            { name: 'Dashboard', href: "/dashboard/nlu_management" },
            { name: 'Unit Coordinator Table', href: "/dashboard/unit_coordinator_table" },
            { name: 'Unit Details' }
          ]}
        />
        <UnitNewForm isEdit={isEdit} unit={unitID} coordinator={coordinator} staffList={staffList}/>
      </Container>
    </Page>
  );
}
