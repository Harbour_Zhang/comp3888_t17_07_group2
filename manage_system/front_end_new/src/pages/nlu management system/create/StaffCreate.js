import {useLocation } from 'react-router-dom';
// material
import { Container } from '@mui/material';
import Axios from 'axios';
import * as React from 'react';
// redux
// hooks
// import useSettings from '../../hooks/useSettings';
// components
import Page from '../../../components/Page';
import StaffNewForm from '../../../components/_dashboard/question-table/StaffNewForm';
import HeaderBreadcrumbs from '../../../components/HeaderBreadcrumbs';
// ----------------------------------------------------------------------

function getTableName(isDegreeTable, isFaqTable, isAcademicTable, isHornousTable){
  if(isDegreeTable){
    return {title: "Degree Table", pathname: '/dashboard/degree_table'}
  }
  else if(isFaqTable){
    return {title: "Faq Table", pathname: '/dashboard/faq_table'}
  }
  else if(isAcademicTable){
    return {title: "Academic Coordinator Table", pathname: '/dashboard/academic_table'}
  }
  else if(isHornousTable){
    return {title: "Hornous Table", pathname: '/dashboard/hornous_table'}
  }
  return {title: "", pathname: ''}
}

export default function StaffCreate() {
  // const { themeStretch } = useSettings();
  // const dispatch = useDispatch();
  const { pathname } = useLocation();
  // const { products } = useSelector((state) => state.product);
  const isEdit = pathname.includes('edit');
  const isDegreeTable = pathname.includes('degree_table');
  const isFaqTable = pathname.includes('faq_table');
  const isAcademicTable = pathname.includes('academic_table');
  const isHornousTable = pathname.includes('hornous_table');
  // const currentProduct = products.find((product) => paramCase(product.name) === name);
  const currentPath = getTableName(isDegreeTable, isFaqTable, isAcademicTable, isHornousTable);
  const [list, setList] = React.useState([]);
  const [count, setCount] = React.useState(0);

  React.useEffect(() => {
    if(currentPath.pathname == "/dashboard/academic_table"){
      if(isEdit){
      const staffID = pathname.split("/")[3];
      console.log("staffID is ", staffID.charAt(8).toString())
      Axios.get('http://localhost:3002/dashboard/staff_table/get_question', {
        params: {
            user_id:1,
            staffID: staffID.split("question")[1].toString(),
            dtype: 'json',
          }
      }).then((response) => {
          console.log("response")
          console.log(response)
          if(response.data.status == 200){
            console.log("successfully get")
            setList([response.data.roles]);
            console.log(list)
          } 

      }).catch((error) => {
        console.log(error);
      })
    }
    }
    else{
      setList([{}]);
    }
  }, [count]);

  return (
    <Page title='NLU management - Academic Coordinator Table'>
      <Container maxWidth='xl'>
        <HeaderBreadcrumbs
          heading={!isEdit ? 'Create a new question' : 'Edit question'}
          links={[
            { name: 'Dashboard', href: "/dashboard/nlu_management" },
            { name: 'Academic Coordinator Table', href: '/dashboard/academic_table' },
            { name: 'Question' }
          ]}
        />
        <StaffNewForm isEdit={isEdit} currentPath={currentPath.pathname} list={list}/>
      </Container>
    </Page>
  );
}