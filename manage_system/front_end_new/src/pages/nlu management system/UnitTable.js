import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import * as React from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import { useSnackbar } from 'notistack';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
// material
import {
  Card,
  Table,
  Stack,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import Axios from 'axios';
// components
import Page from '../../components/Page';
import Scrollbar from '../../components/Scrollbar';
import SearchNotFound from '../../components/SearchNotFound';
import NLUTableHead from 'src/components/Table/NLUTableHead';
import { UserListToolbar } from '../../components/_dashboard/user';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
// ----------------------------------------------------------------------
import ProductMoreMenu from '../../components/ProductMoreMenu';
import TableMoreMenu from '../../components/_dashboard/view-table/TableMoreMenu';
import UserCheck from '../../security/UserCheck';
// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'unitCode', alignRight: false },
  { id: 'credits', label: 'Credits', alignRight: false },
  { id: 'major', label: 'Major', alignRight: false },
  { id: 'isCore', label: 'Is_Core_Unit', alignRight: false },
  { id: 'isElective', label: 'Is_Elective_Unit', alignRight: false },
  { id: 'year', label: 'Year', alignRight: false },
  { id: 'link', label: 'Link', alignRight: false },
  { id: 'more', label: 'More', alignRight: false }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (_user) => _user.unit_id.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function UnitTable() {
  const theme = useTheme();
  const [page, setPage] = React.useState(0);
  const [order, setOrder] = React.useState('asc');
  const [selected, setSelected] = React.useState([]);
  const [orderBy, setOrderBy] = React.useState('name');
  const [filterName, setFilterName] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [unitList, setUnitList] = React.useState([]);
  const [count, setCount] = React.useState(0);
  const { enqueueSnackbar } = useSnackbar();
  let navigate = useNavigate();
  React.useEffect(() => {
    UserCheck().then((res) => {
      if(!res){
        navigate('/login');
      }
    });
    Axios.get('http://localhost:3002/dashboard/unit_table/get_unit_table', {
        user_id:1,
        dtype: 'json',
    }).then((response) => {
        if(response.data.status == 200){
          setUnitList(response.data.unitList);
        }
    }).catch((error) => {
      console.log(error);
    })
  },[count])

  const filteredUnits = applySortFilter(unitList, getComparator(order, orderBy), filterName);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleDelete = (id) => {
    Axios.post('http://localhost:3002/dashboard/unit_table/delete_unit', {
        user_id:1,
        dtype: 'json',
        unitID: id,
    }).then((response) => {
        if(response.data.status == 200){
          enqueueSnackbar('Delete Successful', { variant: 'success' });
          window.location.reload();
        }
    }).catch((error) => {
      enqueueSnackbar('Delete Error', { variant: 'error' });
    })
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - filteredUnits.length) : 0;

  const isUserNotFound = filteredUnits.length === 0;
  return (
    <Page title="NLU management - Unit Table">
      <Container>
        <HeaderBreadcrumbs
          heading="Unit Table"
          links={[
            { name: 'Dashboard', href: "/dashboard/nlu_management" },
            { name: 'Unit Table', href: "/dashboard/unit_table" }
          ]}
          action={
            <Button
              variant="contained"
              component={RouterLink}
              to="/dashboard/unit_table/new"
              startIcon={<Icon icon={plusFill} />}
            >
              New Item
            </Button>
          }
        />


        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
                <NLUTableHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={filteredUnits.length}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {filteredUnits
                   .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const {unit_id, core, credit, prerequisites, prohibition, link, avaliable, year, staff} = row
                      const isItemSelected = selected.indexOf(unit_id) !== -1;
                      return (
                        <TableRow
                          hover
                          key={index}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                        >
                          <TableCell component="th" scope="row" align="left">
                            <Stack direction="row" alignItems="center" spacing={2}>
                              <Typography variant="subtitle2" noWrap>
                                {index + 1}
                              </Typography>
                            </Stack>
                          </TableCell>
                            <TableCell component="th" scope="row" align="left" >
                              <Stack direction="row" alignItems="center" spacing={2}>
                                <Typography variant="subtitle2" noWrap>
                                  {unit_id}
                                </Typography>
                              </Stack>
                            </TableCell>
                            <TableCell align="left">{credit}</TableCell>
                            <TableCell align="left">{'None'}</TableCell>
                            <TableCell align="left">{core ? 'Yes' : 'No'}</TableCell>
                            <TableCell align="left">{core ? 'Yes' : 'No'}</TableCell>
                            <TableCell align="left">{year}</TableCell>
                            <TableCell align="left">{link}</TableCell>

                            <TableCell align="left">
                            <TableMoreMenu details={row}/>
                          </TableCell>

                          <TableCell align="right">
                            <ProductMoreMenu onDelete={() => handleDelete(unit_id)} tableName='unit_table' productName={unit_id} />
                          </TableCell>
                        </TableRow>

                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isUserNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={filteredUnits.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>

      </Container>

    </Page>
  );
}
