import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import * as React from 'react';
import Axios from 'axios';
import { useSnackbar } from 'notistack';
import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
// material
import {
  Card,
  Table,
  Stack,
  Button,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination
} from '@mui/material';
// components
import Page from '../../components/Page';
import Scrollbar from '../../components/Scrollbar';
import SearchNotFound from '../../components/SearchNotFound';
import NLUTableHead from 'src/components/Table/NLUTableHead';
import { UserListToolbar } from '../../components/_dashboard/user';
import ProductMoreMenu from '../../components/ProductMoreMenu';
import HeaderBreadcrumbs from '../../components/HeaderBreadcrumbs';
import UserCheck from '../../security/UserCheck';
//

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'question', label: 'Question', alignRight: false },
  { id: 'answer', label: 'Answer', alignRight: false },
  { id: '' }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (question) => question.question.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }

  return stabilizedThis.map((el) => el[0]);
}

export default function HornousTable() {
  const [page, setPage] = React.useState(0);
  const [order, setOrder] = React.useState('asc');
  const [selected, setSelected] = React.useState([]);
  const [orderBy, setOrderBy] = React.useState('name');
  const [filterName, setFilterName] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [questionList, setQuestionList] = React.useState([]);
  const [count, setCount] = React.useState(0);
  const { enqueueSnackbar } = useSnackbar();
  let navigate = useNavigate();
  React.useEffect(() => {
    UserCheck().then((res) => {
      if(!res){
        navigate('/login');
      }
    });
    Axios.get('http://localhost:3002/dashboard/hornous_table/get_hornous_table', {
        user_id:1,
        dtype: 'json',
    }).then((response) => {
        if(response.data.status == 200){
          setQuestionList(response.data.honoursTableList);
        }
    }).catch((error) => {
      console.log(error);
    })
  },[count])

  const filteredQuestions = applySortFilter(questionList, getComparator(order, orderBy), filterName);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const handleDeleteProduct = (id) => {
    Axios.post('http://localhost:3002/dashboard/hornous_table/delete_question', {
        user_id:1,
        dtype: 'json',
        questionID: id,
    }).then((response) => {
        if(response.data.status == 200){
          enqueueSnackbar('Delete Successful', { variant: 'success' });
          window.location.reload();
        }
    }).catch((error) => {
      enqueueSnackbar('Delete Error', { variant: 'error' });
    })
  };



  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - filteredQuestions.length) : 0;

  const isNotFound = filteredQuestions.length === 0;

  return (
    <Page title="Hornous Related Questions Table">
      <Container>
        <HeaderBreadcrumbs
          heading="Hornous Related Questions Table"
          links={[
            { name: 'Dashboard', href: "/dashboard/nlu_management" },
            { name: 'Hornous Table', href: "/dashboard/hornous_table" }
          ]}
          action={
            <Button
              variant="contained"
              component={RouterLink}
              to="/dashboard/hornous_table/new"
              startIcon={<Icon icon={plusFill} />}
            >
              New Item
            </Button>
          }
        />

        <Card>
          <UserListToolbar
            numSelected={selected.length}
            filterName={filterName}
            onFilterName={handleFilterByName}
          />

          <Scrollbar>
            <TableContainer sx={{ minWidth: 800 }}>
              <Table>
              <NLUTableHead
                  order={order}
                  orderBy={orderBy}
                  headLabel={TABLE_HEAD}
                  rowCount={filteredQuestions.length}
                  onRequestSort={handleRequestSort}
                />
                <TableBody>
                  {filteredQuestions
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      const { id, type, question, answer } = row;
                      const isItemSelected = selected.indexOf(id) !== -1;

                      return (
                        <TableRow
                          hover
                          key={id}
                          tabIndex={-1}
                          role="checkbox"
                          selected={isItemSelected}
                          aria-checked={isItemSelected}
                        >
                          
                          <TableCell component="th" scope="row" padding="left">
                            <Stack direction="row" alignItems="center" spacing={2}>
                              <Typography variant="subtitle2" noWrap>
                                {index + 1}
                              </Typography>
                            </Stack>
                          </TableCell>
                          <TableCell align="left">{question}</TableCell>
                          <TableCell align="left">{answer}</TableCell>

                          <TableCell align="right">
                            <ProductMoreMenu onDelete={() => handleDeleteProduct(id)} tableName='hornous_table' productName={`question${id}`} />
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 53 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
                {isNotFound && (
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow>
                  </TableBody>
                )}
              </Table>
            </TableContainer>
          </Scrollbar>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={filteredQuestions.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Card>
      </Container>
    </Page>
  );
}
