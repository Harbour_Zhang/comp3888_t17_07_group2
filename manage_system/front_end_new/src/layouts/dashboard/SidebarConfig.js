import { Icon } from '@iconify/react';
import peopleFill from '@iconify/icons-eva/people-fill';
import fileTextFill from '@iconify/icons-eva/file-text-fill';
import dashBoardIcon from '@iconify/icons-eva/browser-fill'

// ----------------------------------------------------------------------

const getIcon = (name) => <Icon icon={name} width={22} height={22} />;

const sidebarConfig = [
  {
    title: 'dashboard',
    path: '/dashboard/app',
    icon: getIcon(dashBoardIcon)
  },
  {
    title: 'Time Manager',
    path: '/dashboard/teachermanage',
    icon: getIcon(fileTextFill)
  },
  {
    title: 'Meetings',
    path: '/dashboard/meetings',
    icon: getIcon(peopleFill)
  },
  {
    title: 'NLU Manage',
    path: '/dashboard/nlu_management',
    icon: getIcon(fileTextFill)
  }
];

export default sidebarConfig;
