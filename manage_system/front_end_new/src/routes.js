import { Navigate, useRoutes } from 'react-router-dom';
// layouts
import DashboardLayout from './layouts/dashboard';
import LogoOnlyLayout from './layouts/LogoOnlyLayout';
//
import Login from './pages/Login';
import Register from './pages/Register';
import DashboardApp from './pages/DashboardApp';

import NotFound from './pages/Page404';
import TeacherInfoDashboard from './components/teacherInfo/TeacherInfoDashboard';
import TeacherCreate from './components/teacherInfo/create/TeacherCreate';
// nlu managements
import NLUdashboard from './pages/NLUdashboard';
import UnitTable from './pages/nlu management system/UnitTable';
import AcademicTable from './pages/nlu management system/AcademicTable';
import FaqTable from './pages/nlu management system/FaqTable';
import DegreeTable from './pages/nlu management system/DegreeTable';
import HornousTable from './pages/nlu management system/HornousTable';
import UnitCreate from './pages/nlu management system/create/UnitCreate';
import QuestionCreate from './pages/nlu management system/create/QuestionCreate';
import StaffCreate from './pages/nlu management system/create/StaffCreate';
import PastMeetings from './pages/meetings_manage_system/PastMeetings';
import MeetingsDashBoard from './pages/meetings_manage_system/MeetingsDashBoard';
import AllMeetings from './pages/meetings_manage_system/AllMeetings';
import InScheduleMeetings from './pages/meetings_manage_system/InScheduleMeetings';
import AccountPage from './components/profile/AccountPage';
import UnitCoordinatorCreate from './pages/nlu management system/create/UnitCoordinatorCreate';
import UnitCoordinatorTable from './pages/nlu management system/UnitCoordinatorTable';
// ----------------------------------------------------------------------

export default function Router() {
  return useRoutes([
    {
      path: '/dashboard',
      element: <DashboardLayout />,
      children: [
        { element: <Navigate to="/dashboard/app" replace /> },
        { path: 'app', element: <DashboardApp /> },
        { path: 'meetings', element: <MeetingsDashBoard /> },
        { path: 'teachermanage', element: <TeacherInfoDashboard /> },
        { path: 'teachermanage/:name/edit', element: <TeacherCreate /> },
        { path: 'teachermanage/new', element: <TeacherCreate /> },
        { path: 'nlu_management', element: <NLUdashboard /> },
        { path: 'unit_table', element: <UnitTable /> },
        { path: 'unit_table/:name/edit', element: <UnitCreate />},
        { path: 'unit_table/new', element: <UnitCreate /> },
        { path: 'hornous_table', element: <HornousTable /> },
        { path: 'hornous_table/:name/edit', element: <QuestionCreate />},
        { path: 'hornous_table/new', element: <QuestionCreate /> },
        { path: 'faq_table', element: <FaqTable /> },
        { path: 'faq_table/:name/edit', element: <QuestionCreate />},
        { path: 'faq_table/new', element: <QuestionCreate /> },
        { path: 'degree_table', element: <DegreeTable /> },
        { path: 'degree_table/:name/edit', element: <QuestionCreate />},
        { path: 'degree_table/new', element: <QuestionCreate /> },
        { path: 'academic_table', element: <AcademicTable /> },
        { path: 'academic_table/:name/edit', element: <StaffCreate />},
        { path: 'academic_table/new', element: <StaffCreate /> },
        { path: 'InScheduleMeetings', element: <InScheduleMeetings />},
        { path: 'PastMeetings', element: <PastMeetings />},
        { path: 'unit_coordinator_table', element: <UnitCoordinatorTable />},
        { path: 'unit_coordinator_table/:name/edit', element: <UnitCoordinatorCreate />},
        { path: 'AllMeetings', element: <AllMeetings />},
        { path: 'account', element: <AccountPage />}
      ]
    },
    {
      path: '/',
      element: <LogoOnlyLayout />,
      children: [
        { path: 'login', element: <Login /> },
        { path: 'register', element: <Register /> },
        { path: '404', element: <NotFound /> },
        { path: '/', element: <Navigate to="/dashboard" /> },
        { path: '*', element: <Navigate to="/404" /> }
      ]
    },
    { path: '*', element: <Navigate to="/404" replace /> }
  ]);
}
