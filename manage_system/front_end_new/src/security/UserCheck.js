import Axios from "axios";

export default function UserCheck(){
  return Axios.get('http://localhost:3002/login').then((response) => {
    if(response.data.loggedIn){
      return Axios.get('http://localhost:3002/userVerify',
        {
          headers: {
            'x-access-token':localStorage.getItem("token")
          }}).then((res) => {
            return res.data.auth;
      })
      }
    return false
  })
}
