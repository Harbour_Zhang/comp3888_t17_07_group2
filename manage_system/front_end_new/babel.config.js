module.exports = (api) => {
  const presets = ["react-app"];
  const plugins = [
    "@babel/plugin-transform-modules-commonjs",
    "inline-react-svg",
  ];

  api.cache(false);

  return {
    presets,
    plugins,
  };
};
/* snapshot test
module.exports = {
  presets: [
    ["@babel/preset-env", { targets: { node: "current" } }],
    ["@babel/preset-react", { runtime: "automatic" }],
  ],
};*/
