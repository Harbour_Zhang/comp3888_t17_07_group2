import AccountPage from "../../src/components/profile/AccountPage";
import {
  AccountGeneral,
  AccountChangePassword,
} from "../../src/components/_dashboard/user/account";
import AllMeetings from "../../src/pages/meetings_manage_system/AllMeetings";
import renderer from "react-test-renderer";
import React from "react";
import UnitCreate from "../../src/pages/nlu management system/create/UnitCreate";
/*
it("render all meetings ", () => {
  //const component = renderer.create(<AccountPage></AccountPage>).toJSON;
  const component = renderer.create(<AllMeetings></AllMeetings>).toJSON;
  //expect(component).toMatchSnapshot();
});


*/
it("render account general component in account page ", () => {
  const component = renderer.create(<AccountGeneral></AccountGeneral>).toJSON;
  expect(component).toMatchSnapshot();
});

it("render UnitCreate ", () => {
  //const component = renderer.create(<AccountPage></AccountPage>).toJSON;
  const component = renderer.create(<UnitCreate></UnitCreate>).toJSON;
  //expect(component).toMatchSnapshot();
});
/*
it("render AccountChangePassword component in account page ", () => {
  const component = renderer.create(
    <AccountChangePassword></AccountChangePassword>
  ).toJSON;
  expect(component).toMatchSnapshot();
});*/
