import sqlite3
from sqlite3 import Error


# def create_connection(db_file):
#     """ create a database connection to the SQLite database
#         specified by db_file
#     :param db_file: database file
#     :return: Connection object or None
#     """
#     conn = None
#     try:
#         conn = sqlite3.connect(db_file)
#         return conn
#     except Error as e:
#         print(e)
#
#     return conn
#
#
# def create_table(conn, create_table_sql):
#     """ create a table from the create_table_sql statement
#     :param conn: Connection object
#     :param create_table_sql: a CREATE TABLE statement
#     :return:
#     """
#     try:
#         c = conn.cursor()
#         c.execute(create_table_sql)
#     except Error as e:
#         print(e)
#
# def create_unit(conn, unit):
#     """
#     Create a new unit into the projects table
#     :param conn:
#     :param unit:
#     :return: unit id
#     """
#
#     sql = ''' INSERT INTO units(code,name,prohibitions)
#               VALUES(?,?,?) '''
#     cur = conn.cursor()
#     cur.execute(sql, unit)
#     conn.commit()
#     return cur.lastrowid


def unit_prohibition(unit_code):
    conn = None
    db_file = r"actions/unit.db"
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT prohibition FROM units WHERE Unit_Id=?", (unit_code, ))
        row = cur.fetchone()
        if row is None:
            return "There are no prohibitions"
        else:
            return row[0]
    except Error as e:
        print(e)

def unit_exists(unit_code):
    conn = None
    db_file = r"actions/unit.db"
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT * FROM units WHERE Unit_Id=?", (unit_code, ))
        row = cur.fetchone()
        if row is None:
            return False
        else:
            return True
    except Error as e:
        print(e)

def unit_credit_points(unit_code):
    conn = None
    db_file = r"actions/unit.db"
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT Credit FROM units WHERE Unit_Id=?", (unit_code, ))
        row = cur.fetchone()
        return row[0]
    except Error as e:
        print(e)

def unit_coordinator(unit_code):
    conn = None
    db_file = r"actions/unit.db"
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT Staff FROM Coordinator WHERE Unit_Id=?", (unit_code, ))
        row = cur.fetchone()
        if row is None:
            return False
        else:
            return row
    except Error as e:
        print(e)

def is_core(unit_code):
    conn = None
    db_file = r"actions/unit.db"
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT Core FROM units WHERE Unit_Id=?", (unit_code, ))
        row = cur.fetchone()
        return row[0]
    except Error as e:
        print(e)

def unit_prerequisite(unit_code):
    conn = None
    db_file = r"actions/unit.db"
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT prerequisites FROM units WHERE Unit_Id=?", (unit_code, ))
        row = cur.fetchone()
        if row is None:
            return "There are no prerequisite"
        else:
            return row[0]
    except Error as e:
        print(e)

def major_units(unit_code):
    conn = None
    db_file = r"actions/unit.db"
    try:
        conn = sqlite3.connect(db_file)
        cur = conn.cursor()
        cur.execute("SELECT major FROM Major WHERE Unit_Id=?", (unit_code, ))
        row = cur.fetchall()
        if row is None:
            return "This is not a major unit"
        else:
            return row
    except Error as e:
        print(e)
