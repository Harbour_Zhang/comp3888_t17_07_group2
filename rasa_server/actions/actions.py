# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

from actions import database

class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Hello World!")

        return []

class ActionCallBookingForm(Action):
    def name(self) -> Text:
        return "action_call_booking_form"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        msg = {
            "type":"CUSTOM_COMPONENT"
        }

        dispatcher.utter_message(
            text = "Please Fill the form",
            attachment=msg
        )

        return []

class GetUnitLink(Action):

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_send_unit_link"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']
        # print(entity_unitCode)
        unit_exists = database.unit_exists(entity_unitCode)
        # print(unit_exists)
        if unit_exists:
            dispatcher.utter_message(
                text="https://www.sydney.edu.au/units/" + entity_unitCode
            )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class GetUnitProhibition(Action):

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_prohibition"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            prohibitions = database.unit_prohibition(entity_unitCode)
            # print(prohibitions)
            dispatcher.utter_message(
                text=prohibitions
            )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class GetUnitCoordinator(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_ask_unit_coordinator"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            coordinators = database.unit_coordinator(entity_unitCode)
            print(coordinators)

            if coordinators!= False:
                dispatcher.utter_message(
                    text="The coordinator is:  " + coordinators[0]
                )
            else:
                dispatcher.utter_message(
                    text="No coordinators recoreded for this unit"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []
class GetUnitCreditPoints(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_get_unit_credit_points"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            credit_points = database.unit_credit_points(entity_unitCode)
            # print(prohibitions)
            dispatcher.utter_message(
                text="The credit point for the unit is " + str(credit_points)
            )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class CheckCoreUnits(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_check_core_units"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            is_core = database.is_core(entity_unitCode)
            # print(prohibitions)
            if is_core:
                dispatcher.utter_message(
                    text="This is a core unit"
                )
            else:
                dispatcher.utter_message(
                    text="This is an elective unit"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class CheckElectiveUnits(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_check_elective_units"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            is_core = database.is_core(entity_unitCode)
            # print(prohibitions)
            if is_core:
                dispatcher.utter_message(
                    text="This is a core unit"
                )
            else:
                dispatcher.utter_message(
                    text="This is an elective unit"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class CheckElectiveOrCoreUnits(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_check_elective_or_core_units"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            is_core = database.is_core(entity_unitCode)
            # print(prohibitions)
            if is_core:
                dispatcher.utter_message(
                    text="This is a core unit"
                )
            else:
                dispatcher.utter_message(
                    text="This is an elective unit"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class GetPrerequisite(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_prerequisite"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            prerequisite = database.unit_prerequisite(entity_unitCode)
            # print(prohibitions)
            dispatcher.utter_message(
                text=prerequisite
            )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class CheckCsMajorUnit(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_ask_cs_major_units"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            majors = database.major_units(entity_unitCode)
            is_cs = False
            # print(prohibitions)
            for major in majors:
                if major[0] == "CS":
                    is_cs = True
            if is_cs:
                dispatcher.utter_message(
                    text="It is a Computer Science major course"
                )
            else:
                dispatcher.utter_message(
                    text="It is not a Computer Science major course"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class CheckSeMajorUnit(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_ask_se_major_units"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            majors = database.major_units(entity_unitCode)
            is_se = False
            # print(prohibitions)
            for major in majors:
                if major[0] == "SE":
                    is_se = True
            if is_se:
                dispatcher.utter_message(
                    text="It is a Software Engineering major course"
                )
            else:
                dispatcher.utter_message(
                    text="It is not a Software Engineering major course"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class CheckDsMajorUnit(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_ask_ds_major_units"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            majors = database.major_units(entity_unitCode)
            is_ds = False
            # print(prohibitions)
            for major in majors:
                if major[0] == "CDS":
                    is_ds = True
            if is_ds:
                dispatcher.utter_message(
                    text="It is a Data Science major course"
                )
            else:
                dispatcher.utter_message(
                    text="It is not a Data Science major course"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []

class CheckIsMajorUnit(Action):   # Need to be done

    def name(self) -> Text:
        # Name of the action mentioned in the domain.yml file
        return "action_ask_is_major_units"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # It will return array of entities
        entities = tracker.latest_message['entities']
        entity_unitCode = ""
        # Iterating through the array to retrieve the desired entity
        for e in entities:
            if e['entity'] == "lookup_unitCode":
                entity_unitCode = e['value']

        unit_exists = database.unit_exists(entity_unitCode)
        # print(entity_unitCode)
        if unit_exists:
            majors = database.major_units(entity_unitCode)
            is_is = False
            # print(prohibitions)
            for major in majors:
                if major[0] == "IS":
                    is_is = True
            if is_is:
                dispatcher.utter_message(
                    text="It is a Information System major course"
                )
            else:
                dispatcher.utter_message(
                    text="It is not a Information System major course"
                )
        else:
            dispatcher.utter_message(
                text="Sorry, we can't find unit information for " + entity_unitCode
            )

        return []
