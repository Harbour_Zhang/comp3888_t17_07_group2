import requests

sender = input("Input name:\n")
bot_message=""

while bot_message != "Bye":
    message = input("Input message:\n")

    r = requests.post('http://localhost:5050/webhooks/rest/webhook', json={"sender": sender, "message": message})
    print(r.json())
    print("Bot says:\n")
    for i in r.json():
        bot_message = i['text']
        print(bot_message)
        print("\n")
