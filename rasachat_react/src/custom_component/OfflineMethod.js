import React, {useEffect, useState} from 'react';
import "./MeetingMethod.css"
import Axios from 'axios';

const OfflineMethod = (props) => {
    const [message, setMessage] = useState(
      <div className={"OfflineContainer"}>
        Loading...
    </div>);

    useEffect(() => {
      Axios.post('http://localhost:3002/teacher/getATeacherAllInfo', {
        teacherId:props.teacherId,
      }).then((res) => {
        setMessage(
          <div className={"LabelForChooseMethod"}>
            <p>Please record the teacher's office place and arrive on time.</p>
            <p>{res.data.staff.office}</p>
          </div>
        );
        props.setPlace(res.data.staff.office);
      });
    },[])
    return(
        <div>
            <div className={"LabelForChooseMethod"}>Offline Information:</div>
            <div>{message}</div>
        </div>
    )
}

export default OfflineMethod;
