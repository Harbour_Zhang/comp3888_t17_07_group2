import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import { Grid, InputLabel } from '@mui/material';

const TeacherChoose = (props) => {
  const [teacherId,setTeacherId] = useState('');
  const [teacherList,setTeacherList] = useState([]);
  const [teacherListComponent, setTeacherListComponent] = useState(<div></div>)
  useEffect(() => {
    Axios.get('http://localhost:3002/teacher/getTeacherList').then((res) => {
      if(res.data.status === 200){
        setTeacherList(res.data.infoRow);
        setTeacherListComponent(
          <div style={{'width':'50%', 'marginLeft':'auto','marginRight':'auto'}}>
            <select
              id="degree_select"
              value={(props.teacherId === '') ? 0 : props.teacherId}
              onChange={(e) => {
                props.setTeacherId(e.target.value);
                setTeacherId(e.target.value);
                props.setTeacherName(document.getElementById(e.target.value).innerText);
              }}
            >
              {res.data.infoRow.map((row)=> (
                <option id={row.id} value={row.id}>{row.name}</option>
              ))}
            </select>
          </div>
            )
      }else{
        alert("Connect to database fail, please try again later.");
      }
    })
  },[teacherId,props.teacherId]);

  return(
    <div>
      <InputLabel id="Teacher_Select" style={{ 'paddingLeft': '18%',paddingTop:'5%'}}>Staff List</InputLabel>
      {teacherListComponent}
      <Grid
        container
        direction="column"
        justifyContent="space-around"
        alignItems="center"
        style={{paddingTop:'5%'}}
      >
        <Grid>
          The teacher you choose is:
        </Grid>
        <Grid>
          {props.teacherName}
        </Grid>
      </Grid>
    </div>
  )
}

export default TeacherChoose;
