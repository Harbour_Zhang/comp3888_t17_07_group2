import React, {useEffect, useState} from 'react';
import Axios from 'axios';

const Finish = (props) => {
    useEffect(() => {
        Axios.post('http://localhost:3003/sendMail', {
            name: props.name,
            email: props.email,
            degree: props.degree,
            year: props.year,
            staffName: props.staffName,
            date:props.date,
            meetingMethod: props.method,
            meetingLink: props.place,
            comment: props.comment
        }).then((response) => {
            console.log(response);
        })

        Axios.post('http://localhost:3002/dashboard/meetingmanagement/addMeeting', {
            studentName: props.name,
            studentEmail: props.email,
            degree: props.degree,
            year: props.year,
            teacherId: props.teacherId,
            date: props.date,
            method: props.method,
            place: props.place,
            comment: props.comment,
            status: 'future',
        }).then((res) => {
            console.log(res);
        })
    },[])


    return(
        <div>
            <div className={"LabelForChooseMethod"}>Success Booked !</div>
            <hr />
            <div style={{"height":"300px"}}>
                <div className={"LabelForChooseMethod"}>An Email will send to both side, pls check it for details.</div>
                <div className={"LabelForChooseMethod"}>Please attend the meeting on time.</div>
            </div>
        </div>
    )
}

export default Finish;
