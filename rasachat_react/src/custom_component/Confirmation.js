import React, {useEffect, useState} from 'react';
import "./Comment.css";
import "./MeetingMethod.css";
import "./Confirmation.css";

const Confirmation = (props) => {
    return(
        <div>
            <div className={"LabelForChooseMethod"}>Your booking info summary:</div>
            <hr />
            <div style={{"overflow-y":"scroll", "height":"300px"}}>
                <div className={"InfoSummary"}>
                    <div>Student Name: {props.name}</div>
                    <hr/>
                    <div>EMail: {props.email}</div>
                    <hr/>
                    <div>Degree: {props.degree}</div>
                    <hr/>
                    <div>Year: {props.year}</div>
                    <hr/>
                    <div>Staff Name: {props.staffName}</div>
                    <hr/>
                    <div>Time: {props.date.toString()}</div>
                    <hr/>
                    <div>Meeting method: {props.method}</div>
                    <hr/>
                    <div>Meeting Link or prefer place: {props.place}</div>
                    <hr/>
                    <div>Leave Comments: {props.comment}</div>
                </div>
            </div>
        </div>
    )
}

export default Confirmation;
