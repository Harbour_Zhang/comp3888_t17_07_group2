import React from "react";
import { Grid, InputLabel, TextField } from '@mui/material';

const StudentInformation = (props) => {
  return(
    <div>
      <Grid
        container
        direction="column"
        justifyContent="space-around"
        alignItems="center"
      >
        <Grid>
          <TextField id="standard-basic" value={props.studentName} margin="normal" label="Student Name" variant="standard" onChange={(e)=>{
            props.setStudentName(e.target.value);
          }}/>
        </Grid>
        <Grid>
          <TextField id="standard-basic" value={props.studentEmail} margin="normal" label="Student Email" variant="standard" onChange={(e)=>{
            props.setStudentEmail(e.target.value);
          }}/>
        </Grid>
      </Grid>

      <InputLabel id="degree_text" style={{ 'paddingLeft': '18%',paddingTop:'5%'}}>Degree</InputLabel>
      <div style={{'width':'50%', 'marginLeft':'auto','marginRight':'auto'}}>
        <select
          id="degree_select"
          value={props.degree}
          onChange={(e) => {
            props.setDegree(e.target.value);
          }}
        >
          <option value={'CS'}>Computer Science</option>
          <option value={'SD'}>Software Development</option>
          <option value={'IS'}>Information System</option>
          <option value={'DS'}>Data Science</option>
        </select>
      </div>
      <InputLabel id="year_text" style={{ 'paddingLeft': '18%', paddingTop:'5%'}}>Year</InputLabel>
      <Grid>
        <div style={{'width':'50%', 'marginLeft':'auto','marginRight':'auto'}}>
          <select
            id="year_select"
            value={props.year}
            onChange={(e)=>{
              props.setYear(e.target.value);
            }}
          >
            <option value={1}>Year 1</option>
            <option value={2}>Year 2</option>
            <option value={3}>Year 3</option>
            <option value={4}>Year 4</option>
            <option value={5}>Year 5</option>
            <option value={6}>Other</option>
          </select>
        </div>
      </Grid>
    </div>
  )
}

export default StudentInformation;
