import React, {useEffect, useState} from 'react';
import "./Comment.css";
import "./MeetingMethod.css";

const Comment = (props) => {
    return(
        <div>
            <div className={"LabelForChooseMethod"}>
                Comment
            </div>
            <hr />
            <div style={{"height":"300px"}}>
                <div className={"LabelForChooseMethod"}>Want leave a message?</div>
                <textarea className={"LinkInput"} value={props.comment} onChange={(e) => {
                    props.setComment(e.target.value);
                }}/>
            </div>
        </div>
    )
}

export default Comment;
