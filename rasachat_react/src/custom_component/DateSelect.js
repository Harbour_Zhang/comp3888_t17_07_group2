import React, {useEffect, useState} from 'react';
import Axios from 'axios';
import { LoadingButton, LocalizationProvider, StaticDatePicker } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import { TextField } from '@mui/material';

const DateSelect = (props) => {

  const [allDates,setAllDates] = useState([]);
  const [allowRender,setAllowRender] = useState(0);

  function disableNotAvailable(date){
    for(const eachDate of allDates){
      if(eachDate.getTime() === date.getTime()){
        return false;
      }
    }
    return true;
  }

  useEffect(()=>{
    Axios.post('http://localhost:3002/teacher/getTeacherTime', {
      id:props.teacherId,
    }).then((res) => {
      let allDate = [];
      res.data.infoRow.map((row)=>{
        var newDate = new Date(row);
        newDate.setHours(0,0,0,0);
        allDate.push(newDate);
      })
      setAllDates(allDate);
      setAllowRender(1);
      console.log(allDate);
    });
  },[])

  const maxDate = new Date();
  maxDate.setFullYear(maxDate.getFullYear() + 1);

  return(
    <div>
      <div className={"LabelForChooseMethod"}>Select the date you want:</div>
      {(allowRender === 1) ? <LocalizationProvider dateAdapter={AdapterDateFns}>
        <StaticDatePicker
          displayStaticWrapperAs="desktop"
          value={props.date}
          shouldDisableDate={disableNotAvailable}
          minDate={new Date()}
          maxDate={maxDate}
          onChange={(newValue) => {
            // setValue(newValue);
            props.setDate(newValue);
            var month = newValue.getMonth() + 1;
            var year = newValue.getUTCFullYear();
            var day = newValue.getDate();
            props.setNoti(year + '-' + month + '-' + day);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider> : <div><LoadingButton loading variant="outlined">
      </LoadingButton></div>}
      <div className={"LabelForChooseMethod"} style={{'marginTop':'-15%'}}>
        The time you choose is:
        <div>
          {props.noti}
        </div>
      </div>
    </div>
  )
}

export default DateSelect;
