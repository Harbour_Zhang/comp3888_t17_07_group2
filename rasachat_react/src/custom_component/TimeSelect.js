import React, {useEffect, useState} from 'react';
import Axios from 'axios';
import { Button, Divider, Grid, Stack } from '@mui/material';

const TimeSelect = (props) => {

  const [availableTime, setAvailableTime] = useState([]);
  const [allowRender,setAllowRender] = useState(0);

  function filterDate(date){
    return date.getYear() === props.date.getYear() && date.getMonth() === props.date.getMonth() && date.getDate() === props.date.getDate();
  }
  useEffect(()=>{
    Axios.post('http://localhost:3002/teacher/getTeacherTime', {
      id:props.teacherId,
    }).then((res) => {
      console.log(res);
      let allTime = [];
      res.data.infoRow.map((row)=>{
        var newDate = new Date(row);
        if(filterDate(newDate)){
          allTime.push(newDate);
        }
      })
      setAvailableTime(allTime);
      setAllowRender(1);
      console.log(allTime);
    });
  },[])

  function handleChange(event){
    props.setSelectDate(event.target.value);
    props.setTimeNoti(event.target.value.toString());
  }

  return (
    <div>
      <div className={"LabelForChooseMethod"}>Select the time you want:</div>
      <br />
      <Stack spacing={2} direction="column" divider={<Divider orientation="vertical" flexItem />}>
        {(allowRender === 1) ? <div>
          {availableTime.map((row) => (
            <div>
              <Button variant="outlined" value={row} onClick={handleChange}>
                {row.toString()}
              </Button>
              <br/>
            </div>
            )
          )}
        </div> : <div>Loading...</div>}
      </Stack>
      <br />
      <div className={"LabelForChooseMethod"}>The time you choose is:</div>
      <div className={"LabelForChooseMethod"}>{props.timeNoti}</div>
    </div>
  )

}

export default TimeSelect;
