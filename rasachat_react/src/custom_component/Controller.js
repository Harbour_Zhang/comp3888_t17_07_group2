import React, { Component, useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { Grid } from '@mui/material';
import StudentInformation from './StudentInformation';
import TeacherChoose from './TeacherChoose';
import './Controller.css';
import DateSelect from './DateSelect';
import TimeSelect from './TimeSelect';
import MeetingMethod from './MeetingMethod';
import Comment from './Comment';
import Confirmation from './Confirmation';
import Finish from './Finish';

const Controller = () => {
  const [step,setStep] = useState(0);
  const [studentName, setStudentName] = useState('');
  const [studentEmail, setStudentEmail] = useState('');
  const [degree, setDegree] = useState('CS');
  const [year, setYear] = useState(1);
  const status = 'future';
  const [teacherId, setTeacherId] = useState('1');
  const [teacherName, setTeacherName] = useState('');
  const [date,setDate] = useState(new Date());
  const [comment,setComment] = useState('');
  const [method,setMethod] = useState('Online - Zoom');
  const [place, setPlace] = useState('');
  const [header,setHeader] = useState('Please fill your detail');
  const [currentForm, setCurrentForm] = useState(<StudentInformation studentName={studentName} setStudentName={setStudentName} studentEmail={studentEmail} setStudentEmail={setStudentEmail} degree={degree} setDegree={setDegree} year={year} setYear={setYear}/>);
  const [time,setTime] = useState('');
  const [selectDate, setSelectedDate] = useState(new Date());
  const [dateNoti,setDateNoti] = useState('Please select the date.');
  const [timeNoti,setTimeNoti] = useState('Please select the time.');

  useEffect(() => {
    if(step === 0){
      setHeader("Please fill your detail");
      setCurrentForm(<StudentInformation studentName={studentName} setStudentName={setStudentName} studentEmail={studentEmail} setStudentEmail={setStudentEmail} degree={degree} setDegree={setDegree} year={year} setYear={setYear}/>)
    }else if(step === 1){
      setHeader("Please choose the Staff");
      setCurrentForm(<TeacherChoose teacherId={teacherId} setTeacherId={setTeacherId} teacherName={teacherName} setTeacherName={setTeacherName}/>)
    }else if(step === 2){
      setHeader("Please choose the Date");
      setCurrentForm(<DateSelect date={date} setDate={setDate} teacherId={teacherId} noti={dateNoti} setNoti={setDateNoti}/>)
    }else if(step === 3){
      setHeader("Please choose the Time");
      setCurrentForm(<TimeSelect teacherId={teacherId} time={time} setTime={setTime} date={date} selectDate={selectDate} setSelectDate={setSelectedDate} timeNoti={timeNoti} setTimeNoti={setTimeNoti}/>)
    }else if(step === 4){
      setHeader("Please select the meeting method.")
      setCurrentForm(<MeetingMethod teacherId={teacherId} method={method} setMethod={setMethod} place={place} setPlace={setPlace}/>)
    }else if(step === 5){
      setHeader('Please leave your questions.');
      setCurrentForm(<Comment comment={comment} setComment={setComment}/>);
    }else if(step === 6){
      setHeader('Please confirm the details');
      setCurrentForm(<Confirmation name={studentName} email={studentEmail} degree={degree} year={year} staffName={teacherName} date={selectDate} method={method} place={place} comment={comment}/>);
    }else if(step === 7){
      setHeader('Booking Success');
      setCurrentForm(<Finish name={studentName} email={studentEmail} degree={degree} year={year} staffName={teacherName} date={selectDate} method={method} place={place} comment={comment} teacherId={teacherId}/>)
    }
  },[step,degree,year, studentName,studentEmail,teacherName,teacherId, dateNoti, timeNoti, place, method,comment])

  function next(){
    if(step === 0){
      if(studentName === '' || studentEmail === ''){
        alert("Please fill all information.");
      }else{
        setStep(step + 1);
      }
    }else if(step === 1){
      if(teacherId === '' || teacherName === ''){
        alert("Please choose a teacher to continue.");
      }else{
        setStep(step + 1);
      }
    }else if(step === 2){
      if(dateNoti === 'Please select the date.'){
        alert('Please choose the date to continue.')
      }else{
        setStep(step + 1);
      }
    }else if(step === 3){
      if(timeNoti === 'Please select the time.'){
        alert('Please select the time to continue.');
      }else{
        setStep(step + 1);
      }
    }else if(step === 4){
      if(place === ''){
        alert("Please choose the meeting method.");
      }else{
        setStep(step + 1);
      }
    }else{
      setStep(step + 1);
    }
  }

  function previous(){
    if(step > 0){
      setStep(step-1);
    }
  }
  return(
    <div>
      <div className={"LabelForChooseMethod"}>
        {header}
      </div>
      <hr />
      <div className={"content_body"}>
        {currentForm}
      </div>
      <hr />
      <div>
        <Grid container spacing={2} justifyContent="space-around"
              alignItems="center">
          <Grid item>
            {(step !== 0) ? <Button variant="contained" color="success" onClick={previous}>{'<'}</Button> : <Button variant="contained" color="success" disabled>{'<'}</Button> }
          </Grid>
          <Grid item xs={6}> </Grid>
          <Grid item>
            {(step !== 7) ? <Button variant="contained" color="success" onClick={next}>{'>'}</Button> : <Button variant="contained" color="success" disabled>{'>'}</Button> }
          </Grid>
        </Grid>
      </div>
    </div>
  )
}
export default Controller;
