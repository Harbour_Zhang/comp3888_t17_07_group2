import React from 'react';
import Controller from "./Controller";
const Form = () => {
    return(
        <div className={"Form-container"}>
            <Controller />
        </div>
    )
}

export default Form;
