import React, {useEffect, useState} from 'react';
import "./MeetingMethod.css"
import OnlineMethod from "./OnlineMethod";
import OfflineMethod from "./OfflineMethod";

const MeetingMethod = (props) => {
    const [option,setOption] = useState(<OnlineMethod setMethod={props.setMethod} place={props.place} setPlace={props.setPlace}/>);

    function setReturnMessage(){
        const select = document.getElementById('meetingMethod');
        const value = select.options[select.selectedIndex].value;
        if(value === 'online'){
            props.setMethod('Online - Zoom');
            setOption(<OnlineMethod setMethod={props.setMethod} place={props.place} setPlace={props.setPlace}/>);
        }else{
            props.setMethod('Offline');
            setOption(<OfflineMethod teacherId={props.teacherId} setMethod={props.setMethod} place={props.place} setPlace={props.setPlace}/>);
        }
    }

    return(
        <div>
            <div className={"LabelForChooseMethod"}>Meeting method</div>
            <hr />
            <div style={{"overflow-y":"scroll", "height":"300px"}}>
                <div className={"LabelForChooseMethod"}>Please select the way you want:</div>
                <select name="methods" id="meetingMethod" className={"SelectForChooseMethod"} onChange={setReturnMessage}>
                    <option className={"MethodContent"} value="online">Online</option>
                    <option className={"MethodContent"} value="offline">Offline</option>
                </select>
                <hr className={"Line"} />
                <div>{option}</div>
            </div>
        </div>
    )
}

export default MeetingMethod;
