const express = require('express');
const app = express();
const port = 3003;
const cors = require("cors");
const {response} = require("express");
const sgMail = require("@sendgrid/mail");

app.use(express.json());

app.use(cors({
    origin: ["http://localhost:3001"],
    methods: ["GET", "POST"],
    credentials: false
}));

app.post('/sendMail', (require, response) => {
    const studentName = require.body.name;
    const email = require.body.email;
    const degree = require.body.degree;
    const year = require.body.year;
    const staffName = require.body.staffName;
    const time = require.body.date;
    const meetingMethod = require.body.meetingMethod;
    const meetingLink = require.body.meetingLink;
    const comment = require.body.comment;

    const message = 'Your Summary\n\n' + 'Student Name: ' + studentName +
        '\nEmail: ' + email + '\nDegree: ' + degree +
        '\nYear: ' + year + '\nStaff Name: ' + staffName + '\nTime: ' + time +
        '\n Meeting Method: ' + meetingMethod + '\n Meeting Place: ' +
        meetingLink + '\n Comment: ' + comment;

    // Change to your apikey
    sgMail.setApiKey('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');
    const msg = {
        to: email, // Change to your recipient
        from: 'usydsmartchatbot@gmail.com', // Change to your verified sender
        subject: 'You booking information summary',
        text: message,
    }
    sgMail
        .send(msg)
        .then(() => {
            console.log('Email sent')
        })
        .catch((error) => {
            console.error(error)
        })
})



app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})
