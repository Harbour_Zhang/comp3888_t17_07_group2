import React, {useEffect, useState} from 'react';
import "./MeetingMethod.css"
import { Grid, TextField } from '@mui/material';

const OnlineMethod = (props) => {
    const [link, setLink] = useState(props.place);
    function setMethod(event){
        if(event.target.value === 'zoom'){
            props.setMethod('Online - Zoom');
        }else if(event.target.value==='voov'){
            props.setMethod('Online - VooV');
        }else{
            props.setMethod('Online - Duo');
        }
    }

    return(
        <div>
            <div className={"LabelForChooseMethod"}>Communication Tools:</div>
            <select name="methods" id="meetingMethod" className={"SelectForChooseMethod"} onChange={setMethod}>
                <option className={"MethodContent"} value="zoom">Zoom</option>
                <option className={"MethodContent"} value="voov">Voov</option>
                <option className={"MethodContent"} value="duo">Duo</option>
            </select>
            <div className={"LabelForChooseMethod"}>Meeting Link:</div>
          <Grid
            container
            direction="column"
            justifyContent="space-around"
            alignItems="center"
          >
            <Grid>
              <TextField id="standard-basic" value={link} margin="normal" label="Meeting Link" variant="standard" onChange={(e)=>{
                props.setPlace(e.target.value);
                setLink(e.target.value);
              }}/>
            </Grid>
          </Grid>
        </div>
    )
}

export default OnlineMethod;
