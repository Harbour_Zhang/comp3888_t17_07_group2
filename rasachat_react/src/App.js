import Widget from 'rasa-webchat';
import chatbotstyle from './chatbot.css';
import Form from "./custom_component/Form";

const ChatBox = () => {

  return (
      <Widget
          style={chatbotstyle}
          socketUrl={"http://localhost:5005"}
          socketPath={"/socket.io/"}
          customData={{"language": "en"}} // arbitrary custom data. Stay minimal as this will be added to the socket
          title={"CS UG Chat Bot"}
          customComponent={ (messageData) => (<Form />)}
      />
  )
}

function clear(){
  localStorage.clear();
}

function App() {
  clear();
  return (
      <div className="App">
        <ChatBox></ChatBox>
      </div>
  );
}

export default App;