# README #
USYD UG CS Chatbot

### Prerequisites ###

* Install node js with version 14.15.1 or version above

    [Download Link](https://nodejs.org/en/download/)

* python version 3.8.0+

* pip3 version 21.0.0+

* Install rasa with version 2.8.3+

    [Install link](https://rasa.com/docs/rasa/installation/)



### How to setup Rasa chatbot? ###
* Step 1 Rasa backend train model

    The trained models are under './rasa_server/models' folder.
    
    To train a new model, first go to raas_server folder bu
    
    `cd rasa_server`
    
    Then run command 
    
    `rasa train`


* Step 2 Rasa frontend

    go to the frontend of the management system

    `cd ./rasachat_react`

    Install all needed packages.

    `npm install`
    
    In same terminal, go to the backend of the Rasa chatbot
    
    `cd src/custom_component/backend`
    
    Install all needed packages.

    `npm install`

### How to run Rasa chatbot? ###
* Step 1 Rasa backend

    Open two terminals, both `cd rasa_server`.
    
    One run this command:

    `rasa run -m models --enable-api --cors "*" --debug`

    One run this command:

    `rasa run actions`

* Step 2 Rasa frontend

    1. Front end in one terminal
        
        Go to the frontend of the Rasa chatbot

        `cd ./rasachat_react`

        Run script

        `npm start`
        
    2. back end in another terminal
        
        Go to the backend of the Rasa chatbot

        `cd ./rasachat_react/src/custom_component/backend`

        Run script

        `node index.js`

### How to setup management system ? ###

* Step 1 frontend

    Go to the frontend of the management system

    `cd ./manage_system/front_end_new`

    install all required frontend packages by running the following command

    `npm install`

* Step 2 backend

    Go to the backcend of the management system
    
    `cd ./manage_system/backend`

    install all required backend packages by running the following command

    `npm install`

### How to run management system ? ###

* Step 1 frontend in one terminal

    Go to the frontend of the management system

    `cd ./manage_system/front_end_new`
    
    Run front end React app
    
    `npm start`

* Step 2 backend in another terminal

    Go to the backcend of the management system
    
    `cd ./manage_system/backend`
    
    Run backend express node js.
    
    `node index.js`
    
### How to run tests for backend(NLU and meeting) ? ###

* Step 1

    install mocha framework and chai library

    `npm install --global mocha`

* Step 2

    go to backend of NLU and meeting system

    `cd ./manage_system/backend`

* Step 3

    run script

    `npm test`
    
### How to run tests Rasa chatbot? ###

* Step 1

    Go to Rasa folder by command
    
    `cd rasa_server`
    
    Run tests.
    
    `rasa test`

### How to handle database.image error for any database ? ###

All the current working databases with some fake data were compressed into databases.zip file. If any of the database is broken, please replace it and reinsert the data.

* Step 1

    Delete the the broken database

* Step 2

    Decompress the databases.zip file inside `./manage_system/backend/Database`

* Step 3

    Copy and replace the broken database  
